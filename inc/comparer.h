#include "defines.h"
#include "types.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#pragma once
using namespace std;

class Comparer {
    protected:
    public:
        // Constructor
        Comparer();

        // Compare the next trace line with the golden model trace
        void compare();
        
        // Checks if the strings trace and goldenTrace are equal. If they are not, the program is halted
        void checkTraceDifference(string trace, string goldenTrace);
    private:
        int traceLine = 0;    
        ifstream traceFile;
        ifstream goldenTraceFile;
};