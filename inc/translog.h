#include "defines.h"
#include "types.h"
#pragma once
using namespace std;

class TransLog {
    protected:
        // Constructor
        TransLog();
        // Singleton pointer
        static TransLog *transLog;
    public:
        // Singleton pointer getter
        static TransLog *getInstance();

        // Process a transaction
        void transaction(instr_st commited_instr);

    private:
        // Decode an instruction type into readable string
        string decodeInstrType(instr_type_e type);
        // Decode an instruction name into readable string
        string decodeInstrName(instr_name_e name);
};