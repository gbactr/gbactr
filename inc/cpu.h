#include "defines.h"
#include "fetch_unit.h"
#include "decoder.h"
#include "decoderTHUMB.h"
#include "execution_unit.h"
#include "execution_unit_THUMB.h"
#ifdef FTR_TRACE_LOG
#include "tracelog.h"
#endif // FTR_TRACE_LOG
#ifdef FTR_TRANS_LOG
#include "translog.h"
#endif // FTR_TRANS_LOG
#ifdef PRINT_INSTRUCTION
#include <iostream>
#endif // PRINT_INSTRUCTION
#pragma once

class CPU {
    protected:
        // Constructor
        CPU();
        // Singleton pointer
        static CPU *cpu;
    public:
        // Singleton pointer getter
        static CPU *getInstance();

        // The CPU fetches, decodes, and executes a single instruction
        void executeInstruction();
        void executeInstructionARM();
        void executeInstructionTHUMB();
    private:
        FetchUnit *fetchUnit;
        Decoder *decoder;
        DecoderTHUMB *decoderTHUMB;
        ExecutionUnit *executionUnit;
        ExecutionUnitTHUMB *executionUnitTHUMB;
#ifdef FTR_TRACE_LOG
        TraceLog *traceLog;
#endif // FTR_TRACE_LOG
#ifdef FTR_TRANS_lOG
        TransLog *transLog;
#endif // FTR_TRANS_lOG
        Regfile *regfile;
};