#include "defines.h"
#include "types.h"
#include "mem_req.h"
#include "memory_region.h"

using namespace std;
#pragma once

class WRAM: public MemoryRegion{
    protected:
    public:
        // Constructor
        WRAM(int _size, addr_t _baseAddress);
    private:
};