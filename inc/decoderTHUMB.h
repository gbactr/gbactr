#include "defines.h"
#include "types.h"
#include "regfile.h"
#pragma once

// Op fields
#define THUMB_OP1_MSB 12
#define THUMB_OP1_LSB 11
#define THUMB_OP2_BIT 9
#define THUMB_OP3_MSB 9
#define THUMB_OP3_LSB 6
#define THUMB_OP4_MSB 9
#define THUMB_OP4_LSB 8
#define OPCODE_MSB 15
#define OPCODE_LSB 8

// Offset / Word fields
#define OFFSET3_MSB 8
#define OFFSET3_LSB 6
#define OFFSET5_MSB 10
#define OFFSET5_LSB 6
#define OFFSET7_MSB 6
#define OFFSET7_LSB 0
#define OFFSET8_MSB 7
#define OFFSET8_LSB 0
#define OFFSET11_MSB 10
#define OFFSET11_LSB 0
#define THUMB_CONDITION_MSB 11
#define THUMB_CONDITION_LSB 8

// Register operands
#define RD1_MSB 2
#define RD1_LSB 0
#define RD2_MSB 10
#define RD2_LSB 8
#define RB1_MSB 5
#define RB1_LSB 3
#define RB2_MSB 10
#define RB2_LSB 8
#define RS_MSB 5
#define RS_LSB 3
#define RO_MSB 8
#define RO_LSB 6

// Bit fields
#define I_BIT 10
#define H1_BIT 7
#define H2_BIT 6
#define L_BIT 11
#define H_BIT 11
#define SP_BIT 11
#define B_BIT 10
#define R_BIT 8
#define S1_BIT 10
#define S2_BIT 7

// Opcodes
#define MOVE_SHIFTED_REGISTER_MASK 0xe0
#define MOVE_SHIFTED_REGISTER_OPCODE 0x00
#define ADD_SUBTRACT_MASK 0xf8
#define ADD_SUBTRACT_OPCODE 0x18
#define MOVE_COMPARE_ADD_SUBTRACT_IMMEDIATE_MASK 0xe0
#define MOVE_COMPARE_ADD_SUBTRACT_IMMEDIATE_OPCODE 0x20
#define ALU_OPERATIONS_MASK 0xfc
#define ALU_OPERATIONS_OPCODE 0x40
#define HI_REGISTER_OPERATIONS_BRANCH_EXCHANGE_MASK 0xfc
#define HI_REGISTER_OPERATIONS_BRANCH_EXCHANGE_OPCODE 0x44
#define PC_RELATIVE_LOAD_MASK 0xf8
#define PC_RELATIVE_LOAD_OPCODE 0x48
#define LOAD_STORE_WITH_REGISTER_OFFSET_MASK 0xf2
#define LOAD_STORE_WITH_REGISTER_OFFSET_OPCODE 0x50
#define LOAD_STORE_SIGN_EXTENDED_BYTE_HALFWORD_MASK 0xf2
#define LOAD_STORE_SIGN_EXTENDED_BYTE_HALFWORD_OPCODE 0x52
#define LOAD_STORE_WITH_IMMEDIATE_OFFSET_MASK 0xe0
#define LOAD_STORE_WITH_IMMEDIATE_OFFSET_OPCODE 0x60
#define LOAD_STORE_HALFWORD_MASK 0xf0
#define LOAD_STORE_HALFWORD_OPCODE 0x80
#define SP_RELATIVE_LOAD_STORE_MASK 0xf0
#define SP_RELATIVE_LOAD_STORE_OPCODE 0x90
#define LOAD_ADDRESS_MASK 0xf0
#define LOAD_ADDRESS_OPCODE 0xa0
#define ADD_OFFSET_TO_STACK_POINTER_MASK 0xff
#define ADD_OFFSET_TO_STACK_POINTER_OPCODE 0xb0
#define PUSH_POP_REGISTERS_MASK 0xf6
#define PUSH_POP_REGISTERS_OPCODE 0xb4
#define MULTIPLE_LOAD_STORE_MASK 0xf0
#define MULTIPLE_LOAD_STORE_OPCODE 0xc0
#define CONDITIONAL_BRANCH_MASK 0xf0
#define CONDITIONAL_BRANCH_OPCODE 0xd0
#define SOFTWARE_INTERRUPT_MASK 0xff
#define SOFTWARE_INTERRUPT_OPCODE 0xdf
#define UNCONDITIONAL_BRANCH_MASK 0xf8
#define UNCONDITIONAL_BRANCH_OPCODE 0xe0
#define LONG_BRANCH_WITH_LINK_MASK 0xf0
#define LONG_BRANCH_WITH_LINK_OPCODE 0xf0

class DecoderTHUMB {
    protected:
        // Constructor
        DecoderTHUMB();
        // Singleton pointer
        static DecoderTHUMB *decoderTHUMB;
    public:
        // Singleton pointer getter
        static DecoderTHUMB *getInstance();

        // Decodes an instruction
        instr_THUMB_st decodeInstruction(half_t fetch_instr);
    private:
        instr_type_THUMB_e decodeType(half_t fetch_instr);
        void decodeMoveShiftedRegister(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeAddSubtract(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeMoveCompareAndSubtractImmediate(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeALUOperation(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeHiRegisterOperationsBranchExchange(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodePCRelativeLoad(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeLoadStoreWithRegisterOffset(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeLoadStoreSignExtendedByteHalfword(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeLoadStoreWithImmediateOffset(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeLoadStoreHalfword(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeSPRelativeLoadStore(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeLoadAddress(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeAddOffsetToStackPointer(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodePushPopRegisters(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeMultipleLoadStore(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeConditionalBranch(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeSoftwareInterrupt(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeUnconditionalBranch(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        void decodeLongBranchWithLink(half_t fetch_instr, instr_THUMB_st *decoded_instr);
        bool checkBitmask(half_t instr, byte_t field_lsb, byte_t mask, byte_t value);
        word_t getRange(word_t word, byte_t lsb, byte_t msb);

        Regfile *regfile;
};
