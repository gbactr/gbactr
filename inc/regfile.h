#include "defines.h"
#include "types.h"
#ifdef PRINT_INSTRUCTION
#include <iostream>
#endif // PRINT_INSTRUCTION
#pragma once

#define NUM_GPR 16

class Regfile {
    protected:
        // Constructor
        Regfile();
        // Singleton pointer
        static Regfile *regfile;
    public:
        // Singleton pointer getter
        static Regfile *getInstance();

        // Read a register pertaining to the current mode
        word_t readGPR(int reg);

        // Read a register pertaining to a specific mode
        word_t readGPR(int reg, mode_e mode);

        // Write a register pertaining to the current mode
        void writeGPR(int reg, word_t value);
    
        // Write a register pertaining to a specific mode
        void writeGPR(int reg, word_t value, mode_e mode);

        // Transforms a PSR struct into its equivalent word encoding
        word_t PSRToWord(PSR_t psr);

        // Transforms a word into a PSR struct
        PSR_t wordToPSR(word_t psr);

        // Gets the CPSR register
        PSR_t getCPSR();

        // Reads the diferent flags from the CPSR
        bool readCPSR_N();
        bool readCPSR_Z();
        bool readCPSR_C();
        bool readCPSR_V();
        bool readCPSR_I();
        bool readCPSR_F();
        bool readCPSR_T();

        // Writes a PSR into CPSR
        void writeCPSR(PSR_t psr);
        // Writes the word encoding PSR information into CPSR
        void writeCPSR(word_t value);
        
        // Writes the diferent flags from the CPSR
        void writeCPSR_N(bool N);
        void writeCPSR_Z(bool Z);
        void writeCPSR_C(bool C);
        void writeCPSR_V(bool V);
        void writeCPSR_I(bool I);
        void writeCPSR_F(bool F);
        void writeCPSR_T(bool T);

        // Set current execution mode
        void setCurrentMode(mode_e mode);
        // Get current execution mode from CPSR
        mode_e getCurrentMode();

        // Get the current mode's SPSR
        PSR_t getSPSR();
        // Gets the specified mode's SPSR
        PSR_t getSPSR(mode_e mode);

        // Writes the word encoding PSR information into the current mode's SPSR
        void writeSPSR(word_t value);
        // Writes the word encoding PSR information into the specified mode's SPSR
        void writeSPSR(mode_e mode, word_t value);

        // Get the CPU's mode
        cpu_mode_e getCPUMode();
        // Set the CPU's mode
        void setCPUMode(cpu_mode_e _cpu_mode);
    private:
        // A gpr array is initialized with User/System GPR
        void initializeGprArray(word_t **gprArray);

        // All register contents are initialized
        void initializeRegisters();

        // Returns a register file pertaining to a specific mode
        word_t **getGprArray(mode_e mode);

        // Shared and User/System GPR
        word_t **usr_gpr;
        word_t R0, R1, R2, R3, R4, R5, R6, R7, R8, R9, R10, R11, R12, R13, R14, R15;
        PSR_t CPSR;
        // FIQ GPR
        word_t **fiq_gpr;
        word_t R8_fiq, R9_fiq, R10_fiq, R11_fiq, R12_fiq, R13_fiq, R14_fiq;
        PSR_t SPSR_fiq;
        // Supervisor GPR
        word_t **svc_gpr;
        word_t R13_svc, R14_svc;
        PSR_t SPSR_svc;
        // Abort GPR
        word_t **abt_gpr;
        word_t R13_abt, R14_abt;
        PSR_t SPSR_abt;
        // IRQ GPR
        word_t **irq_gpr;
        word_t R13_irq, R14_irq;
        PSR_t SPSR_irq;
        // Undefined GPR
        word_t **und_gpr;
        word_t R13_und, R14_und;
        PSR_t SPSR_und;

        cpu_mode_e cpu_mode;
};