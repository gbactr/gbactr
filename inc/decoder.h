#include "defines.h"
#include "types.h"
#include "regfile.h"
#pragma once

// Register Operand 2
#define OPERAND2_SHIFT_MSB 11
#define OPERAND2_SHIFT_LSB 4
#define OPERAND2_RM_MSB 3
#define OPERAND2_RM_LSB 0

#define OPERAND2_EXPLICIT_SHIFT 4
#define OPERAND2_SHIFT_TYPE_MSB 6
#define OPERAND2_SHIFT_TYPE_LSB 5
#define OPERAND2_SHIFT_AMOUNT_MSB 11
#define OPERAND2_SHIFT_AMOUNT_LSB 7
#define OPERAND2_SHIFT_REG_MSB 11
#define OPERAND2_SHIFT_REG_LSB 8

// Immediate Operand 2
#define OPERAND2_ROTATE_MSB 11
#define OPERAND2_ROTATE_LSB 8
#define OPERAND2_IMM_MSB 7
#define OPERAND2_IMM_LSB 0

// Generic Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond         | OP1                | OP3                   | OP2                   |           |
 *  -------------------------------------------------------------------------------------------------
 */
#define CONDITION_MSB 31
#define CONDITION_LSB 28
#define CONDITION_BITMASK 0x0000000f

#define OP1_MSB 27
#define OP1_LSB 20
#define OP2_MSB 11
#define OP2_LSB 4
#define OP3_MSB 19
#define OP3_LSB 12

// Branch and Exchange Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 000100101111111111110001                                              | Rn        |
 *  -------------------------------------------------------------------------------------------------
 */
#define BRANCH_AND_EXCHANGE_RN_MSB 3
#define BRANCH_AND_EXCHANGE_RN_LSB 0

// Branch Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 101    | L| offset                                                                |
 *  -------------------------------------------------------------------------------------------------
 */
#define BRANCH_L 24
#define BRANCH_OFFSET_MSB 23
#define BRANCH_OFFSET_LSB 0

// Data Processing Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 0| 0| I| Opcode    | S| Rn        | Rd        |  Operand 2                        |
 *  -------------------------------------------------------------------------------------------------
 */
#define DATA_PROCESSING_I 25
#define DATA_PROCESSING_OPCODE_MSB 24
#define DATA_PROCESSING_OPCODE_LSB 21
#define DATA_PROCESSING_P 22
#define DATA_PROCESSING_S 20
#define DATA_PROCESSING_RN_MSB 19
#define DATA_PROCESSING_RN_LSB 16
#define DATA_PROCESSING_RD_MSB 15
#define DATA_PROCESSING_RD_LSB 12
#define DATA_PROCESSING_OPERAND2_MSB 11
#define DATA_PROCESSING_PSR_OPERAND2_LSB 4
#define DATA_PROCESSING_PSR_RM_MSB 3
#define DATA_PROCESSING_OPERAND2_LSB 0

// Multiply Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 0               | A| S| Rd        | Rn        | Rs        | 1001      | Rm        |
 *  -------------------------------------------------------------------------------------------------
 */
#define MULTIPLY_A 21
#define MULTIPLY_S 20
#define MULTIPLY_RD_MSB 19
#define MULTIPLY_RD_LSB 16
#define MULTIPLY_RN_MSB 15
#define MULTIPLY_RN_LSB 12
#define MULTIPLY_RS_MSB 11
#define MULTIPLY_RS_LSB 8
#define MULTIPLY_RM_MSB 3
#define MULTIPLY_RM_LSB 0

// Multiply Long Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 0            | U| A| S| RdHi      | RdLo      | Rs        | 1001      | Rm        |
 *  -------------------------------------------------------------------------------------------------
 */
#define MULTIPLY_LONG_U 22
#define MULTIPLY_LONG_A 21
#define MULTIPLY_LONG_S 20
#define MULTIPLY_LONG_RDHI_MSB 19
#define MULTIPLY_LONG_RDHI_LSB 16
#define MULTIPLY_LONG_RDLO_MSB 15
#define MULTIPLY_LONG_RDLO_LSB 12
#define MULTIPLY_LONG_RS_MSB 11
#define MULTIPLY_LONG_RS_LSB 8
#define MULTIPLY_LONG_RM_MSB 3
#define MULTIPLY_LONG_RM_LSB 0

// Single Data Transfer Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 01  | I| P| U| B| W| L| Rn        | Rd        | Offset                            |
 *  -------------------------------------------------------------------------------------------------
 */
#define SINGLE_DATA_TRANSFER_I 25
#define SINGLE_DATA_TRANSFER_P 24
#define SINGLE_DATA_TRANSFER_U 23
#define SINGLE_DATA_TRANSFER_B 22
#define SINGLE_DATA_TRANSFER_W 21
#define SINGLE_DATA_TRANSFER_L 20
#define SINGLE_DATA_TRANSFER_RN_MSB 19
#define SINGLE_DATA_TRANSFER_RN_LSB 16
#define SINGLE_DATA_TRANSFER_RD_MSB 15
#define SINGLE_DATA_TRANSFER_RD_LSB 12
#define SINGLE_DATA_TRANSFER_OFFSET_MSB 11
#define SINGLE_DATA_TRANSFER_OFFSET_LSB 0

// Halfword Data Transfer Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 000    | P| U| x| W| L| Rn        | Rd        | Offset1   | 1| S| H| 1| Rm/Offset2|
 *  -------------------------------------------------------------------------------------------------
 */
#define HALFWORD_DATA_TRANSFER_P 24
#define HALFWORD_DATA_TRANSFER_U 23
#define HALFWORD_DATA_TRANSFER_W 21
#define HALFWORD_DATA_TRANSFER_L 20
#define HALFWORD_DATA_TRANSFER_RN_MSB 19
#define HALFWORD_DATA_TRANSFER_RN_LSB 16
#define HALFWORD_DATA_TRANSFER_RD_MSB 15
#define HALFWORD_DATA_TRANSFER_RD_LSB 12
#define HALFWORD_DATA_TRANSFER_OFFSET1_MSB 11
#define HALFWORD_DATA_TRANSFER_OFFSET1_LSB 8
#define HALFWORD_DATA_TRANSFER_S 6
#define HALFWORD_DATA_TRANSFER_H 5
#define HALFWORD_DATA_TRANSFER_OFFSET2_MSB 3
#define HALFWORD_DATA_TRANSFER_OFFSET2_LSB 0
#define HALFWORD_DATA_TRANSFER_RM_MSB 3
#define HALFWORD_DATA_TRANSFER_RM_LSB 0

// Block Data Transfer Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 100    | P| U| S| W| L| Rn        | Register List                                 |
 *  -------------------------------------------------------------------------------------------------
 */
#define BLOCK_DATA_TRANSFER_P 24
#define BLOCK_DATA_TRANSFER_U 23
#define BLOCK_DATA_TRANSFER_S 22
#define BLOCK_DATA_TRANSFER_W 21
#define BLOCK_DATA_TRANSFER_L 20
#define BLOCK_DATA_TRANSFER_RN_MSB 19
#define BLOCK_DATA_TRANSFER_RN_LSB 16
#define BLOCK_DATA_TRANSFER_REGLIST_MSB 15
#define BLOCK_DATA_TRANSFER_REGLIST_LSB 0

// Single Data Swap Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 00010        | B| 00  | Rn        | Rd        | 0000      | 1001      | Rm         |
 *  -------------------------------------------------------------------------------------------------
 */
#define SINGLE_DATA_SWAP_B 22
#define SINGLE_DATA_SWAP_RN_MSB 19
#define SINGLE_DATA_SWAP_RN_LSB 16
#define SINGLE_DATA_SWAP_RD_MSB 15
#define SINGLE_DATA_SWAP_RD_LSB 12
#define SINGLE_DATA_SWAP_RM_MSB 3
#define SINGLE_DATA_SWAP_RM_LSB 0

// Software Interrupt Fields
/**
 *  -------------------------------------------------------------------------------------------------
 *  |31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|9 |8 |7 |6 |5 |4 |3 |2 |1 |0 |
 *  -------------------------------------------------------------------------------------------------
 *  | Cond      | 1111      | Comment                                                               |
 *  -------------------------------------------------------------------------------------------------
 */
#define SWI_COMMENT_MSB 23
#define SWI_COMMENT_LSB 0

// Opcodes
#define DATA_PROCESSING_OP1_MASK 0xc0
#define DATA_PROCESSING_OP1 0x00

#define MULTIPLY_OP1_MASK 0xfc
#define MULTIPLY_OP2_MASK 0x0f
#define MULTIPLY_OP1 0x00
#define MULTIPLY_OP2 0x09

#define MULTIPLY_LONG_OP1_MASK 0xf8
#define MULTIPLY_LONG_OP2_MASK 0x0f
#define MULTIPLY_LONG_OP1 0x08
#define MULTIPLY_LONG_OP2 0x09

#define SINGLE_DATA_SWAP_OP1_MASK 0xfb
#define SINGLE_DATA_SWAP_OP2_MASK 0xff
#define SINGLE_DATA_SWAP_OP1 0x10
#define SINGLE_DATA_SWAP_OP2 0x09

#define BRANCH_AND_EXCHANGE_OP1_MASK 0xff
#define BRANCH_AND_EXCHANGE_OP2_MASK 0xff
#define BRANCH_AND_EXCHANGE_OP3_MASK 0xff
#define BRANCH_AND_EXCHANGE_OP1 0x12
#define BRANCH_AND_EXCHANGE_OP2 0xf1
#define BRANCH_AND_EXCHANGE_OP3 0xff

#define HALFWORD_DATA_TRANSFER_REG_OP1_MASK 0xe4
#define HALFWORD_DATA_TRANSFER_REG_OP2_MASK 0xf9
#define HALFWORD_DATA_TRANSFER_REG_OP1 0x00
#define HALFWORD_DATA_TRANSFER_REG_OP2 0x09

#define HALFWORD_DATA_TRANSFER_IMM_OP1_MASK 0xe4
#define HALFWORD_DATA_TRANSFER_IMM_OP2_MASK 0x09
#define HALFWORD_DATA_TRANSFER_IMM_OP1 0x04
#define HALFWORD_DATA_TRANSFER_IMM_OP2 0x09

#define SINGLE_DATA_TRANSFER_OP1_MASK 0xc0
#define SINGLE_DATA_TRANSFER_OP1 0x40

#define UNDEFINED_OP1_MASK 0xe0
#define UNDEFINED_OP2_MASK 0x01
#define UNDEFINED_OP1 0x60
#define UNDEFINED_OP2 0x01

#define BLOCK_DATA_TRANSFER_OP1_MASK 0xe0
#define BLOCK_DATA_TRANSFER_OP1 0x80

#define BRANCH_OP1_MASK 0xe0
#define BRANCH_OP1 0xa0

#define COPROCESSOR_DATA_TRANSFER_OP1_MASK 0xe0
#define COPROCESSOR_DATA_TRANSFER_OP1 0xc0

#define COPROCESSOR_DATA_OPERATION_OP1_MASK 0xf0
#define COPROCESSOR_DATA_OPERATION_OP2_MASK 0x01
#define COPROCESSOR_DATA_OPERATION_OP1 0xe0
#define COPROCESSOR_DATA_OPERATION_OP2 0x00

#define COPROCESSOR_REGISTER_TRANSFER_OP1_MASK 0xf0
#define COPROCESSOR_REGISTER_TRANSFER_OP2_MASK 0x01
#define COPROCESSOR_REGISTER_TRANSFER_OP1 0xe0
#define COPROCESSOR_REGISTER_TRANSFER_OP2 0x01

#define SOFTWARE_INTERRUPT_OP1_MASK 0xf0
#define SOFTWARE_INTERRUPT_OP1 0xf0

class Decoder {
    protected:
        // Constructor
        Decoder();
        // Singleton pointer
        static Decoder *decoder;
    public:
        // Singleton pointer getter
        static Decoder *getInstance();

        // Decodes an instruction
        instr_st decodeInstruction(word_t fetch_instr);
    private:
        condition_e decodeCondition(word_t fetch_instr);
        instr_type_e decodeType(word_t fetch_instr);
        void decodeDataProcessing(word_t fetch_instr, instr_st *decoded_instr);
        void decodeMultiply(word_t fetch_instr, instr_st *decoded_instr);
        void decodeMultiplyLong(word_t fetch_instr, instr_st *decoded_instr);
        void decodeSingleDataSwap(word_t fetch_instr, instr_st *decoded_instr);
        void decodeBranchAndExchange(word_t fetch_instr, instr_st *decoded_instr);
        void decodeHalfwordDataTransferReg(word_t fetch_instr, instr_st *decoded_instr);
        void decodeHalfwordDataTransferImm(word_t fetch_instr, instr_st *decoded_instr);
        void decodeSingleDataTransfer(word_t fetch_instr, instr_st *decoded_instr);
        void decodeUndefined(word_t fetch_instr, instr_st *decoded_instr);
        void decodeBlockDataTransfer(word_t fetch_instr, instr_st *decoded_instr);
        void decodeBranch(word_t fetch_instr, instr_st *decoded_instr);
        void decodeCoprocessorDataTransfer(word_t fetch_instr, instr_st *decoded_instr);
        void decodeCoprocessorDataOperation(word_t fetch_instr, instr_st *decoded_instr);
        void decodeCoprocessorRegisterTransfer(word_t fetch_instr, instr_st *decoded_instr);
        void decodeSoftwareInterrupt(word_t fetch_instr, instr_st *decoded_instr);
        bool checkBitmask(word_t instr, byte_t field_lsb, byte_t mask, byte_t value);
        word_t getRange(word_t word, byte_t lsb, byte_t msb);

        Regfile *regfile;
};
