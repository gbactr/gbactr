#include "types.h"
#include "defines.h"
#pragma once

enum MemReqType {
    READ_BYTE, READ_HALF, READ_WORD,
    WRITE_BYTE, WRITE_HALF, WRITE_WORD
};

class MemReq {
    protected:
    public:
        // Constructor
        MemReq(MemReqType _type, addr_t _address, word_t _data);
        // Constructor without data initialization
        MemReq(MemReqType _type, addr_t _address);
        // Constructor without data
        MemReq();

        // Returns true if the memory request is a read request
        bool isRead(MemReqType type);
        bool isRead();

        // Returns true if the memory request is a write request
        bool isWrite(MemReqType type);
        bool isWrite();

        // Returns the size in bytes of the memory request
        int size(MemReqType type);
        int size();

        // Memory request type
        MemReqType type;
        // Data to read or write. Word-size is allocated
        word_t data;
        // Address of the request
        addr_t address;
    private:
};