
#include "defines.h"
#include "types.h"
#include "mem_req.h"
#include "memory_region.h"
#pragma once

class IORegs: public MemoryRegion {
    protected:
        // Constructor
        IORegs();
        // Singleton pointer
        static IORegs *ioRegs;
    public:
        // Singleton pointer getter
        static IORegs *getInstance();

        void request(MemReq *request);

        DISPCNT_st DISPCNT;
        DISPSTAT_st DISPSTAT;
        BGCNT_st BG0CNT;
        BGCNT_st BG1CNT;
        BGCNT_st BG2CNT;
        BGCNT_st BG3CNT;
        BGOFS_st BG0HOFS;
        BGOFS_st BG0VOFS;
        BGOFS_st BG1HOFS;
        BGOFS_st BG1VOFS;
        BGOFS_st BG2HOFS;
        BGOFS_st BG2VOFS;
        BGOFS_st BG3HOFS;
        BGOFS_st BG3VOFS;
        BGP_st BGP2A;
        BGP_st BGP2B;
        BGP_st BGP2C;
        BGP_st BGP2D;
        BGXY_st BG2X;
        BGXY_st BG2Y;
        BGP_st BGP3A;
        BGP_st BGP3B;
        BGP_st BGP3C;
        BGP_st BGP3D;
        BGXY_st BG3X;
        BGXY_st BG3Y;
        WIN_st WIN0H;
        WIN_st WIN1H;
        WIN_st WIN0V;
        WIN_st WIN1V;
        WININ_st WININ;
        WINOUT_st WINOUT;
        MOSAIC_st MOSAIC;
        BLDCNT_st BLDCNT;
        BLDALPHA_st BLDALPHA;
        BLDY_st BLDY;
        DMAAD_st DMA0SAD;
        DMAAD_st DMA1SAD;
        DMAAD_st DMA2SAD;
        DMAAD_st DMA3SAD;
        DMAAD_st DMA0DAD;
        DMAAD_st DMA1DAD;
        DMAAD_st DMA2DAD;
        DMAAD_st DMA3DAD;
        DMACNT_L_st DMA0CNT_L;
        DMACNT_L_st DMA1CNT_L;
        DMACNT_L_st DMA2CNT_L;
        DMACNT_L_st DMA3CNT_L;
        DMACNT_H_st DMA0CNT_H;
        DMACNT_H_st DMA1CNT_H;
        DMACNT_H_st DMA2CNT_H;
        DMACNT_H_st DMA3CNT_H;
    private:
            word_t packDISPCNT(DISPCNT_st _DISPCNT);
            word_t packDISPSTAT(DISPSTAT_st _DISPSTAT);
            word_t packBGCNT(BGCNT_st _BGCNT);
            word_t packBGOFS(BGOFS_st _BGOFS);
            word_t packBGP(BGP_st _BGP);
            word_t packBGXY(BGXY_st _BGXY);
            word_t packWIN(WIN_st _WIN);
            word_t packWININ(WININ_st _WININ);
            word_t packWINOUT(WINOUT_st _WINOUT);
            word_t packMOSAIC(MOSAIC_st _MOSAIC);
            word_t packBLDCNT(BLDCNT_st _BLDCNT);
            word_t packBLDALPHA(BLDALPHA_st _BLDALPHA);
            word_t packBLDY(BLDY_st _BLDY);
            word_t packDMAAD(DMAAD_st _DMAAD);
            word_t packDMACNT_L(DMACNT_L_st _DMACNT_L);
            word_t packDMACNT_H(DMACNT_H_st _DMACNT_H);

            void unpackDISPCNT(DISPCNT_st *_DISPCNT, word_t data);
            void unpackDISPSTAT(DISPSTAT_st *DISPSTAT, word_t data);
            void unpackBGCNT(BGCNT_st *_BGCNT, word_t data);
            void unpackBGOFS(BGOFS_st *_BGOFS, word_t data);
            void unpackBGP(BGP_st *_BGP, word_t data);
            void unpackBGXY(BGXY_st *_BGXY, word_t data);
            void unpackWIN(WIN_st *_WIN, word_t data);
            void unpackWININ(WININ_st *_WININ, word_t data);
            void unpackWINOUT(WINOUT_st *_WINOUT, word_t data);
            void unpackMOSAIC(MOSAIC_st *_MOSAIC, word_t data);
            void unpackBLDCNT(BLDCNT_st *_BLDCNT, word_t data);
            void unpackBLDALPHA(BLDALPHA_st *_BLDALPHA, word_t data);
            void unpackBLDY(BLDY_st *_BLDY, word_t data);
            void unpackDMAAD(DMAAD_st *_DMAAD, word_t data);
            void unpackDMACNT_L(DMACNT_L_st *_DMACNT_L, word_t data);
            void unpackDMACNT_H(DMACNT_H_st *_DMACNT_H, word_t data);

        word_t size;
        word_t baseAddress;
};