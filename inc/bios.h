#include "defines.h"
#include "types.h"
#include "mem_req.h"
#include "memory_region.h"

#include <fstream>
#include <string>
#include <iostream>
using namespace std;
#pragma once

class BIOS : public MemoryRegion{
    protected:
        // Constructor
        BIOS();
        // Singleton pointer
        static BIOS* bios;
    public:
        // Singleton pointer getter
        static BIOS *getInstance();

        // Memory request
        void request(MemReq *request);

    private:
        void mapBIOS(char * biosName);
};