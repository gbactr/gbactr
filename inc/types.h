#include <stdint.h>
#pragma once

// Data granularity
typedef uint8_t byte_t;
typedef int8_t sbyte_t;
typedef uint16_t half_t;
typedef int16_t shalf_t;
typedef uint32_t word_t;
typedef int32_t sword_t;
typedef uint64_t dword_t;
typedef int64_t sdword_t;

// Memory address
typedef uint32_t addr_t;

enum cpu_mode_e {
    ARM,
    THUMB
};

enum mode_e {
    USER       = 0b10000,
    FIQ        = 0b10001,
    IRQ        = 0b10010,
    SUPERVISOR = 0b10011,
    ABORT      = 0b10111,
    UNDEFINED  = 0b11011,
    SYSTEM     = 0b11111
};

struct PSR_t {
    bool N = false;   // Negative
    bool Z = false;   // Zero
    bool C = false;   // Carry
    bool V = false;   // Overflow
    bool I = false;   // IRQ disable
    bool F = false;   // FIQ disable
    bool T = false;   // THUMB state
    mode_e M = SUPERVISOR; // Mode
};

/**
 * DECODE TYPES
 */

enum instr_type_e {
    DATA_PROCESSING,
    MULTIPLY,
    MULTIPLY_LONG,
    SINGLE_DATA_SWAP,
    BRANCH_AND_EXCHANGE,
    HALFWORD_DATA_TRANSFER_REG,
    HALFWORD_DATA_TRANSFER_IMM,
    SINGLE_DATA_TRANSFER,
    UNDEFINED_INSTR,
    BLOCK_DATA_TRANSFER,
    BRANCH,
    COPROCESSOR_DATA_TRANSFER,
    COPROCESSOR_DATA_OPERATION,
    COPROCESSOR_REGISTER_TRANSFER,
    SOFTWARE_INTERRUPT
};

enum instr_type_THUMB_e {
    MOVE_SHIFTED_REGISTER,
    ADD_SUBTRACT,
    MOVE_COMPARE_ADD_SUBTRACT_IMMEDIATE,
    ALU_OPERATIONS,
    HI_REGISTER_OPERATIONS_BRANCH_EXCHANGE,
    PC_RELATIVE_LOAD,
    LOAD_STORE_WITH_REGISTER_OFFSET,
    LOAD_STORE_SIGN_EXTENDED_BYTE_HALFWORD,
    LOAD_STORE_WITH_IMMEDIATE_OFFSET,
    LOAD_STORE_HALFWORD,
    SP_RELATIVE_LOAD_STORE,
    LOAD_ADDRESS,
    ADD_OFFSET_TO_STACK_POINTER,
    PUSH_POP_REGISTERS,
    MULTIPLE_LOAD_STORE,
    CONDITIONAL_BRANCH,
    SOFTWARE_INTERRUPT_THUMB,
    UNCONDITIONAL_BRANCH,
    LONG_BRANCH_WITH_LINK
};

enum instr_name_e {
    ADC, // Add with carry
    ADD, // Add
    AND, // And
    B,   // Branch
    BIC, // Bit Clear
    BL,  // Branch with Link
    BX,  // Branch and Exchange
    CDP, // Coprocessor Data Processing
    CMN, // Compare Negative
    CMP, // Compare
    EOR, // Exclusive OR
    LDC, // Load coprocessor from memory
    LDM, // Load multiple registers
    LDR, // Load register from memory
    LDRH, // Load register from memory half
    MCR, // Move CPU register to coprocessor register
    MLA, // Multiply Accumulate
    MLAL, // Multiply Accumulate Long
    MOV, // Move register or constant
    MRC, // Move from coprocessor register to CPU register
    MRS, // Move PSR status/flags to register
    MSR, // Move register to PSR status/flags
    MUL, // Multiply
    MULL, // Multiply Long
    MVN, // Move negative register
    ORR, // OR
    RSB, // Reverse Subtract
    RSC, // Reverse Subtract with Carry
    SBC, // Store coprocessor register to memory
    STC, // Store Multiple
    STM, // Store to memory
    STR, // Store register to memory
    STRH, // Store register to memory half
    SUB, // Subtract
    SWI, // Software Interrupt
    SWP, // Swap register with memory
    TEQ, // Test bitwise equality
    TST, // Test bits
    UND  // Undefined Instruction
};

enum instr_name_THUMB_e {
    THUMB_ADC, // Add with carry
    THUMB_ADD, // Add
    THUMB_AND, // And
    THUMB_ASR, // Arithmetic Shift Right
    THUMB_B,   // Branch
    THUMB_Bxx, // Conditional Branch
    THUMB_BIC, // Bit Clear
    THUMB_BL,  // Branch with Link
    THUMB_BLH,  // Branch with Link
    THUMB_BX,  // Branch and Exchange
    THUMB_CMN, // Compare Negative
    THUMB_CMP, // Compare
    THUMB_EOR, // Exclusive OR
    THUMB_LDMIA, // Load multiple registers
    THUMB_LDR, // Load register from memory
    THUMB_LDRB, // Load register from memory byte
    THUMB_LDRH, // Load register from memory half
    THUMB_LSL, //Logical Shift Left
    THUMB_LDSB, // Load sign-extended register from memory byte
    THUMB_LDSH, // Load sign-extended register from memory half
    THUMB_LSR, // Logical Shift Right
    THUMB_MOV, // Move register or constant
    THUMB_MUL, // Multiply
    THUMB_MVN, // Move negative register
    THUMB_NEG, // Negate
    THUMB_ORR, // OR
    THUMB_POP, // Pop registers
    THUMB_PUSH, // Push registers
    THUMB_ROR, // Rotate Right
    THUMB_SBC, // Store coprocessor register to memory
    THUMB_STMIA, // Store to memory
    THUMB_STR, // Store register to memory
    THUMB_STRB, // Store register to memory byte
    THUMB_STRH, // Store register to memory half
    THUMB_SWI, // Software Interrupt
    THUMB_SUB, // Subtract
    THUMB_TST // Test bits
};

enum condition_e {
    EQ = 0b0000, // Equal
    NE = 0b0001, // Not Equal
    CS = 0b0010, // Carry Set
    CC = 0b0011, // Carry Clear
    MI = 0b0100, // Negative
    PL = 0b0101, // Positive or Zero
    VS = 0b0110, // Overflow
    VC = 0b0111, // No Overflow
    HI = 0b1000, // Unsigned Higher
    LS = 0b1001, // Unsigned Lower or Same
    GE = 0b1010, // Greater or Equal
    LT = 0b1011, // Less than
    GT = 0b1100, // Greater than
    LE = 0b1101, // Less than or Equal
    AL = 0b1110, // Always
    RE = 0b1111  // Reserved
};

enum data_processing_opcode_e {
    OP_AND = 0b0000,
    OP_EOR = 0b0001,
    OP_SUB = 0b0010,
    OP_RSB = 0b0011,
    OP_ADD = 0b0100,
    OP_ADC = 0b0101,
    OP_SBC = 0b0110,
    OP_RSC = 0b0111,
    OP_TST = 0b1000,
    OP_TEQ = 0b1001,
    OP_CMP = 0b1010,
    OP_CMN = 0b1011,
    OP_ORR = 0b1100,
    OP_MOV = 0b1101,
    OP_BIC = 0b1110,
    OP_MVN = 0b1111
};

enum operand2_shift_type_e {
    LOGICAL_LEFT     = 0b00,
    LOGICAL_RIGHT    = 0b01,
    ARITHMETIC_RIGHT = 0b10,
    ROTATE_RIGHT     = 0b11
};

/**
 * INSTRUCTION STRUCTS 
 */

struct instr_st {
    instr_name_e name;
    instr_type_e type;
    condition_e condition;
    bool setCondition;
    byte_t r1, r2, r3, rd, rdHi;
    bool isImm, isUns;
    // Load/store
    bool isWriteBack, isByte, isUp, isPreIndexing;
    // Halfword Load/store
    bool isSigned, isHalfword;
    // Block Load/store
    bool isPSR;
    // Data Processing CPSR/SPSR
    bool isSPSR;
    word_t regList;
    word_t op1, op2, op3;
    // Software Interrupt
    word_t comment;
    bool C;
};

struct instr_THUMB_st {
    instr_name_THUMB_e name;
    instr_type_THUMB_e type;
    byte_t r1, r2, r3, rd;
    word_t op1, op2, op3;
    bool isImm;
    // Load/store
    bool isByte;
    // Halfword Load/store
    bool isSigned, isHalfword;
    word_t regList;
    byte_t branchCond;
    bool isSecondHalf;
    bool C;
    bool setCondCode;
};

/**
 *  IO REGISTERS
 */

struct DISPCNT_st {
    byte_t BG_mode;
    bool display_frame;
    bool hblank_OAM_access;
    bool two_d_obj_char;
    bool BG0_display;
    bool BG1_display;
    bool BG2_display;
    bool BG3_display;
    bool OBJ_display;
    bool WIN0_display;
    bool WIN1_display;
};

struct DISPSTAT_st {
    bool vblank;
    bool hblank;
    bool vcounter;
    bool vblank_irq_en;
    bool hblank_irq_en;
    bool vcounter_irq_en;
    byte_t vcount_line;
};

struct BGCNT_st {
    byte_t priority;
    byte_t char_base_block;
    bool mosaic;
    bool palettes;
    byte_t screen_base_block;
    bool display_overflow;
    byte_t screen_size;
};

struct BGOFS_st {
    half_t offset;
};

struct BGXY_st {
    byte_t fractional;
    word_t integer;
    bool sign;
};

struct BGP_st {
    byte_t fractional;
    byte_t integer;
    bool sign;
};

struct WIN_st {
    byte_t coord2;
    byte_t coord1;
};

struct WININ_st {
    bool win0_bg0_en;
    bool win0_bg1_en;
    bool win0_bg2_en;
    bool win0_bg3_en;
    bool win0_obj_en;
    bool win0_color_effect;
    bool win1_bg0_en;
    bool win1_bg1_en;
    bool win1_bg2_en;
    bool win1_bg3_en;
    bool win1_obj_en;
    bool win1_color_effect;
};

struct WINOUT_st {
    bool winout_bg0_en;
    bool winout_bg1_en;
    bool winout_bg2_en;
    bool winout_bg3_en;
    bool winout_obj_en;
    bool winout_color_effect;
    bool winobj_bg0_en;
    bool winobj_bg1_en;
    bool winobj_bg2_en;
    bool winobj_bg3_en;
    bool winobj_obj_en;
    bool winobj_color_effect;
};

struct MOSAIC_st {
    byte_t bg_hsize;
    byte_t bg_vsize;
    byte_t obj_hsize;
    byte_t obj_vsize;
};

struct BLDCNT_st {
    bool bg0_1st_target_pixel;
    bool bg1_1st_target_pixel;
    bool bg2_1st_target_pixel;
    bool bg3_1st_target_pixel;
    bool obj_1st_target_pixel;
    bool bd_1st_target_pixel;
    byte_t color_special_effect;
    bool bg0_2nd_target_pixel;
    bool bg1_2nd_target_pixel;
    bool bg2_2nd_target_pixel;
    bool bg3_2nd_target_pixel;
    bool obj_2nd_target_pixel;
    bool bd_2nd_target_pixel;
};

struct BLDALPHA_st {
    byte_t EVA_coef;
    byte_t EVB_coef;
};

struct BLDY_st {
    byte_t EVY_coef;
};

struct DMAAD_st {
    word_t address;
};

struct DMACNT_L_st {
    word_t count;
};

struct DMACNT_H_st {
    byte_t dest_addr_cont;
    byte_t source_addr_cont;
    bool DMA_repeat;
    bool DMA_type;
    bool DRQ;
    byte_t start_timing;
    bool IRQ;
    bool DMA_en;
};