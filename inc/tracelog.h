#include "defines.h"
#include "types.h"
#include "regfile.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#pragma once
using namespace std;

class TraceLog {
    protected:
        // Constructor
        TraceLog();
        // Singleton pointer
        static TraceLog *traceLog;
    public:
        // Singleton pointer getter
        static TraceLog *getInstance();

        // Trace the PC for the current instruction
        void tracePC(addr_t pc);
        // Trace the instr hex encoding
        void traceFetchInstr(word_t instr);
        void traceFetchInstrTHUMB(half_t instr);
        // Trace the instr assembly syntax
        void traceInstrAsm(instr_st instr);
        void traceInstrAsmTHUMB(instr_THUMB_st instr);
        // Trace the state of the registers
        void traceRegisters();

        bool log;
    private:
        // Get the string encoding of an instr
        string getInstrName(instr_st instr);
        string getInstrNameTHUMB(instr_THUMB_st instr);

        Regfile *regfile;
        ofstream traceFile;
};