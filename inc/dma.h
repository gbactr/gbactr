#include "defines.h"
#include "types.h"
#include "io_regs.h"
#pragma once

class Mem; // Forward declaration

class DMA{
    protected:
        // Constructor
        DMA(Mem *_mem);
        // Singleton pointer
        static DMA *dma;
    public:
        // Singleton pointer getter
        static DMA *getInstance(Mem *_mem);

        void processTransfers();

        bool transfering;
        bool repeating[4];

    private:
        Mem *mem;
        IORegs *ioregs;

        // Internal registers for repeats and the like
        DMAAD_st DMA0DAD;
        DMAAD_st DMA1DAD;
        DMAAD_st DMA2DAD;
        DMAAD_st DMA3DAD;
};