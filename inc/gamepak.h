#include "defines.h"
#include "types.h"
#include "mem_req.h"
#include "memory_region.h"

#include <fstream>
#include <string>
#include <iostream>
using namespace std;
#pragma once

class Gamepak : public MemoryRegion{
    protected:
        // Constructor
        Gamepak();
        // Singleton pointer
        static Gamepak*gamepak;
    public:
        // Singleton pointer getter
        static Gamepak *getInstance();

        // Memory request
        void request(MemReq *request);

    private:
        void mapRom(char * gameName);
};