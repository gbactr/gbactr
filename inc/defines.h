#include "conf.h"
#include <iostream>

#ifdef FTR_PRINT_DEBUG
#include <iostream>
#endif // PRINT_DEBUG
#pragma once

#define GAME_NAME "metroid.gba"
#define BIOS_NAME "gba_bios.bin"
#define GBACTR_TRACE "gbactr_trace.log"
#define GOLDEN_TRACE "golden_trace.log"

#define BIOS_BASE_ADDRESS        0x00000000 // BIOS    00000000
#define BIOS_TOP_ADDRESS         0x00004000 //         00003FFF
#define WRAM_1_BASE_ADDRESS      0x02000000 // WRAM    02000000
#define WRAM_1_TOP_ADDRESS       0x02040000 //         0203FFFF
#define WRAM_2_BASE_ADDRESS      0x03000000 // WRAM    03000000
#define WRAM_2_TOP_ADDRESS       0x03008000 //         03007FFF
#define IO_REGS_BASE_ADDRESS     0x04000000 // IO REGS 04000000
#define IO_REGS_TOP_ADDRESS      0x04000400 //         040003FE
#define PALETTE_RAM_BASE_ADDRESS 0x05000000 // PALETTE 05000000 
#define PALETTE_RAM_TOP_ADDRESS  0x05000400 //         050003FF
#define VRAM_BASE_ADDRESS        0x06000000 // VRAM    06000000
#define VRAM_TOP_ADDRESS         0x06018000 //         06017FFF
#define OAM_BASE_ADDRESS         0x07000000 // OAM     07000000
#define OAM_TOP_ADDRESS          0x07000400 //         070003FF
#define GAMEPAK_BASE_ADDRESS     0x08000000 // GAMEPAK 08000000
#define GAMEPAK_TOP_ADDRESS      0x0e000000 //         0DFFFFFF
#define SRAM_BASE_ADDRESS        0x0e000000 // SRAM    0E000000
#define SRAM_TOP_ADDRESS         0x0e010000 //         0E00FFFF

#define BIOS_SIZE        BIOS_TOP_ADDRESS        - BIOS_BASE_ADDRESS
#define WRAM_1_SIZE      WRAM_1_TOP_ADDRESS      - WRAM_1_BASE_ADDRESS
#define WRAM_2_SIZE      WRAM_2_TOP_ADDRESS      - WRAM_2_BASE_ADDRESS
#define IO_REGS_SIZE     IO_REGS_TOP_ADDRESS     - IO_REGS_BASE_ADDRESS
#define PALETTE_RAM_SIZE PALETTE_RAM_TOP_ADDRESS - PALETTE_RAM_BASE_ADDRESS
#define VRAM_SIZE        VRAM_TOP_ADDRESS        - VRAM_BASE_ADDRESS
#define OAM_SIZE         OAM_TOP_ADDRESS         - OAM_BASE_ADDRESS
#define GAMEPAK_SIZE     GAMEPAK_TOP_ADDRESS     - GAMEPAK_BASE_ADDRESS
#define SRAM_SIZE        SRAM_TOP_ADDRESS        - SRAM_BASE_ADDRESS

#define SP 13
#define LR 14
#define PC 15

#define PC_INIT_VALUE 0x00000000

#ifdef FTR_DBG_ASSERT
#define DBG_ASSERT_E(expression) \
    if (expression) { \
        cout << std::dec << "DEBUG ASSERTION: " << __FILE__ << " " << __LINE__ <<  endl; \
    }
#define DBG_ASSERT() \
    cout << std::dec << "DEBUG ASSERTION: " << __FILE__ << " " << __LINE__ <<  endl;
#else // !FTR_DBG_ASSERT
#define DBG_ASSERT(expression)
#define DBG_ASSERT()
#endif // FTR_DBG_ASSERT

#ifdef FTR_HALT_ASSERT
#define HALT_ASSERT_E(expression) \
    if (expression) { \
        cout << std::dec << "HALT ASSERTION: " << __FILE__ << " " << __LINE__ <<  endl; \
        while(1); \
    }
#define HALT_ASSERT() \
    cout << std::dec << "DEBUG ASSERTION: " << __FILE__ << " " << __LINE__ <<  endl; \
    while(1);
#else // !FTR_HALT_ASSERT
#define HALT_ASSERT(expression)
#define HALT_ASSERT()
#endif // FTR_HALT_ASSERT

// General utility macro to get a particular bit from a value
#define GET_BIT(value, bit) ((value >> bit) & 1)
// General utility macro to get a particular range of bits from a value
#define GET_BITS(value, bit, mask) ((value >> bit) & mask)
// General utility macro to set a particular mask to a value
#define SET_BITS(value, bit, mask) (value |= (mask << bit))

// General utility macro to extend the sign starting from a particular bit up to a particular bit
// Always specify the type you want to extend to before using the macro:
//
// shalf_t signed_half = (shalf_t)SIGN_EXT(value, 4, 15);
//
#define SIGN_EXT(value, bit_sign, bit_end) ((sword_t) (value << (bit_end - bit_sign)) >> (bit_end - bit_sign))

// Macro used for couting a hex value with 0 setfill, of n digits
#define HEX_COUT(width) std::hex << std::setfill('0') << std::setw(width)
#define DEC_COUT(width) std::dec << std::setfill('0') << std::setw(width)

// Macro that describes the condition a <= n < b
#define IS_BETWEEN(a, b, n) n >= a && n < b