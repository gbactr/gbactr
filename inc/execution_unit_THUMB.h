#include "defines.h"
#include "types.h"
#include "regfile.h"
#include "mem.h"

class ExecutionUnitTHUMB {
    protected:
        // Constructor
        ExecutionUnitTHUMB();
        // Singleton pointer
        static ExecutionUnitTHUMB *executionUnitTHUMB;
    public:
        // Singleton pointer getter
        static ExecutionUnitTHUMB *getInstance();

        // Executes an instruction
        void executeInstruction(instr_THUMB_st instr);
    private:
        // Sets the CPSR according to the logical instructions conditions
        void setLogicalInstrCPSR(instr_THUMB_st instr, word_t result);
        // Sets the CPSR according to the arithmetical instructions conditions
        void setArithmeticalInstrCPSR(instr_THUMB_st instr, word_t op1, word_t op2, word_t result, bool carry);
        // Sets the CPSR according to the multiply instructions conditions
        void setMultiplyInstrCPSR(instr_THUMB_st instr, word_t result);
        void executeTHUMB_ADC(instr_THUMB_st instr);
        void executeTHUMB_ADD(instr_THUMB_st instr);
        void executeTHUMB_AND(instr_THUMB_st instr);
        void executeTHUMB_ASR(instr_THUMB_st instr);
        void executeTHUMB_B(instr_THUMB_st instr);
        void executeTHUMB_Bxx(instr_THUMB_st instr);
        void executeTHUMB_BIC(instr_THUMB_st instr);
        void executeTHUMB_BL(instr_THUMB_st instr);
        void executeTHUMB_BX(instr_THUMB_st instr);
        void executeTHUMB_CMN(instr_THUMB_st instr);
        void executeTHUMB_CMP(instr_THUMB_st instr);
        void executeTHUMB_EOR(instr_THUMB_st instr);
        void executeTHUMB_LDMIA(instr_THUMB_st instr);
        void executeTHUMB_LDR(instr_THUMB_st instr);
        void executeTHUMB_LDRB(instr_THUMB_st instr);
        void executeTHUMB_LDRH(instr_THUMB_st instr);
        void executeTHUMB_LSL(instr_THUMB_st instr);
        void executeTHUMB_LDSB(instr_THUMB_st instr);
        void executeTHUMB_LDSH(instr_THUMB_st instr);
        void executeTHUMB_LSR(instr_THUMB_st instr);
        void executeTHUMB_MOV(instr_THUMB_st instr);
        void executeTHUMB_MUL(instr_THUMB_st instr);
        void executeTHUMB_MVN(instr_THUMB_st instr);
        void executeTHUMB_NEG(instr_THUMB_st instr);
        void executeTHUMB_ORR(instr_THUMB_st instr);
        void executeTHUMB_POP(instr_THUMB_st instr);
        void executeTHUMB_PUSH(instr_THUMB_st instr);
        void executeTHUMB_ROR(instr_THUMB_st instr);
        void executeTHUMB_SBC(instr_THUMB_st instr);
        void executeTHUMB_STMIA(instr_THUMB_st instr);
        void executeTHUMB_STR(instr_THUMB_st instr);
        void executeTHUMB_STRB(instr_THUMB_st instr);
        void executeTHUMB_STRH(instr_THUMB_st instr);
        void executeTHUMB_SWI(instr_THUMB_st instr);
        void executeTHUMB_SUB(instr_THUMB_st instr);
        void executeTHUMB_TST(instr_THUMB_st instr);
        Regfile *regfile;
        Mem *mem;
};