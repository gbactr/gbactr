#include "defines.h"
#include "types.h"
#include "mem.h"
#include "regfile.h"
#pragma once

class FetchUnit {
    protected:
        // Constructor
        FetchUnit();
        // Singleton pointer
        static FetchUnit *fetchUnit;
    public:
        // Singleton pointer getter
        static FetchUnit *getInstance();

        // Fetches an instruction
        word_t fetchInstruction();
        half_t fetchInstructionTHUMB();
    private:
        Mem *mem;
        Regfile *regfile;
};