#include "defines.h"
#include "types.h"
#include "regfile.h"
#include "mem.h"

class ExecutionUnit {
    protected:
        // Constructor
        ExecutionUnit();
        // Singleton pointer
        static ExecutionUnit *executionUnit;
    public:
        // Singleton pointer getter
        static ExecutionUnit *getInstance();

        // Executes an instruction
        void executeInstruction(instr_st instr);
    private:
        // Checks if the condition code is fulfilled according to CPSR
        bool isConditionFulfilled(condition_e cond_code);
        // Sets the CPSR according to the logical instructions conditions
        void setLogicalInstrCPSR(instr_st instr, word_t result);
        // Sets the CPSR according to the arithmetical instructions conditions
        void setArithmeticalInstrCPSR(instr_st instr, word_t op1, word_t op2, word_t result, bool carry);
        // Sets the CPSR according to the multiply instructions conditions
        void setMultiplyInstrCPSR(instr_st instr, word_t result);
        // Sets the CPSR according to the multiply long instructions conditions
        void setMultiplyLongInstrCPSR(instr_st instr, dword_t result);
        void executeADC(instr_st instr);
        void executeADD(instr_st instr);
        void executeAND(instr_st instr);
        void executeB(instr_st instr);
        void executeBIC(instr_st instr);
        void executeBL(instr_st instr);
        void executeBX(instr_st instr);
        void executeCDP(instr_st instr);
        void executeCMN(instr_st instr);
        void executeCMP(instr_st instr);
        void executeEOR(instr_st instr);
        void executeLDC(instr_st instr);
        void executeLDM(instr_st instr);
        void executeLDR(instr_st instr);
        void executeLDRH(instr_st instr);
        void executeMCR(instr_st instr);
        void executeMLA(instr_st instr);
        void executeMLAL(instr_st instr);
        void executeMOV(instr_st instr);
        void executeMRC(instr_st instr);
        void executeMRS(instr_st instr);
        void executeMSR(instr_st instr);
        void executeMUL(instr_st instr);
        void executeMULL(instr_st instr);
        void executeMVN(instr_st instr);
        void executeORR(instr_st instr);
        void executeRSB(instr_st instr);
        void executeRSC(instr_st instr);
        void executeSBC(instr_st instr);
        void executeSTC(instr_st instr);
        void executeSTM(instr_st instr);
        void executeSTR(instr_st instr);
        void executeSTRH(instr_st instr);
        void executeSUB(instr_st instr);
        void executeSWI(instr_st instr);
        void executeSWP(instr_st instr);
        void executeTEQ(instr_st instr);
        void executeTST(instr_st instr);
        void executeUND(instr_st instr);
        Regfile *regfile;
        Mem *mem;
};