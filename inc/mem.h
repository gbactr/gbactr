#include "defines.h"
#include "mem_req.h"
#include "gamepak.h"
#include "bios.h"
#include "io_regs.h"
#include "wram.h"
#ifdef PRINT_INSTRUCTION
#include <iostream>
#endif // PRINT_INSTRUCTION
#pragma once

class DMA; // Forward declaration

/**
 * Memory interface. This module takes in all memory requests and reroutes them to all
 * memory-mapped components.
 */

class Mem{
    protected:
        // Constructor
        Mem();
        // Singleton pointer
        static Mem*mem;
    public:
        // Singleton pointer getter
        static Mem*getInstance();

        // Memory request
        void request(MemReq *request);
    private:
        Gamepak *gamepak;
        BIOS *bios;
        WRAM *wram1;
        WRAM *wram2;
        IORegs *ioRegs;
        DMA *dma;
};