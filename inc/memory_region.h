#include "defines.h"
#include "types.h"
#include "mem_req.h"
#pragma once

using namespace std;

class MemoryRegion {
    public:
        // Constructor
        MemoryRegion();
        MemoryRegion(int _size, int _baseAddress);

        // Allocate the memory
        void allocateMem();
        // Memory request
        void request(MemReq *request);
    
    // Only subclasses can access these variables
    protected:
        byte_t *memory;
        int size, baseAddress;
};