#include "gamepak.h"
using namespace std;

Gamepak::Gamepak() {
    // Constructor
    size = GAMEPAK_SIZE;
    baseAddress = GAMEPAK_BASE_ADDRESS;
    allocateMem();

    mapRom((char *)GAME_NAME);
}

// Singleton
Gamepak*Gamepak::gamepak = nullptr;

Gamepak*Gamepak::getInstance() {
    if (gamepak == nullptr) {
        gamepak = new Gamepak();
    }
    return gamepak;
}

void Gamepak::request(MemReq *request) {
    // Can't write to ROM
    DBG_ASSERT_E(request->isWrite());

    // Call super method
    MemoryRegion::request(request);
}

void Gamepak::mapRom(char * gameName) {
    ifstream gameFile;
    gameFile.open( gameName, ios::in|ios::binary|ios::ate );
    HALT_ASSERT_E(!gameFile.is_open());
    gameFile.seekg(0, ios::end); // pointer to the end
    int gameFileSize = gameFile.tellg(); // length of game rom
    gameFile.seekg(0, ios::beg); // Pointer to the beginning

    gameFile.read((char *)memory, gameFileSize); // Read ROM
}