#include "decoder.h"
#include <iomanip>
using namespace std;

Decoder::Decoder() {
    // Constructor
    regfile = Regfile::getInstance();
}

// Singleton
Decoder *Decoder::decoder = nullptr;

Decoder *Decoder::getInstance() {
    if (decoder == nullptr) {
        decoder = new Decoder();
    }
    return decoder;
}

/*
    Decoding flow:
    decodeInstruction(): fetched instruction is provided.
    decodeCondition(): The condition is decoded and saved into the decoded result.
    decodeType(): The type of instruction (Figure 4-1) is decoded and saved into the decoded result.
    decode[TYPE](): The instruction type is decoded and all the characteristics are saved into the decoded result.
    The decoded result is returned as instr_st
*/

instr_st Decoder::decodeInstruction(word_t fetch_instr) {
    instr_st decoded_instr;

    // Decode the condition field
    decoded_instr.condition = decodeCondition(fetch_instr);
    // Decode the instruction type
    decoded_instr.type = decodeType(fetch_instr);
    // Decode type-specific information
    switch (decoded_instr.type) {
        case DATA_PROCESSING:
            decodeDataProcessing(fetch_instr, &decoded_instr);
            break;
        case MULTIPLY:
            decodeMultiply(fetch_instr, &decoded_instr);
            break;
        case MULTIPLY_LONG:
            decodeMultiplyLong(fetch_instr, &decoded_instr);
            break;
        case SINGLE_DATA_SWAP:
            decodeSingleDataSwap(fetch_instr, &decoded_instr);
            break;
        case BRANCH_AND_EXCHANGE:
            decodeBranchAndExchange(fetch_instr, &decoded_instr);
            break;
        case HALFWORD_DATA_TRANSFER_REG:
            decodeHalfwordDataTransferReg(fetch_instr, &decoded_instr);
            break;
        case HALFWORD_DATA_TRANSFER_IMM:
            decodeHalfwordDataTransferImm(fetch_instr, &decoded_instr);
            break;
        case SINGLE_DATA_TRANSFER:
            decodeSingleDataTransfer(fetch_instr, &decoded_instr);
            break;
        case UNDEFINED_INSTR:
            decodeUndefined(fetch_instr, &decoded_instr);
            break;
        case BLOCK_DATA_TRANSFER:
            decodeBlockDataTransfer(fetch_instr, &decoded_instr);
            break;
        case BRANCH:
            decodeBranch(fetch_instr, &decoded_instr);
            break;
        case COPROCESSOR_DATA_TRANSFER:
            decodeCoprocessorDataTransfer(fetch_instr, &decoded_instr);
            break;
        case COPROCESSOR_DATA_OPERATION:
            decodeCoprocessorDataOperation(fetch_instr, &decoded_instr);
            break;
        case COPROCESSOR_REGISTER_TRANSFER:
            decodeCoprocessorRegisterTransfer(fetch_instr, &decoded_instr);
            break;
        case SOFTWARE_INTERRUPT:
            decodeSoftwareInterrupt(fetch_instr, &decoded_instr);
            break;
        default:
            HALT_ASSERT();
    }
    return decoded_instr;
}

condition_e Decoder::decodeCondition(word_t fetch_instr) {
    return (condition_e) getRange(fetch_instr, CONDITION_LSB, CONDITION_MSB);
}

instr_type_e Decoder::decodeType(word_t fetch_instr) {
    if (checkBitmask(fetch_instr, OP1_LSB, MULTIPLY_OP1_MASK, MULTIPLY_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, MULTIPLY_OP2_MASK, MULTIPLY_OP2)) {
        return MULTIPLY;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, MULTIPLY_LONG_OP1_MASK, MULTIPLY_LONG_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, MULTIPLY_LONG_OP2_MASK, MULTIPLY_LONG_OP2)) {
        return MULTIPLY_LONG;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, SINGLE_DATA_SWAP_OP1_MASK, SINGLE_DATA_SWAP_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, SINGLE_DATA_SWAP_OP2_MASK, SINGLE_DATA_SWAP_OP2)) {
        return SINGLE_DATA_SWAP;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, BRANCH_AND_EXCHANGE_OP1_MASK, BRANCH_AND_EXCHANGE_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, BRANCH_AND_EXCHANGE_OP2_MASK, BRANCH_AND_EXCHANGE_OP2) &&
        checkBitmask(fetch_instr, OP3_LSB, BRANCH_AND_EXCHANGE_OP3_MASK, BRANCH_AND_EXCHANGE_OP3)) {
        return BRANCH_AND_EXCHANGE;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, HALFWORD_DATA_TRANSFER_REG_OP1_MASK, HALFWORD_DATA_TRANSFER_REG_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, HALFWORD_DATA_TRANSFER_REG_OP2_MASK, HALFWORD_DATA_TRANSFER_REG_OP2)) {
        return HALFWORD_DATA_TRANSFER_REG;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, HALFWORD_DATA_TRANSFER_IMM_OP1_MASK, HALFWORD_DATA_TRANSFER_IMM_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, HALFWORD_DATA_TRANSFER_IMM_OP2_MASK, HALFWORD_DATA_TRANSFER_IMM_OP2)) {
        return HALFWORD_DATA_TRANSFER_IMM;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, DATA_PROCESSING_OP1_MASK, DATA_PROCESSING_OP1)) {
        return DATA_PROCESSING;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, SINGLE_DATA_TRANSFER_OP1_MASK, SINGLE_DATA_TRANSFER_OP1)) {
        return SINGLE_DATA_TRANSFER;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, UNDEFINED_OP1_MASK, UNDEFINED_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, UNDEFINED_OP2_MASK, UNDEFINED_OP2)) {
        return UNDEFINED_INSTR;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, BLOCK_DATA_TRANSFER_OP1_MASK, BLOCK_DATA_TRANSFER_OP1)) {
        return BLOCK_DATA_TRANSFER;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, BRANCH_OP1_MASK, BRANCH_OP1)) {
        return BRANCH;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, COPROCESSOR_DATA_TRANSFER_OP1_MASK, COPROCESSOR_DATA_TRANSFER_OP1)) {
        return COPROCESSOR_DATA_TRANSFER;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, COPROCESSOR_DATA_OPERATION_OP1_MASK, COPROCESSOR_DATA_OPERATION_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, COPROCESSOR_DATA_OPERATION_OP2_MASK, COPROCESSOR_DATA_OPERATION_OP2)) {
        return COPROCESSOR_DATA_OPERATION;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, COPROCESSOR_REGISTER_TRANSFER_OP1_MASK, COPROCESSOR_REGISTER_TRANSFER_OP1) &&
        checkBitmask(fetch_instr, OP2_LSB, COPROCESSOR_REGISTER_TRANSFER_OP2_MASK, COPROCESSOR_REGISTER_TRANSFER_OP2)) {
        return COPROCESSOR_REGISTER_TRANSFER;
    }
    if (checkBitmask(fetch_instr, OP1_LSB, SOFTWARE_INTERRUPT_OP1_MASK, SOFTWARE_INTERRUPT_OP1)) {
        return SOFTWARE_INTERRUPT;
    }
    DBG_ASSERT();
    return (instr_type_e) 0; // Just to mitigate warning
}

void Decoder::decodeBranchAndExchange(word_t fetch_instr, instr_st *decoded_instr) {
    byte_t Rn = getRange(fetch_instr, BRANCH_AND_EXCHANGE_RN_LSB, BRANCH_AND_EXCHANGE_RN_MSB);
    // R15 use is undefined
    DBG_ASSERT_E(Rn == PC);

    word_t op1 = regfile->readGPR(Rn);

    decoded_instr->name = BX;
    decoded_instr->r1 = Rn;
    decoded_instr->op1 = op1;
}

void Decoder::decodeBranch(word_t fetch_instr, instr_st *decoded_instr) {
    bool L = GET_BIT(fetch_instr, BRANCH_L);

    word_t op1 = getRange(fetch_instr, BRANCH_OFFSET_LSB, BRANCH_OFFSET_MSB);

    if (L)
        decoded_instr->name = BL;
    else {
        decoded_instr->name = B;
    }
    decoded_instr->op1 = op1;
}

void Decoder::decodeDataProcessing(word_t fetch_instr, instr_st *decoded_instr) {
    // TODO: PSR instructions may need additional special cases

    // Get Fields
    condition_e cond = (condition_e) getRange(fetch_instr, CONDITION_LSB, CONDITION_MSB);
    bool I = GET_BIT(fetch_instr, DATA_PROCESSING_I);
    data_processing_opcode_e opcode = (data_processing_opcode_e) getRange(fetch_instr, DATA_PROCESSING_OPCODE_LSB, DATA_PROCESSING_OPCODE_MSB);
    bool P = GET_BIT(fetch_instr, DATA_PROCESSING_P);
    bool S = GET_BIT(fetch_instr, DATA_PROCESSING_S);
    byte_t Rn = getRange(fetch_instr, DATA_PROCESSING_RN_LSB, DATA_PROCESSING_RN_MSB);
    byte_t Rd = getRange(fetch_instr, DATA_PROCESSING_RD_LSB, DATA_PROCESSING_RD_MSB);
    byte_t Rm = getRange(fetch_instr, DATA_PROCESSING_OPERAND2_LSB, DATA_PROCESSING_PSR_RM_MSB);
    word_t operand2 = getRange(fetch_instr, DATA_PROCESSING_OPERAND2_LSB, DATA_PROCESSING_OPERAND2_MSB);
    word_t operand2_psr = getRange(fetch_instr, DATA_PROCESSING_PSR_OPERAND2_LSB, DATA_PROCESSING_OPERAND2_MSB);
    word_t op1 = regfile->readGPR(Rn);
    if (Rn == PC) {
        op1 += 4;
    }
    word_t op2 = 0;
    bool C = false;
    bool prevC = regfile->readCPSR_C();

    // Decode Operand 2
    if (!I) {
        // Operand 2 is a shifted register
        Rm = getRange(operand2, OPERAND2_RM_LSB, OPERAND2_RM_MSB);
        word_t shift_value = regfile->readGPR(Rm);

        bool perform_shift;
        byte_t shift_amount;
        operand2_shift_type_e shift_type = (operand2_shift_type_e) getRange(operand2, OPERAND2_SHIFT_TYPE_LSB, OPERAND2_SHIFT_TYPE_MSB);
        bool explicit_shift = !GET_BIT(operand2, OPERAND2_EXPLICIT_SHIFT);
        if (explicit_shift) {
            // Shift amount is explicit in the instruction
            if (Rm == PC) shift_value += 4; // Normally + 8, but already prefetched
            perform_shift = true;
            shift_amount = getRange(operand2, OPERAND2_SHIFT_AMOUNT_LSB, OPERAND2_SHIFT_AMOUNT_MSB);
        } else {
            // Shift amount is in a register
            if (Rm == PC) shift_value += 8; // Normally + 12, but already prefetched
            byte_t Rs = getRange(operand2, OPERAND2_SHIFT_REG_LSB, OPERAND2_SHIFT_REG_MSB);
            DBG_ASSERT_E(Rs == PC); // Rs can't be R15
            shift_amount = regfile->readGPR(Rs);
            if (shift_amount == 0) perform_shift = false;
            else perform_shift = true;
        }

        // Perform the shift
        if (perform_shift) {
            dword_t long_shift_value = 0;
            switch (shift_type) {
                case LOGICAL_LEFT:
                    op2 = shift_value << shift_amount;
                    if (shift_amount == 0) {
                        C = prevC;
                    } else {
                        C = GET_BIT(shift_value, 31 - shift_amount + 1);
                    }
                    break;
                case LOGICAL_RIGHT:
                    if (shift_amount == 0) shift_amount = 32;
                    op2 = shift_value >> shift_amount;
                    if (shift_amount > 32) C = false;
                    else C = GET_BIT(shift_value, shift_amount - 1);
                    break;
                case ARITHMETIC_RIGHT:
                    if (shift_amount == 0) shift_amount = 32;
                    op2 = (sword_t) shift_value >> shift_amount;
                    // ASR of 32 or higher sets C to bit 31 of shift value
                    if (shift_amount >= 32) C = GET_BIT(shift_value, 31);
                    else C = GET_BIT(shift_value, shift_amount - 1);
                    break;
                case ROTATE_RIGHT:
                    // Special case, RRX
                    if (shift_amount == 0) {
                        shift_amount = 1;
                        long_shift_value = (dword_t) prevC << 32;
                        long_shift_value |= shift_value;
                        op2 = long_shift_value >> shift_amount;
                        C = GET_BIT(shift_value, 0);
                    } else {
                        shift_amount %= 32;
                        if (shift_amount == 0) shift_amount = 32; // Different than RRX 0 encoding
                        long_shift_value = (dword_t) shift_value << 32;
                        op2 = shift_value >> shift_amount;
                        op2 |= long_shift_value >> shift_amount;
                        C = GET_BIT(shift_value, shift_amount - 1); // shift_amount is always > 0
                    }
                    break;
                default:
                    DBG_ASSERT();
            }
        } else {
            op2 = shift_value;
            C = prevC;
        }
    } else {
        // Operand 2 is an immediate
        word_t imm = getRange(operand2, OPERAND2_IMM_LSB, OPERAND2_IMM_MSB);
        word_t rotate_amount = getRange(operand2, OPERAND2_ROTATE_LSB, OPERAND2_ROTATE_MSB);
        dword_t long_imm = (dword_t) imm << 32;
        rotate_amount <<= 1; // op2 = rot(imm, 2*rotAmount)

        //TODO: Does this affect C flag?
        op2 = imm >> rotate_amount;
        op2 |= long_imm >> rotate_amount;
    }

    switch (opcode) {
        case OP_AND: decoded_instr->name = AND; break;
        case OP_EOR: decoded_instr->name = EOR; break;
        case OP_SUB: decoded_instr->name = SUB; break;
        case OP_RSB: decoded_instr->name = RSB; break;
        case OP_ADD: decoded_instr->name = ADD; break;
        case OP_ADC: decoded_instr->name = ADC; break;
        case OP_SBC: decoded_instr->name = SBC; break;
        case OP_RSC: decoded_instr->name = RSC; break;
        case OP_TST: decoded_instr->name = TST; break;
        case OP_TEQ: decoded_instr->name = TEQ; break;
        case OP_CMP: decoded_instr->name = CMP; break;
        case OP_CMN: decoded_instr->name = CMN; break;
        case OP_ORR: decoded_instr->name = ORR; break;
        case OP_MOV: decoded_instr->name = MOV; break;
        case OP_BIC: decoded_instr->name = BIC; break;
        case OP_MVN: decoded_instr->name = MVN; break;
        default: DBG_ASSERT();
    }

    // Special cases: PSR transfer instructions, to be optimized
    if ((opcode == OP_TST || opcode == OP_CMP) && !S && Rn == 0xf && operand2 == 0) {
        decoded_instr->name = MRS;
        DBG_ASSERT_E(Rd == PC); // Rd can't be R15
    }

    if ((opcode == OP_TEQ || opcode == OP_CMN) && !S && Rn == 0x9 && Rd == 0xf && operand2_psr == 0) {
        decoded_instr->name = MSR;
        DBG_ASSERT_E(Rm == PC); // Rm can't be R15
        op2 = regfile->readGPR(Rm);
    }

    if ((opcode == OP_TEQ || opcode == OP_CMN) && !S && Rn == 0x8 && Rd == 0xf) {
        decoded_instr->name = MSR;
    }

    decoded_instr->setCondition = S;
    decoded_instr->r1 = Rn;
    decoded_instr->r2 = Rm;
    decoded_instr->rd = Rd;
    decoded_instr->isImm = I;
    decoded_instr->isSPSR = P;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->C = C;
}

void Decoder::decodeMultiply(word_t fetch_instr, instr_st *decoded_instr) {
    bool A = GET_BIT(fetch_instr, MULTIPLY_A);
    bool S = GET_BIT(fetch_instr, MULTIPLY_S);
    byte_t Rd = getRange(fetch_instr, MULTIPLY_RD_LSB, MULTIPLY_RD_MSB);
    byte_t Rn = getRange(fetch_instr, MULTIPLY_RN_LSB, MULTIPLY_RN_MSB);
    byte_t Rs = getRange(fetch_instr, MULTIPLY_RS_LSB, MULTIPLY_RS_MSB);
    byte_t Rm = getRange(fetch_instr, MULTIPLY_RM_LSB, MULTIPLY_RM_MSB);

    DBG_ASSERT_E(Rd == Rm); // Rd can't be the same as Rm
    DBG_ASSERT_E((Rd == PC) || (Rm == PC) || (Rn == PC) || (Rs == PC)); // No operand can be R15

    word_t op1 = regfile->readGPR(Rn);
    word_t op2 = regfile->readGPR(Rm);
    word_t op3 = regfile->readGPR(Rs);

    if (A) {
        decoded_instr->name = MLA;
    } else {
        decoded_instr->name = MUL;
    }
    decoded_instr->setCondition = S;
    decoded_instr->r1 = Rn;
    decoded_instr->r2 = Rm;
    decoded_instr->r3 = Rs;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->op3 = op3;
}

void Decoder::decodeMultiplyLong(word_t fetch_instr, instr_st *decoded_instr) {
    bool U = GET_BIT(fetch_instr, MULTIPLY_LONG_U);
    bool A = GET_BIT(fetch_instr, MULTIPLY_LONG_A);
    bool S = GET_BIT(fetch_instr, MULTIPLY_LONG_S);
    byte_t RdHi = getRange(fetch_instr, MULTIPLY_LONG_RDHI_LSB, MULTIPLY_LONG_RDHI_MSB);
    byte_t RdLo = getRange(fetch_instr, MULTIPLY_LONG_RDLO_LSB, MULTIPLY_LONG_RDLO_MSB);
    byte_t Rs = getRange(fetch_instr, MULTIPLY_LONG_RS_LSB, MULTIPLY_LONG_RS_MSB);
    byte_t Rm = getRange(fetch_instr, MULTIPLY_LONG_RM_LSB, MULTIPLY_LONG_RM_MSB);

    DBG_ASSERT_E((RdHi == RdLo) || (RdLo == Rs) || (RdHi == Rs)); // RdHi, RdLo and Rs must be different
    DBG_ASSERT_E((RdHi == PC) || (RdLo == PC) || (Rm == PC) || (Rs == PC)); // No operand can be R15

    word_t op2 = regfile->readGPR(Rm);
    word_t op3 = regfile->readGPR(Rs);

    if (A) {
        decoded_instr->name = MLAL;
    } else {
        decoded_instr->name = MULL;
    }
    decoded_instr->setCondition = S;
    decoded_instr->isUns = U;
    decoded_instr->r2 = Rm;
    decoded_instr->r3 = Rs;
    decoded_instr->rd = RdLo;
    decoded_instr->rdHi = RdHi;
    decoded_instr->op2 = op2;
    decoded_instr->op3 = op3;
}

void Decoder::decodeSingleDataTransfer(word_t fetch_instr, instr_st *decoded_instr) {
    bool I = GET_BIT(fetch_instr, SINGLE_DATA_TRANSFER_I);
    bool P = GET_BIT(fetch_instr, SINGLE_DATA_TRANSFER_P);
    bool U = GET_BIT(fetch_instr, SINGLE_DATA_TRANSFER_U);
    bool B = GET_BIT(fetch_instr, SINGLE_DATA_TRANSFER_B);
    bool W = GET_BIT(fetch_instr, SINGLE_DATA_TRANSFER_W);
    bool L = GET_BIT(fetch_instr, SINGLE_DATA_TRANSFER_L);
    byte_t Rn = getRange(fetch_instr, SINGLE_DATA_TRANSFER_RN_LSB, SINGLE_DATA_TRANSFER_RN_MSB);
    byte_t Rd = getRange(fetch_instr, SINGLE_DATA_TRANSFER_RD_LSB, SINGLE_DATA_TRANSFER_RD_MSB);
    byte_t Rm = 0;

    DBG_ASSERT_E(W && (Rn == PC)); // If W is set, Rn can't be R15

    word_t op1 = regfile->readGPR(Rn);
    if (Rn == PC) op1 += 4; // Normally +8, but prefetch done
    word_t op2 = 0;

    word_t operand2;
    bool C = false;
    bool prevC = regfile->readCPSR_C();
    // Decode Operand 2
    // TODO: This in a single function
    if (I) {
        // Operand 2 is a shifted register
        Rm = getRange(operand2, OPERAND2_RM_LSB, OPERAND2_RM_MSB);
        word_t shift_value = regfile->readGPR(Rm);
        DBG_ASSERT_E(Rm == PC); // The register offset can't be R15

        byte_t shift_amount;
        operand2_shift_type_e shift_type = (operand2_shift_type_e) getRange(operand2, OPERAND2_SHIFT_TYPE_LSB, OPERAND2_SHIFT_TYPE_MSB);
        // Shift amount is explicit in the instruction
        shift_amount = getRange(operand2, OPERAND2_IMM_LSB, OPERAND2_IMM_MSB);
        // Perform the shift
        dword_t long_shift_value = 0;
        switch (shift_type) {
            case LOGICAL_LEFT:
                op2 = shift_value << shift_amount;
                if (shift_amount == 0) {
                    C = prevC;
                } else {
                    C = GET_BIT(shift_value, 31 - shift_amount + 1);
                }
                break;
            case LOGICAL_RIGHT:
                if (shift_amount == 0) shift_amount = 32;
                op2 = shift_value >> shift_amount;
                if (shift_amount > 32) C = false;
                else C = GET_BIT(shift_value, shift_amount - 1);
                break;
            case ARITHMETIC_RIGHT:
                if (shift_amount == 0) shift_amount = 32;
                op2 = (sword_t) shift_value >> shift_amount;
                // ASR of 32 or higher sets C to bit 31 of shift value
                if (shift_amount >= 32) C = GET_BIT(shift_value, 31);
                else C = GET_BIT(shift_value, shift_amount - 1);
                break;
            case ROTATE_RIGHT:
                // Special case, RRX
                if (shift_amount == 0) {
                    shift_amount = 1;
                    long_shift_value = (dword_t) prevC << 32;
                    long_shift_value |= shift_value;
                    op2 = long_shift_value >> shift_amount;
                } else {
                    shift_amount %= 32;
                    if (shift_amount == 0) shift_amount = 32; // Different than RRX 0 encoding
                    long_shift_value = (dword_t) shift_value << 32;
                    op2 = shift_value >> shift_amount;
                    op2 |= long_shift_value >> shift_amount;
                    C = GET_BIT(shift_value, shift_amount - 1); // shift_amount is always > 0
                }
                break;
            default:
                DBG_ASSERT();
        }
    }
    else {
        // Operand 2 is an immediate
        op2 = getRange(fetch_instr, SINGLE_DATA_TRANSFER_OFFSET_LSB, SINGLE_DATA_TRANSFER_OFFSET_MSB);
    }

    if (L) {
        decoded_instr->name = LDR;
    } else {
        decoded_instr->name = STR;
    }
    decoded_instr->isWriteBack = W;
    decoded_instr->isByte = B;
    decoded_instr->isUp = U;
    decoded_instr->isPreIndexing = P;
    decoded_instr->isImm = !I;
    decoded_instr->C = C;
    decoded_instr->r1 = Rn;
    decoded_instr->r2 = Rm;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
}

void Decoder::decodeHalfwordDataTransferReg(word_t fetch_instr, instr_st *decoded_instr) {
    bool P = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_P);
    bool U = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_U);
    bool W = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_W);
    bool L = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_L);
    bool S = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_S);
    bool H = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_H);
    byte_t Rn = getRange(fetch_instr, HALFWORD_DATA_TRANSFER_RN_LSB, HALFWORD_DATA_TRANSFER_RN_MSB);
    byte_t Rd = getRange(fetch_instr, HALFWORD_DATA_TRANSFER_RD_LSB, HALFWORD_DATA_TRANSFER_RD_MSB);
    byte_t Rm = getRange(fetch_instr, HALFWORD_DATA_TRANSFER_RM_LSB, HALFWORD_DATA_TRANSFER_RM_MSB);

    DBG_ASSERT_E(W && (Rn == PC)); // If W is set, Rn can't be R15
    DBG_ASSERT_E(Rm == PC); // The register offset can't be R15

    word_t op1 = regfile->readGPR(Rn);
    word_t op2 = regfile->readGPR(Rm);

    if (L) {
        decoded_instr->name = LDRH;
    } else {
        decoded_instr->name = STRH;
    }
    decoded_instr->isWriteBack = W;
    decoded_instr->isUp = U;
    decoded_instr->isPreIndexing = P;
    decoded_instr->isImm = false;
    decoded_instr->isSigned = S;
    decoded_instr->isHalfword = H;
    decoded_instr->r1 = Rn;
    decoded_instr->r2 = Rm;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
}

// TODO: Single function with previous
void Decoder::decodeHalfwordDataTransferImm(word_t fetch_instr, instr_st *decoded_instr) {
    bool P = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_P);
    bool U = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_U);
    bool W = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_W);
    bool L = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_L);
    bool S = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_S);
    bool H = GET_BIT(fetch_instr, HALFWORD_DATA_TRANSFER_H);
    byte_t Rn = getRange(fetch_instr, HALFWORD_DATA_TRANSFER_RN_LSB, HALFWORD_DATA_TRANSFER_RN_MSB);
    byte_t Rd = getRange(fetch_instr, HALFWORD_DATA_TRANSFER_RD_LSB, HALFWORD_DATA_TRANSFER_RD_MSB);
    byte_t Rm = getRange(fetch_instr, OPERAND2_RM_LSB, OPERAND2_RM_MSB);

    word_t op1 = regfile->readGPR(Rn);
    byte_t op2 = getRange(fetch_instr, HALFWORD_DATA_TRANSFER_OFFSET1_LSB, HALFWORD_DATA_TRANSFER_OFFSET1_MSB);
    op2 <<= 4;
    op2 |= getRange(fetch_instr, HALFWORD_DATA_TRANSFER_OFFSET2_LSB, HALFWORD_DATA_TRANSFER_OFFSET2_MSB);

    if (L) {
        decoded_instr->name = LDRH;
    } else {
        decoded_instr->name = STRH;
    }
    decoded_instr->isWriteBack = W;
    decoded_instr->isUp = U;
    decoded_instr->isPreIndexing = P;
    decoded_instr->isImm = true;
    decoded_instr->isSigned = S;
    decoded_instr->isHalfword = H;
    decoded_instr->r1 = Rn;
    decoded_instr->r2 = Rm;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
}

void Decoder::decodeBlockDataTransfer(word_t fetch_instr, instr_st *decoded_instr) {
    bool P = GET_BIT(fetch_instr, BLOCK_DATA_TRANSFER_P);
    bool U = GET_BIT(fetch_instr, BLOCK_DATA_TRANSFER_U);
    bool S = GET_BIT(fetch_instr, BLOCK_DATA_TRANSFER_S);
    bool W = GET_BIT(fetch_instr, BLOCK_DATA_TRANSFER_W);
    bool L = GET_BIT(fetch_instr, BLOCK_DATA_TRANSFER_L);
    byte_t Rn = getRange(fetch_instr, BLOCK_DATA_TRANSFER_RN_LSB, BLOCK_DATA_TRANSFER_RN_MSB);
    DBG_ASSERT_E(Rn == PC); // Rn can't be R15
    // TODO: Use as op2
    half_t regList = getRange(fetch_instr, BLOCK_DATA_TRANSFER_REGLIST_LSB, BLOCK_DATA_TRANSFER_REGLIST_MSB);

    word_t op1 = regfile->readGPR(Rn);

    if (L) {
        decoded_instr->name = LDM;
    } else {
        decoded_instr->name = STM;
    }
    decoded_instr->isWriteBack = W;
    decoded_instr->isUp = U;
    decoded_instr->isPreIndexing = P;
    decoded_instr->isImm = false;
    decoded_instr->isPSR = S;
    decoded_instr->r1 = Rn;
    decoded_instr->op1 = op1;
    decoded_instr->regList = regList;
}

void Decoder::decodeSingleDataSwap(word_t fetch_instr, instr_st *decoded_instr) {
    bool B = GET_BIT(fetch_instr, SINGLE_DATA_SWAP_B);
    byte_t Rn = getRange(fetch_instr, SINGLE_DATA_SWAP_RN_LSB, SINGLE_DATA_SWAP_RN_MSB);
    byte_t Rd = getRange(fetch_instr, SINGLE_DATA_SWAP_RD_LSB, SINGLE_DATA_SWAP_RD_MSB);
    byte_t Rm = getRange(fetch_instr, SINGLE_DATA_SWAP_RM_LSB, SINGLE_DATA_SWAP_RM_MSB);

    DBG_ASSERT_E((Rm == PC) || (Rn == PC) || (Rd == PC)); // No operand can be R15

    word_t op1 = regfile->readGPR(Rn);
    word_t op2 = regfile->readGPR(Rm);

    decoded_instr->name = SWP;
    decoded_instr->isByte = B;
    decoded_instr->isImm = false;
    decoded_instr->r1 = Rn;
    decoded_instr->r2 = Rm;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
}

void Decoder::decodeSoftwareInterrupt(word_t fetch_instr, instr_st *decoded_instr) {
    word_t comment = getRange(fetch_instr, SWI_COMMENT_LSB, SWI_COMMENT_MSB);

    decoded_instr->name = SWI;
    decoded_instr->comment = comment;
}

void Decoder::decodeCoprocessorDataOperation(word_t fetch_instr, instr_st *decoded_instr) {
    decoded_instr->name = CDP;
}

void Decoder::decodeCoprocessorDataTransfer(word_t fetch_instr, instr_st *decoded_instr) {
    bool L = GET_BIT(fetch_instr, BLOCK_DATA_TRANSFER_L);

    if (L) {
        decoded_instr->name = LDC;
    } else {
        decoded_instr->name = STC;
    }
}

void Decoder::decodeCoprocessorRegisterTransfer(word_t fetch_instr, instr_st *decoded_instr) {
    bool L = GET_BIT(fetch_instr, BLOCK_DATA_TRANSFER_L);

    if (L) {
        decoded_instr->name = MRC;
    } else {
        decoded_instr->name = MCR;
    }
}

void Decoder::decodeUndefined(word_t fetch_instr, instr_st *decoded_instr) {
    decoded_instr->name = UND;
}

bool Decoder::checkBitmask(word_t instr, byte_t field_lsb, byte_t mask, byte_t value) {
    byte_t field = (byte_t) (instr >> field_lsb);
    byte_t bitmask = field & mask;
    if (bitmask == value) return true;
    else return false;
}

word_t Decoder::getRange(word_t word, byte_t lsb, byte_t msb) {
    DBG_ASSERT_E(lsb > msb);
    word_t result = word << 31 - msb;
    result >>= (31 - msb) + lsb; // word_t is unsigned
    return result;
}
