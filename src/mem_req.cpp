#include "mem_req.h"
using namespace std;

MemReq::MemReq(MemReqType _type, addr_t _address) {
    // Constructor
    type = _type;
    data = 0;
    address = _address;
}

MemReq::MemReq(MemReqType _type, addr_t _address, word_t _data) {
    // Constructor
    type = _type;
    data = _data;
    address = _address;
}

MemReq::MemReq() {
    // Constructor
    data = 0;
}

bool MemReq::isRead(MemReqType type)  {
    if (type == READ_BYTE ||
        type == READ_HALF ||
        type == READ_WORD) {
            return true;
    }
    return false;
}

bool MemReq::isRead() {
    return isRead(type);
}

bool MemReq::isWrite(MemReqType type)  {
    if (type == WRITE_BYTE ||
        type == WRITE_HALF ||
        type == WRITE_WORD) {
            return true;
    }
    return false;
}

bool MemReq::isWrite() {
    return isWrite(type);
}

int MemReq::size(MemReqType type) {
    if (type == READ_BYTE || type == WRITE_BYTE)
        return 1;
    if (type == READ_HALF || type == WRITE_HALF)
        return 2;
    if (type == READ_WORD || type == WRITE_WORD)
        return 4;

    DBG_ASSERT();
    return 0; // Unreachable
}

int MemReq::size() {
    return size(type);
}