#include "memory_region.h"
using namespace std;

MemoryRegion::MemoryRegion() {
    // Dummy Constructor
    size = 4;
    baseAddress = 0;
    allocateMem();
}

MemoryRegion::MemoryRegion(int _size, int _baseAddress) {
    // Constructor
    // Memory sizes must be word aligned
    HALT_ASSERT_E(_size % 4 != 0);
    HALT_ASSERT_E(_baseAddress % 4 != 0);
    size = _size;
    baseAddress = _baseAddress;

    allocateMem();
}

void MemoryRegion::allocateMem() {
    memory = (byte_t *) malloc(sizeof(byte_t) * size);
}

void MemoryRegion::request(MemReq *request) {
    addr_t effectiveAddress = request->address - baseAddress;
    HALT_ASSERT_E(effectiveAddress >= (baseAddress + size));

    if (request->isRead()) {
        // TODO:
        // Each byte is accessed separately because misaligned reads are permitted
        // Misaligned reads result in rotation of the half/word, placing the byte
        // of the misaligned address into the lower 8 bits of the result
        word_t data = 0;
        if      (request->type == READ_BYTE) data = memory[effectiveAddress];
        else if (request->type == READ_HALF) {
            addr_t alignedAddress = (effectiveAddress / 2) * 2;
            int startByte = effectiveAddress - alignedAddress;
            data = memory[alignedAddress + startByte];
            data |= (memory[alignedAddress + ((startByte + 1) % 2)] << 8);
        }
        else if (request->type == READ_WORD) {
            addr_t alignedAddress = (effectiveAddress / 4) * 4;
            int startByte = effectiveAddress - alignedAddress;
            data = memory[alignedAddress + startByte];
            data |= (memory[effectiveAddress + ((startByte + 1) % 4)] << 8);
            data |= (memory[effectiveAddress + ((startByte + 2) % 4)] << 16);
            data |= (memory[effectiveAddress + ((startByte + 3) % 4)] << 24);
        }
        request->data = data;
    }
    else if (request->isWrite()) {
        // Write addresses are always aligned
        if (request->type == WRITE_BYTE) memory[effectiveAddress] = request->data;
        if (request->type == WRITE_HALF) ((half_t *)memory)[effectiveAddress/2] = request->data;
        if (request->type == WRITE_WORD) ((word_t *)memory)[effectiveAddress/4] = request->data;
    }
}