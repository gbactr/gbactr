#include "translog.h"
using namespace std;

TransLog::TransLog() {
    // Constructor
}

//Singleton
TransLog *TransLog::transLog = nullptr;

TransLog *TransLog::getInstance() {
    if (transLog == nullptr) {
        transLog = new TransLog();
    }
    return transLog;
}

void TransLog::transaction(instr_st commited_instr) {
    cout << "Commited instruction: " << endl <<
            "TYPE: " << decodeInstrType(commited_instr.type) << endl <<
            "NAME: " << decodeInstrName(commited_instr.name) << endl <<
            endl;
}

string TransLog::decodeInstrType(instr_type_e type) {
    string s;
    switch (type) {
        case (DATA_PROCESSING):               s = "DATA_PROCESSING"; break;
        case (MULTIPLY):                      s = "MULTIPLY"; break;
        case (MULTIPLY_LONG):                 s = "MULTIPLY_LONG"; break;
        case (SINGLE_DATA_SWAP):              s = "SINGLE_DATA_SWAP"; break;
        case (BRANCH_AND_EXCHANGE):           s = "BRANCH_AND_EXCHANGE"; break;
        case (HALFWORD_DATA_TRANSFER_REG):    s = "HALFWORD_DATA_TRANSFER_REG"; break;
        case (HALFWORD_DATA_TRANSFER_IMM):    s = "HALFWORD_DATA_TRANSFER_IMM"; break;
        case (SINGLE_DATA_TRANSFER):          s = "SINGLE_DATA_TRANSFER"; break;
        case (UNDEFINED_INSTR):               s = "UNDEFINED_INSTR"; break;
        case (BLOCK_DATA_TRANSFER):           s = "BLOCK_DATA_TRANSFER"; break;
        case (BRANCH):                        s = "BRANCH"; break;
        case (COPROCESSOR_DATA_TRANSFER):     s = "COPROCESSOR_DATA_TRANSFER"; break;
        case (COPROCESSOR_DATA_OPERATION):    s = "COPROCESSOR_DATA_OPERATION"; break;
        case (COPROCESSOR_REGISTER_TRANSFER): s = "COPROCESSOR_REGISTER_TRANSFER"; break;
        case (SOFTWARE_INTERRUPT):            s = "SOFTWARE_INTERRUPT"; break;
        default:
            DBG_ASSERT();
    }
    return s;
}

string TransLog::decodeInstrName(instr_name_e name) {
    string s;
    switch(name) {
    case (ADC): s = "ADC"; break;
    case (ADD): s = "ADD"; break;
    case (AND): s = "AND"; break;
    case (B): s = "B"; break;
    case (BIC): s = "BIC"; break;
    case (BL): s = "BL"; break;
    case (BX): s = "BX"; break;
    case (CDP): s = "CDP"; break;
    case (CMN): s = "CMN"; break;
    case (CMP): s = "CMP"; break;
    case (EOR): s = "EOR"; break;
    case (LDC): s = "LDC"; break;
    case (LDM): s = "LDM"; break;
    case (LDR): s = "LDR"; break;
    case (MCR): s = "MCR"; break;
    case (MLA): s = "MLA"; break;
    case (MLAL): s = "MLAL"; break;
    case (MOV): s = "MOV"; break;
    case (MRC): s = "MRC"; break;
    case (MRS): s = "MRS"; break;
    case (MSR): s = "MSR"; break;
    case (MUL): s = "MUL"; break;
    case (MULL): s = "MULL"; break;
    case (MVN): s = "MVN"; break;
    case (ORR): s = "ORR"; break;
    case (RSB): s = "RSB"; break;
    case (RSC): s = "RSC"; break;
    case (SBC): s = "SBC"; break;
    case (STC): s = "STC"; break;
    case (STM): s = "STM"; break;
    case (STR): s = "STR"; break;
    case (SUB): s = "SUB"; break;
    case (SWI): s = "SWI"; break;
    case (SWP): s = "SWP"; break;
    case (TEQ): s = "TEQ"; break;
    case (TST): s = "TST"; break;
    case (UND): s = "UND"; break;
    default:
        DBG_ASSERT();
    }
    return s;
}