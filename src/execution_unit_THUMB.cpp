#include "execution_unit_THUMB.h"
#include <iomanip>
using namespace std;

ExecutionUnitTHUMB::ExecutionUnitTHUMB() {
    // Constructor
    regfile = Regfile::getInstance();
    mem = Mem::getInstance();
}

// Singleton
ExecutionUnitTHUMB *ExecutionUnitTHUMB::executionUnitTHUMB = nullptr;

ExecutionUnitTHUMB *ExecutionUnitTHUMB::getInstance() {
    if (executionUnitTHUMB == nullptr) {
        executionUnitTHUMB = new ExecutionUnitTHUMB();
    }
    return executionUnitTHUMB;
}

void ExecutionUnitTHUMB::executeInstruction(instr_THUMB_st instr) {
    switch(instr.name) {
        case THUMB_ADC:   executeTHUMB_ADC(instr);   break;
        case THUMB_ADD:   executeTHUMB_ADD(instr);   break;
        case THUMB_AND:   executeTHUMB_AND(instr);   break;
        case THUMB_ASR:   executeTHUMB_ASR(instr);   break;
        case THUMB_B:     executeTHUMB_B(instr);     break;
        case THUMB_Bxx:   executeTHUMB_Bxx(instr);   break;
        case THUMB_BIC:   executeTHUMB_BIC(instr);   break;
        case THUMB_BL:    executeTHUMB_BL(instr);    break;
        case THUMB_BLH:   executeTHUMB_BL(instr);    break;
        case THUMB_BX:    executeTHUMB_BX(instr);    break;
        case THUMB_CMN:   executeTHUMB_CMN(instr);   break;
        case THUMB_CMP:   executeTHUMB_CMP(instr);   break;
        case THUMB_EOR:   executeTHUMB_EOR(instr);   break;
        case THUMB_LDMIA: executeTHUMB_LDMIA(instr); break;
        case THUMB_LDR:   executeTHUMB_LDR(instr);   break;
        case THUMB_LDRH:  executeTHUMB_LDRH(instr);  break;
        case THUMB_LSL:   executeTHUMB_LSL(instr);   break;
        case THUMB_LSR:   executeTHUMB_LSR(instr);   break;
        case THUMB_MOV:   executeTHUMB_MOV(instr);   break;
        case THUMB_MUL:   executeTHUMB_MUL(instr);   break;
        case THUMB_MVN:   executeTHUMB_MVN(instr);   break;
        case THUMB_NEG:   executeTHUMB_NEG(instr);   break;
        case THUMB_ORR:   executeTHUMB_ORR(instr);   break;
        case THUMB_POP:   executeTHUMB_POP(instr);   break;
        case THUMB_PUSH:  executeTHUMB_PUSH(instr);  break;
        case THUMB_ROR:   executeTHUMB_ROR(instr);   break;
        case THUMB_SBC:   executeTHUMB_SBC(instr);   break;
        case THUMB_STMIA: executeTHUMB_STMIA(instr); break;
        case THUMB_STR:   executeTHUMB_STR(instr);   break;
        case THUMB_STRH:  executeTHUMB_STRH(instr);  break;
        case THUMB_SWI:   executeTHUMB_SWI(instr);   break;
        case THUMB_SUB:   executeTHUMB_SUB(instr);   break;
        case THUMB_TST:   executeTHUMB_TST(instr);   break;
        default: DBG_ASSERT();
    }
}

/*
**********************
* CPSR set functions *
**********************
*/

void ExecutionUnitTHUMB::setLogicalInstrCPSR(instr_THUMB_st instr, word_t result) {
    // If rd is R15 and S is set, set CPSR to SPSR of current mode
    // TODO: Is this true in THUMB mode as well?
    if (instr.rd == PC) {
        PSR_t spsr = regfile->getSPSR();
        regfile->writeCPSR(spsr);
    } 
    else {
        // V flag remains unchanged
        // C flag is set to the carry out from the barrel shifter
        regfile->writeCPSR_C(instr.C);
        // Z flag is set if the result is all zeroes
        regfile->writeCPSR_Z(result == 0);
        // N flag is set to the logical value of bit 31 of the result
        regfile->writeCPSR_N(GET_BIT(result, 31));
    }
}

void ExecutionUnitTHUMB::setArithmeticalInstrCPSR(instr_THUMB_st instr, word_t op1, word_t op2, word_t result, bool carry) {
    // V flag records if an overflow happened
    if (GET_BIT(op1, 31) == GET_BIT(op2, 31)) {
        regfile->writeCPSR_V(GET_BIT(op1, 31) != GET_BIT(result, 31));
    }
    // C flag will be set to the carry out of bit 31 of the ALU
    regfile->writeCPSR_C(carry);
    // Z flag is set if the result is all zeroes
    regfile->writeCPSR_Z(result == 0);
    // N flag will be set to the value of bit 31 of the result
    regfile->writeCPSR_N(GET_BIT(result, 31));
}

void ExecutionUnitTHUMB::setMultiplyInstrCPSR(instr_THUMB_st instr, word_t result) {
    // N flag is set to bit 31 of the result
    regfile->writeCPSR_N(GET_BIT(result, 31));
    // Z flag is set if the result is zero
    regfile->writeCPSR_Z(result == 0);
}

/*
***********************************
* Instruction execution functions *
***********************************
*/

void ExecutionUnitTHUMB::executeTHUMB_ADC(instr_THUMB_st instr) {
    dword_t result = instr.op1 + instr.op2 + regfile->readCPSR_C();
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_ADD(instr_THUMB_st instr) {
    dword_t result = instr.op1 + instr.op2;
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_AND(instr_THUMB_st instr) {
    word_t result = instr.op1 & instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_ASR(instr_THUMB_st instr) {
    // The shift was done on decode
    word_t result = instr.op1;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}
void ExecutionUnitTHUMB::executeTHUMB_B(instr_THUMB_st instr) {
    regfile->writeGPR(PC, instr.op1);
}

void ExecutionUnitTHUMB::executeTHUMB_Bxx(instr_THUMB_st instr) {
    bool jump;
    PSR_t cpsr = regfile->getCPSR();
    switch (instr.branchCond) {
        case EQ: jump =  cpsr.Z;                       break;
        case NE: jump = !cpsr.Z;                       break;
        case CS: jump =  cpsr.C;                       break;
        case CC: jump = !cpsr.C;                       break;
        case MI: jump =  cpsr.N;                       break;
        case PL: jump = !cpsr.N;                       break;
        case VS: jump =  cpsr.V;                       break;
        case VC: jump = !cpsr.V;                       break;
        case HI: jump =  cpsr.C && !cpsr.Z;            break;
        case LS: jump = !cpsr.C ||  cpsr.Z;            break;
        case GE: jump =  cpsr.N ==  cpsr.V;            break;
        case LT: jump =  cpsr.N !=  cpsr.V;            break;
        case GT: jump = !cpsr.Z && (cpsr.N == cpsr.V); break;
        case LE: jump =  cpsr.Z || (cpsr.N != cpsr.V); break;
        default: DBG_ASSERT();
    }
    if (jump) regfile->writeGPR(PC, instr.op1);
}

void ExecutionUnitTHUMB::executeTHUMB_BIC(instr_THUMB_st instr) {
    word_t result = instr.op1 & (~instr.op2);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_BL(instr_THUMB_st instr) {
    if (instr.name == THUMB_BL) {
        regfile->writeGPR(LR, instr.op1);
    }
    else {
        regfile->writeGPR(LR, regfile->readGPR(PC) | 1); // Prefetch already done, force bit 0 set
        regfile->writeGPR(PC, instr.op1);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_BX(instr_THUMB_st instr) {
    word_t address = instr.op2 & (-2); // Clear bit 0
    cpu_mode_e cpu_mode = GET_BIT(instr.op1, 0) ? THUMB : ARM;

    regfile->writeGPR(PC, address);
    regfile->setCPUMode(cpu_mode);
}

void ExecutionUnitTHUMB::executeTHUMB_CMN(instr_THUMB_st instr) {
    dword_t result = instr.op1 + instr.op2;
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_CMP(instr_THUMB_st instr) {
    dword_t result = instr.op1 - instr.op2;
    bool carry = !GET_BIT(result, 32);

    if (instr.setCondCode) {
        setArithmeticalInstrCPSR(instr, instr.op1, -instr.op2, result, carry);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_EOR(instr_THUMB_st instr) {
    word_t result = instr.op1 ^ instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_LDMIA(instr_THUMB_st instr) {
    word_t regList = instr.regList;
    DBG_ASSERT_E(regList == 0);
    word_t result;
    addr_t address = instr.op1;
    word_t offset = 4;

    for (int i = 0 ; i != NUM_GPR; i++) {
        if (GET_BIT(regList, i)) {
            MemReq req(READ_WORD, address);
            mem->request(&req);
            regfile->writeGPR(i, req.data);
            address += offset;
        }
    }

    regfile->writeGPR(instr.r1, address);
}

void ExecutionUnitTHUMB::executeTHUMB_LDR(instr_THUMB_st instr) {
    addr_t address = instr.op2 + instr.op3; // base + offset
    
    MemReq req(instr.isByte ? READ_BYTE : READ_WORD, address);
    mem->request(&req);
    regfile->writeGPR(instr.rd, req.data);
}

void ExecutionUnitTHUMB::executeTHUMB_LDRH(instr_THUMB_st instr) {
    addr_t address = instr.op2 + instr.op3; // base + offset
    
    MemReq req(instr.isHalfword ? READ_HALF : READ_BYTE, address);
    mem->request(&req);

    word_t result = req.data;
    if (instr.isSigned) {
        result = SIGN_EXT(result, instr.isHalfword ? 15 : 7, 31);
    }
    regfile->writeGPR(instr.rd, result);
}

void ExecutionUnitTHUMB::executeTHUMB_LSL(instr_THUMB_st instr) {
    // The shift was done on decode
    word_t result = instr.op1;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_LSR(instr_THUMB_st instr) {
    // The shift was done on decode
    word_t result = instr.op1;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_MOV(instr_THUMB_st instr) {
    word_t result = instr.op1;
    regfile->writeGPR(instr.rd, result);

    // IT DOESN'T SET COND CODES?!
    //if (instr.setCondCode) {
    //    setLogicalInstrCPSR(instr, result);
    //}
}

void ExecutionUnitTHUMB::executeTHUMB_MUL(instr_THUMB_st instr) {
    word_t result = instr.op1 * instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setMultiplyInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_MVN(instr_THUMB_st instr) {
    word_t result = ~instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_NEG(instr_THUMB_st instr) {
    dword_t result = -instr.op2; // op1 = 0, this is implemented as op1 - op2
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setArithmeticalInstrCPSR(instr, 0, instr.op2, result, carry);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_ORR(instr_THUMB_st instr) {
    word_t result = instr.op1 | instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_POP(instr_THUMB_st instr) {
    word_t regList = instr.regList;
    DBG_ASSERT_E(regList == 0);
    word_t result;
    addr_t address = regfile->readGPR(SP);
    word_t offset = 4;

    for (int i = 0; i != NUM_GPR; i++) {
        if (GET_BIT(regList, i)) {
            MemReq req(READ_WORD, address);
            mem->request(&req);
            regfile->writeGPR(i, req.data);
            address += offset;
        }
    }

    regfile->writeGPR(SP, address);
}

void ExecutionUnitTHUMB::executeTHUMB_PUSH(instr_THUMB_st instr) {
    word_t regList = instr.regList;
    DBG_ASSERT_E(regList == 0);
    word_t result;
    addr_t address = regfile->readGPR(SP);
    word_t offset = -4;

    for (int i = NUM_GPR; i != 0; i--) {
        if (GET_BIT(regList, i)) {
            address += offset;
            word_t value = regfile->readGPR(i);
            MemReq req(WRITE_WORD, address, value);
            mem->request(&req);
        }
    }

    regfile->writeGPR(SP, address);
}

void ExecutionUnitTHUMB::executeTHUMB_ROR(instr_THUMB_st instr) {
    word_t result = instr.op1; // Already shifted on decode
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_SBC(instr_THUMB_st instr) {
    dword_t result = instr.op1 - instr.op2 - !regfile->readCPSR_C();
    bool carry = !GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setArithmeticalInstrCPSR(instr, instr.op1, -instr.op2, result, carry);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_STMIA(instr_THUMB_st instr) {
    word_t regList = instr.regList;
    DBG_ASSERT_E(regList == 0);
    word_t result;
    addr_t address = instr.op1;
    word_t offset = 4;

    for (int i = 0; i != NUM_GPR; i++) {
        if (GET_BIT(regList, i)) {
            word_t value = regfile->readGPR(i);
            MemReq req(WRITE_WORD, address, value);
            mem->request(&req);
            address += offset;
        }
    }

    regfile->writeGPR(instr.r1, address);
}

void ExecutionUnitTHUMB::executeTHUMB_STR(instr_THUMB_st instr) {
    addr_t address = instr.op2 + instr.op3; // base + offset
    
    MemReq req(instr.isByte ? WRITE_BYTE : WRITE_WORD, address, instr.op1);
    mem->request(&req);
}

void ExecutionUnitTHUMB::executeTHUMB_STRH(instr_THUMB_st instr) {
    addr_t address = instr.op2 + instr.op3; // base + offset
    
    MemReq req(WRITE_HALF, address, instr.op1);
    mem->request(&req);
}

void ExecutionUnitTHUMB::executeTHUMB_SWI(instr_THUMB_st instr) {

}

void ExecutionUnitTHUMB::executeTHUMB_SUB(instr_THUMB_st instr) {
    dword_t result = instr.op1 - instr.op2;
    bool carry = !GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondCode) {
        setArithmeticalInstrCPSR(instr, instr.op1, -instr.op2, result, carry);
    }
}

void ExecutionUnitTHUMB::executeTHUMB_TST(instr_THUMB_st instr) {
    word_t result = instr.op1 & instr.op2;

    if (instr.setCondCode) {
        setLogicalInstrCPSR(instr, result);
    }
}