#include "fetch_unit.h"
using namespace std;

FetchUnit::FetchUnit() {
    // Constructor
    mem = Mem::getInstance();
    regfile = Regfile::getInstance();
}

// Singleton
FetchUnit *FetchUnit::fetchUnit = nullptr;

FetchUnit *FetchUnit::getInstance() {
    if (fetchUnit == nullptr) {
        fetchUnit = new FetchUnit();
    }
    return fetchUnit;
}

word_t FetchUnit::fetchInstruction() {
    // Read fetch address from PC
    addr_t fetch_address = regfile->readGPR(PC);

    // Fetch instruction from memory
    MemReq fetchRequest(READ_WORD, fetch_address);
    mem->request(&fetchRequest);

    // Increment PC
    regfile->writeGPR(PC, fetch_address+4);

    // Return instruction
    return fetchRequest.data;
}

half_t FetchUnit::fetchInstructionTHUMB() {
    // Read fetch address from PC
    addr_t fetch_address = regfile->readGPR(PC);

    // Fetch instruction from memory
    MemReq fetchRequest(READ_HALF, fetch_address);
    mem->request(&fetchRequest);

    // Increment PC
    regfile->writeGPR(PC, fetch_address+2);

    // Return instruction
    return fetchRequest.data;
}