#include "bios.h"
using namespace std;

BIOS::BIOS() {
    // Constructor
    size = BIOS_SIZE;
    baseAddress = BIOS_BASE_ADDRESS;
    allocateMem();

    mapBIOS((char *)BIOS_NAME);
}

// Singleton
BIOS*BIOS::bios= nullptr;

BIOS*BIOS::getInstance() {
    if (bios == nullptr) {
        bios = new BIOS();
    }
    return bios;
}

void BIOS::request(MemReq *request) {
    // Can't write to ROM
    DBG_ASSERT_E(request->isWrite());

    // Call super method
    MemoryRegion::request(request);
}

void BIOS::mapBIOS(char * gameName) {
    ifstream gameFile;
    gameFile.open( gameName, ios::in|ios::binary|ios::ate );
    HALT_ASSERT_E(!gameFile.is_open());
    gameFile.seekg(0, ios::end); // pointer to the end
    int gameFileSize = gameFile.tellg(); // length of BIOS
    gameFile.seekg(0, ios::beg); // Pointer to the beginning

    gameFile.read((char *)memory, gameFileSize); // Read BIOS
}