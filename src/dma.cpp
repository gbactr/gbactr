#include "dma.h"
#include "mem.h"
using namespace std;

DMA::DMA(Mem *_mem) {
    // Constructor
    mem = _mem;
    ioregs = IORegs::getInstance();

    transfering = false;
    for (int i = 0; i<4; ++i) repeating[i] = false;
}

// Singleton
DMA *DMA::dma = nullptr;

DMA *DMA::getInstance(Mem *_mem) {
    if (dma == nullptr) {
        dma = new DMA(_mem);
    }
    return dma;
}

void DMA::processTransfers() {
    DMAAD_st dmasad;
    DMAAD_st dmadad;
    DMACNT_L_st dmacnt_l;
    DMACNT_H_st dmacnt_h;

    MemReq *r_req = new MemReq();
    MemReq *w_req = new MemReq();

    for (int i = 0; i<4; ++i) {
        // Initialize registers
        switch (i) {
            case 0: dmasad = ioregs->DMA0SAD; dmadad = ioregs->DMA0DAD;
                    dmacnt_l = ioregs->DMA0CNT_L; dmacnt_h = ioregs->DMA0CNT_H; break;
            case 1: dmasad = ioregs->DMA1SAD; dmadad = ioregs->DMA1DAD;
                    dmacnt_l = ioregs->DMA1CNT_L; dmacnt_h = ioregs->DMA1CNT_H; break;
            case 2: dmasad = ioregs->DMA2SAD; dmadad = ioregs->DMA2DAD;
                    dmacnt_l = ioregs->DMA2CNT_L; dmacnt_h = ioregs->DMA2CNT_H; break;
            case 3: dmasad = ioregs->DMA3SAD; dmadad = ioregs->DMA3DAD;
                    dmacnt_l = ioregs->DMA3CNT_L; dmacnt_h = ioregs->DMA3CNT_H; break;
        }
        if (dmacnt_l.count == 0) { // 0 is treated as max for that channel
            if (i == 3) dmacnt_l.count = 0x10000;
            else dmacnt_l.count = 0x4000;
        }

        // Do not reload if repeating and option is not enabled
        if (repeating[i] && (dmacnt_h.dest_addr_cont != 0x3)) {
            switch (i) {
                case 0: dmadad = DMA0DAD; break;
                case 1: dmadad = DMA1DAD; break;
                case 2: dmadad = DMA2DAD; break;
                case 3: dmadad = DMA3DAD; break;
            }
        }

        // Enable DMA
        if (dmacnt_h.DMA_en) {
            // Transfer start conditions
            if (dmacnt_h.DMA_repeat) {
                switch (dmacnt_h.start_timing) {
                    case 0: // Immediately
                        transfering = true; break;
                    case 1: // VBlank
                        transfering = ioregs->DISPSTAT.vblank; break;
                    case 2: // HBlank
                        transfering = ioregs->DISPSTAT.vblank; break;
                    case 3: // Special
                        transfering = false; break; // TODO: Special DMA conditions
                }
                repeating[i] = true;
            } else {
                transfering = true;
                repeating[i] = false;
            }

            // Perform DMA transfers
            if (transfering) {
                // Amounts and increments
                int size = dmacnt_h.DMA_type ? 4 : 2;
                int dad_op = 0;
                int sad_op = 0;
                if ((dmacnt_h.dest_addr_cont == 0x0) || (dmacnt_h.dest_addr_cont == 0x3)) dad_op = size;
                else if (dmacnt_h.dest_addr_cont == 0x1) dad_op = -size;
                if (dmacnt_h.source_addr_cont == 0x0) dad_op = size;
                else if (dmacnt_h.source_addr_cont == 0x1) dad_op = -size;

                addr_t dad = dmadad.address;
                addr_t sad = dmasad.address;
                r_req->type = dmacnt_h.DMA_type ? READ_WORD : READ_HALF;
                w_req->type = dmacnt_h.DMA_type ? WRITE_WORD : WRITE_HALF;

                // Transfer the bytes
                for (int i = 0; i<dmacnt_l.count; ++i) {
                    r_req->address = sad;
                    w_req->address = dad;
                    mem->request(r_req);
                    w_req->data = r_req->data;
                    sad += sad_op;
                    dad += dad_op;
                }
                
                // Update internal register in case of repeat and no reload
                switch (i) {
                    // TODO: Why is the count updated? It shouldn't!
                    case 0: DMA0DAD = dmadad; ioregs->DMA0CNT_L.count = 0; break;
                    case 1: DMA1DAD = dmadad; ioregs->DMA1CNT_L.count = 0; break;
                    case 2: DMA2DAD = dmadad; ioregs->DMA2CNT_L.count = 0; break;
                    case 3: DMA3DAD = dmadad; ioregs->DMA3CNT_L.count = 0; break;
                }
            }

            // If no repeat option, clear enable bit
            if (!dmacnt_h.DMA_repeat) {
                switch (i) {
                    case 0: ioregs->DMA0CNT_H.DMA_en = false; break;
                    case 1: ioregs->DMA1CNT_H.DMA_en = false; break;
                    case 2: ioregs->DMA2CNT_H.DMA_en = false; break;
                    case 3: ioregs->DMA3CNT_H.DMA_en = false; break;
                }
            }
        } else {
            repeating[i] = false;
        }
        transfering = false;
    }
}