#include "regfile.h"
using namespace std;

Regfile::Regfile() {
    // Constructor
    usr_gpr = (word_t **) malloc(NUM_GPR * sizeof(word_t *));
    initializeGprArray(usr_gpr);
    fiq_gpr = (word_t **) malloc(NUM_GPR * sizeof(word_t *));
    initializeGprArray(fiq_gpr);
    svc_gpr = (word_t **) malloc(NUM_GPR * sizeof(word_t *));
    initializeGprArray(svc_gpr);
    abt_gpr = (word_t **) malloc(NUM_GPR * sizeof(word_t *));
    initializeGprArray(abt_gpr);
    irq_gpr = (word_t **) malloc(NUM_GPR * sizeof(word_t *));
    initializeGprArray(irq_gpr);
    und_gpr = (word_t **) malloc(NUM_GPR * sizeof(word_t *));
    initializeGprArray(und_gpr);
    initializeRegisters();

    // FIQ GPR
    fiq_gpr[8] = &R8_fiq;
    fiq_gpr[9] = &R9_fiq;
    fiq_gpr[10] = &R10_fiq;
    fiq_gpr[11] = &R11_fiq;
    fiq_gpr[12] = &R12_fiq;
    fiq_gpr[13] = &R13_fiq;
    fiq_gpr[14] = &R14_fiq;

    // Supervisor GPR
    svc_gpr[13] = &R13_svc;
    svc_gpr[14] = &R14_svc;

    // Abort GPR
    abt_gpr[13] = &R13_abt;
    abt_gpr[14] = &R14_abt;

    // IRQ GPR
    irq_gpr[13] = &R13_irq;
    irq_gpr[14] = &R14_irq;

    // Undefined GPR
    und_gpr[13] = &R13_und;
    und_gpr[14] = &R14_und;

    cpu_mode = ARM;
}

// Singleton
Regfile *Regfile::regfile = nullptr;

Regfile *Regfile::getInstance() {
    if (regfile == nullptr) {
        regfile = new Regfile();
    }
    return regfile;
}

word_t Regfile::readGPR(int reg) {
    mode_e mode = getCurrentMode();
    return readGPR(reg, mode);
}

word_t Regfile::readGPR(int reg, mode_e mode) {
    DBG_ASSERT_E(reg < 0 || reg >= NUM_GPR);
    word_t **gprArray = getGprArray(mode);

    return *(gprArray[reg]);
}

void Regfile::writeGPR(int reg, word_t value) {
    mode_e mode = getCurrentMode();
    return writeGPR(reg, value, mode);
}

void Regfile::writeGPR(int reg, word_t value, mode_e mode) {
#ifdef FTR_PRINT_INSTRUCTION
    cout << std::dec << "GPR W R" << reg << " " << std::hex << value << endl;
#endif
    DBG_ASSERT_E(reg < 0 || reg >= NUM_GPR);
    word_t **gprArray = getGprArray(mode);

    *(gprArray[reg]) = value;
}

void Regfile::initializeGprArray(word_t **gprArray) {
    gprArray[0] = &R0;
    gprArray[1] = &R1;
    gprArray[2] = &R2;
    gprArray[3] = &R3;
    gprArray[4] = &R4;
    gprArray[5] = &R5;
    gprArray[6] = &R6;
    gprArray[7] = &R7;
    gprArray[8] = &R8;
    gprArray[9] = &R9;
    gprArray[10] = &R10;
    gprArray[11] = &R11;
    gprArray[12] = &R12;
    gprArray[13] = &R13;
    gprArray[14] = &R14;
    gprArray[15] = &R15;
}

void Regfile::initializeRegisters() {
    R0 = 0;
    R1 = 0;
    R2 = 0;
    R3 = 0;
    R4 = 0;
    R5 = 0;
    R6 = 0;
    R7 = 0;
    R8 = 0;
    R9 = 0;
    R10 = 0;
    R11 = 0;
    R12 = 0;
    R13 = 0;
    R14 = 0;
    R15 = PC_INIT_VALUE;
    R8_fiq = 0;
    R9_fiq = 0;
    R10_fiq = 0;
    R11_fiq = 0;
    R12_fiq = 0;
    R13_fiq = 0;
    R14_fiq = 0;
    R13_svc = 0;
    R14_svc = 0;
    R13_abt = 0;
    R14_abt = 0;
    R13_irq = 0;
    R14_irq = 0;
    R13_und = 0;
    R14_und = 0;
}

word_t **Regfile::getGprArray(mode_e mode) {
    switch(mode) {
        case USER: return usr_gpr; break;
        case FIQ: return fiq_gpr; break;
        case SUPERVISOR: return svc_gpr; break;
        case ABORT: return abt_gpr; break;
        case IRQ: return irq_gpr; break;
        case UNDEFINED: return und_gpr; break;
        case SYSTEM: return usr_gpr; break;
    }
    DBG_ASSERT();
    return NULL; // Unreachable
}

mode_e Regfile::getCurrentMode() {
    return CPSR.M;
}

void Regfile::setCurrentMode(mode_e mode) {
    CPSR.M = mode;
}

word_t Regfile::PSRToWord(PSR_t psr) {
    word_t result = 0;
    result |= psr.N << 31;
    result |= psr.Z << 30;
    result |= psr.C << 29;
    result |= psr.V << 28;
    result |= psr.I << 7;
    result |= psr.F << 6;
    result |= psr.T << 5;
    result |= (psr.M & 0x1f);

    return result;
}

PSR_t Regfile:: wordToPSR(word_t psr) {
    PSR_t result;
    result.N = (psr >> 31) & 1;
    result.Z = (psr >> 30) & 1;
    result.C = (psr >> 29) & 1;
    result.V = (psr >> 28) & 1;
    result.I = (psr >> 7) & 1;
    result.F = (psr >> 6) & 1;
    result.T = (psr >> 5) & 1;
    result.M = (mode_e) (psr & 0x1f);
    return result;
}

PSR_t Regfile::getCPSR() {
    return CPSR;
}

bool Regfile::readCPSR_N() {
    return CPSR.N;
}

bool Regfile::readCPSR_Z() {
    return CPSR.Z;
}

bool Regfile::readCPSR_C() {
    return CPSR.C;
}

bool Regfile::readCPSR_V() {
    return CPSR.V;
}

bool Regfile::readCPSR_I() {
    return CPSR.I;
}

bool Regfile::readCPSR_F() {
    return CPSR.F;
}

bool Regfile::readCPSR_T() {
    return CPSR.T;
}

void Regfile::writeCPSR(PSR_t psr) {
    word_t wpsr = PSRToWord(psr);
    writeCPSR(wpsr);
}

void Regfile::writeCPSR(word_t value) {
    PSR_t psr_value = wordToPSR(value);
    CPSR.N = psr_value.N;
    CPSR.Z = psr_value.Z;
    CPSR.C = psr_value.C;
    CPSR.V = psr_value.V;
    if (getCurrentMode() != USER) {
        CPSR.I = psr_value.I;
        CPSR.F = psr_value.F;
        CPSR.T = psr_value.T;
    }
    CPSR.M = psr_value.M;
}

void Regfile::writeCPSR_N(bool N) {
    CPSR.N = N;
}

void Regfile::writeCPSR_Z(bool Z) {
    CPSR.Z = Z;
}

void Regfile::writeCPSR_C(bool C) {
    CPSR.C = C;
}

void Regfile::writeCPSR_V(bool V) {
    CPSR.V = V;
}

void Regfile::writeCPSR_I(bool I) {
    if (getCurrentMode() != USER) {
        CPSR.I = I;
    }
}

void Regfile::writeCPSR_F(bool F) {
    if (getCurrentMode() != USER) {
        CPSR.F = F;
    }
}

void Regfile::writeCPSR_T(bool T) {
    if (getCurrentMode() != USER) {
        CPSR.T = T;
    }
}

PSR_t Regfile::getSPSR() {
    return getSPSR(getCurrentMode());
}

PSR_t Regfile::getSPSR(mode_e mode) {
    PSR_t SPSR;
    switch (mode) {
        case FIQ:        SPSR = SPSR_fiq; break;
        case IRQ:        SPSR = SPSR_irq; break;
        case SUPERVISOR: SPSR = SPSR_svc; break;
        case ABORT:      SPSR = SPSR_abt; break;
        case UNDEFINED:  SPSR = SPSR_und; break;
        default: DBG_ASSERT();
    }

    return SPSR;
}

void Regfile::writeSPSR(word_t value) {
    writeSPSR(getCurrentMode(), value);
}

void Regfile::writeSPSR(mode_e mode, word_t value) {
    PSR_t psr_value = wordToPSR(value);
    PSR_t *SPSR;
    switch (mode) {
        case FIQ:
            SPSR = &SPSR_fiq;
            break;
        case IRQ:
            SPSR = &SPSR_irq;
            break;
        case SUPERVISOR:
            SPSR = &SPSR_svc;
            break;
        case ABORT:
            SPSR = &SPSR_abt;
            break;
        case UNDEFINED:
            SPSR = &SPSR_und;
            break;
        default:
            // No SPSR for other modes!
            DBG_ASSERT();
    }

    SPSR->N = psr_value.N;
    SPSR->Z = psr_value.Z;
    SPSR->C = psr_value.C;
    SPSR->V = psr_value.V;
    SPSR->I = psr_value.I;
    SPSR->F = psr_value.F;
    SPSR->T = psr_value.T;
}

cpu_mode_e Regfile::getCPUMode() {
    return cpu_mode;
}

void Regfile::setCPUMode(cpu_mode_e _cpu_mode) {
    cpu_mode = _cpu_mode;
}