#include "io_regs.h"
using namespace std;

IORegs::IORegs() {
    // Constructor
    size = IO_REGS_SIZE;
    baseAddress = IO_REGS_BASE_ADDRESS;
}

//Singleton
IORegs*IORegs::ioRegs = nullptr;

IORegs*IORegs::getInstance() {
    if (ioRegs == nullptr) {
        ioRegs = new IORegs();
    }
    return ioRegs;
}

void IORegs::request(MemReq *request) {
    addr_t effectiveAddress = request->address - baseAddress;
    HALT_ASSERT_E(effectiveAddress >= (baseAddress + size));

    word_t data;
    bool word = (request->size() == 4);
    bool two_accesses = (word && (
                                 (effectiveAddress % 4 != 0) ||
                                 (IS_BETWEEN(0x00, 0x28, effectiveAddress)) ||
                                 (IS_BETWEEN(0x30, 0x38, effectiveAddress)) ||
                                 (IS_BETWEEN(0x40, 0x56, effectiveAddress)) ||
                                 (IS_BETWEEN(0xb8, 0xbc, effectiveAddress)) ||
                                 (IS_BETWEEN(0xc4, 0xc8, effectiveAddress)) ||
                                 (IS_BETWEEN(0xd0, 0xd4, effectiveAddress)) ||
                                 (IS_BETWEEN(0xdc, 0xde, effectiveAddress))));

    for (int i = 0; i<=two_accesses; ++i) {
        if (request->isRead()) {
            switch (effectiveAddress) {
                case 0x00: data = packDISPCNT(DISPCNT); break;
                case 0x04: data = packDISPSTAT(DISPSTAT); break;
                case 0x08: data = packBGCNT(BG0CNT); break;
                case 0x0a: data = packBGCNT(BG1CNT); break;
                case 0x0c: data = packBGCNT(BG2CNT); break;
                case 0x0e: data = packBGCNT(BG3CNT); break;
                case 0x10: data = packBGOFS(BG0HOFS); break;
                case 0x12: data = packBGOFS(BG0VOFS); break;
                case 0x14: data = packBGOFS(BG1HOFS); break;
                case 0x16: data = packBGOFS(BG1VOFS); break;
                case 0x18: data = packBGOFS(BG2HOFS); break;
                case 0x1a: data = packBGOFS(BG2VOFS); break;
                case 0x1c: data = packBGOFS(BG3HOFS); break;
                case 0x1e: data = packBGOFS(BG3VOFS); break;
                case 0x20: data = packBGP(BGP2A); break;
                case 0x22: data = packBGP(BGP2B); break;
                case 0x24: data = packBGP(BGP2C); break;
                case 0x26: data = packBGP(BGP2D); break;
                case 0x28: data = packBGXY(BG2X); break;
                case 0x2c: data = packBGXY(BG2Y); break;
                case 0x30: data = packBGP(BGP3A); break;
                case 0x32: data = packBGP(BGP3B); break;
                case 0x34: data = packBGP(BGP3C); break;
                case 0x36: data = packBGP(BGP3D); break;
                case 0x38: data = packBGXY(BG3X); break;
                case 0x3c: data = packBGXY(BG3Y); break;
                case 0x40: data = packWIN(WIN0H); break;
                case 0x42: data = packWIN(WIN1H); break;
                case 0x44: data = packWIN(WIN0V); break;
                case 0x46: data = packWIN(WIN1V); break;
                case 0x48: data = packWININ(WININ); break;
                case 0x4a: data = packWINOUT(WINOUT); break;
                case 0x4c: data = packMOSAIC(MOSAIC); break;
                case 0x50: data = packBLDCNT(BLDCNT); break;
                case 0x52: data = packBLDALPHA(BLDALPHA); break;
                case 0x54: data = packBLDY(BLDY); break;
                case 0xb0: data = packDMAAD(DMA0SAD); break;
                case 0xb4: data = packDMAAD(DMA0DAD); break;
                case 0xb8: data = packDMACNT_L(DMA0CNT_L); break;
                case 0xba: data = packDMACNT_H(DMA0CNT_H); break;
                case 0xbc: data = packDMAAD(DMA1SAD); break;
                case 0xc0: data = packDMAAD(DMA1DAD); break;
                case 0xc4: data = packDMACNT_L(DMA1CNT_L); break;
                case 0xc6: data = packDMACNT_H(DMA1CNT_H); break;
                case 0xc8: data = packDMAAD(DMA2SAD); break;
                case 0xcc: data = packDMAAD(DMA2DAD); break;
                case 0xd0: data = packDMACNT_L(DMA2CNT_L); break;
                case 0xd2: data = packDMACNT_H(DMA2CNT_H); break;
                case 0xd4: data = packDMAAD(DMA3SAD); break;
                case 0xd8: data = packDMAAD(DMA3DAD); break;
                case 0xdc: data = packDMACNT_L(DMA3CNT_L); break;
                case 0xde: data = packDMACNT_H(DMA3CNT_H); break;
                default: data = 0; // Unused slots
            }
            if (i == 1) {
                request->data = ((request->data & 0xffff) | ((data & 0xffff) << 16));
            } else {
                request->data = data;
                effectiveAddress += 2;
            }
        }
        else if (request->isWrite()) {
            switch (effectiveAddress) {
                case 0x00: unpackDISPCNT(&DISPCNT, request->data); break;
                case 0x04: unpackDISPSTAT(&DISPSTAT, request->data); break;
                case 0x08: unpackBGCNT(&BG0CNT, request->data); break;
                case 0x0a: unpackBGCNT(&BG1CNT, request->data); break;
                case 0x0c: unpackBGCNT(&BG2CNT, request->data); break;
                case 0x0e: unpackBGCNT(&BG3CNT, request->data); break;
                case 0x10: unpackBGOFS(&BG0HOFS, request->data); break;
                case 0x12: unpackBGOFS(&BG0VOFS, request->data); break;
                case 0x14: unpackBGOFS(&BG1HOFS, request->data); break;
                case 0x16: unpackBGOFS(&BG1VOFS, request->data); break;
                case 0x18: unpackBGOFS(&BG2HOFS, request->data); break;
                case 0x1a: unpackBGOFS(&BG2VOFS, request->data); break;
                case 0x1c: unpackBGOFS(&BG3HOFS, request->data); break;
                case 0x1e: unpackBGOFS(&BG3VOFS, request->data); break;
                case 0x20: unpackBGP(&BGP2A, request->data); break;
                case 0x22: unpackBGP(&BGP2B, request->data); break;
                case 0x24: unpackBGP(&BGP2C, request->data); break;
                case 0x26: unpackBGP(&BGP2D, request->data); break;
                case 0x28: unpackBGXY(&BG2X, request->data); break;
                case 0x2c: unpackBGXY(&BG2Y, request->data); break;
                case 0x30: unpackBGP(&BGP3A, request->data); break;
                case 0x32: unpackBGP(&BGP3B, request->data); break;
                case 0x34: unpackBGP(&BGP3C, request->data); break;
                case 0x36: unpackBGP(&BGP3D, request->data); break;
                case 0x38: unpackBGXY(&BG3X, request->data); break;
                case 0x3c: unpackBGXY(&BG3Y, request->data); break;
                case 0x40: unpackWIN(&WIN0H, request->data); break;
                case 0x42: unpackWIN(&WIN1H, request->data); break;
                case 0x44: unpackWIN(&WIN0V, request->data); break;
                case 0x46: unpackWIN(&WIN1V, request->data); break;
                case 0x48: unpackWININ(&WININ, request->data); break;
                case 0x4a: unpackWINOUT(&WINOUT, request->data); break;
                case 0x4c: unpackMOSAIC(&MOSAIC, request->data); break;
                case 0x50: unpackBLDCNT(&BLDCNT, request->data); break;
                case 0x52: unpackBLDALPHA(&BLDALPHA, request->data); break;
                case 0x54: unpackBLDY(&BLDY, request->data); break;
                case 0xb0: unpackDMAAD(&DMA0SAD, request->data); break;
                case 0xb4: unpackDMAAD(&DMA0DAD, request->data); break;
                case 0xb8: unpackDMACNT_L(&DMA0CNT_L, request->data); break;
                case 0xba: unpackDMACNT_H(&DMA0CNT_H, request->data); break;
                case 0xbc: unpackDMAAD(&DMA1SAD, request->data); break;
                case 0xc0: unpackDMAAD(&DMA1DAD, request->data); break;
                case 0xc4: unpackDMACNT_L(&DMA1CNT_L, request->data); break;
                case 0xc6: unpackDMACNT_H(&DMA1CNT_H, request->data); break;
                case 0xc8: unpackDMAAD(&DMA2SAD, request->data); break;
                case 0xcc: unpackDMAAD(&DMA2DAD, request->data); break;
                case 0xd0: unpackDMACNT_L(&DMA2CNT_L, request->data); break;
                case 0xd2: unpackDMACNT_H(&DMA2CNT_H, request->data); break;
                case 0xd4: unpackDMAAD(&DMA3SAD, request->data); break;
                case 0xd8: unpackDMAAD(&DMA3DAD, request->data); break;
                case 0xdc: unpackDMACNT_L(&DMA3CNT_L, request->data); break;
                case 0xde: unpackDMACNT_H(&DMA3CNT_H, request->data); break;
            }
            if (i == 0) {
                request->data = (request->data >> 16) & 0xffff;
            }
            effectiveAddress += 2;
        }
    }
}

word_t IORegs::packDISPCNT(DISPCNT_st _DISPCNT) {
    word_t result = 0;
    SET_BITS(result, 0, _DISPCNT.BG_mode);
    SET_BITS(result, 4, _DISPCNT.display_frame);
    SET_BITS(result, 5, _DISPCNT.hblank_OAM_access);
    SET_BITS(result, 6, _DISPCNT.two_d_obj_char);
    SET_BITS(result, 8, _DISPCNT.BG0_display);
    SET_BITS(result, 9, _DISPCNT.BG1_display);
    SET_BITS(result, 10, _DISPCNT.BG2_display);
    SET_BITS(result, 11, _DISPCNT.BG3_display);
    SET_BITS(result, 12, _DISPCNT.OBJ_display);
    SET_BITS(result, 13, _DISPCNT.WIN0_display);
    SET_BITS(result, 14, _DISPCNT.WIN1_display);
    return result;
}

word_t IORegs::packDISPSTAT(DISPSTAT_st _DISPSTAT){
    word_t result = 0;
    SET_BITS(result, 0, _DISPSTAT.vblank);
    SET_BITS(result, 1, _DISPSTAT.hblank);
    SET_BITS(result, 2, _DISPSTAT.vcounter);
    SET_BITS(result, 3, _DISPSTAT.vblank_irq_en);
    SET_BITS(result, 4, _DISPSTAT.hblank_irq_en);
    SET_BITS(result, 5, _DISPSTAT.vcounter_irq_en);
    SET_BITS(result, 8, _DISPSTAT.vcount_line);
    return result;
}

word_t IORegs::packBGCNT(BGCNT_st _BGCNT){
    word_t result = 0;
    SET_BITS(result, 0, _BGCNT.priority);
    SET_BITS(result, 2, _BGCNT.char_base_block);
    SET_BITS(result, 6, _BGCNT.mosaic);
    SET_BITS(result, 7, _BGCNT.palettes);
    SET_BITS(result, 8, _BGCNT.screen_base_block);
    SET_BITS(result, 13, _BGCNT.display_overflow);
    SET_BITS(result, 14, _BGCNT.screen_size);
    return result;
}

word_t IORegs::packBGOFS(BGOFS_st _BGOFS){
    word_t result = 0;
    SET_BITS(result, 0, _BGOFS.offset);
    return result;
}

word_t IORegs::packBGP(BGP_st _BGP){
    word_t result = 0;
    SET_BITS(result, 0, _BGP.fractional);
    SET_BITS(result, 8, _BGP.integer);
    SET_BITS(result, 15, _BGP.sign);
    return result;
}

word_t IORegs::packBGXY(BGXY_st _BGXY){
    word_t result = 0;
    SET_BITS(result, 0, _BGXY.fractional);
    SET_BITS(result, 26, _BGXY.integer);
    SET_BITS(result, 27, _BGXY.sign);
    return result;
}

word_t IORegs::packWIN(WIN_st _WIN){
    word_t result = 0;
    SET_BITS(result, 0, _WIN.coord1);
    SET_BITS(result, 8, _WIN.coord2);
    return result;
}

word_t IORegs::packWININ(WININ_st _WININ){
    word_t result = 0;
    SET_BITS(result, 0, _WININ.win0_bg0_en);
    SET_BITS(result, 1, _WININ.win0_bg1_en);
    SET_BITS(result, 2, _WININ.win0_bg2_en);
    SET_BITS(result, 3, _WININ.win0_bg3_en);
    SET_BITS(result, 4, _WININ.win0_obj_en);
    SET_BITS(result, 5, _WININ.win0_color_effect);
    SET_BITS(result, 8, _WININ.win1_bg0_en);
    SET_BITS(result, 9, _WININ.win1_bg1_en);
    SET_BITS(result, 10, _WININ.win1_bg2_en);
    SET_BITS(result, 11, _WININ.win1_bg3_en);
    SET_BITS(result, 12, _WININ.win1_obj_en);
    SET_BITS(result, 13, _WININ.win1_color_effect);
    return result;
}

word_t IORegs::packWINOUT(WINOUT_st _WINOUT){
    word_t result = 0;
    SET_BITS(result, 0, _WINOUT.winout_bg0_en);
    SET_BITS(result, 1, _WINOUT.winout_bg1_en);
    SET_BITS(result, 2, _WINOUT.winout_bg2_en);
    SET_BITS(result, 3, _WINOUT.winout_bg3_en);
    SET_BITS(result, 4, _WINOUT.winout_obj_en);
    SET_BITS(result, 5, _WINOUT.winout_color_effect);
    SET_BITS(result, 8, _WINOUT.winobj_bg0_en);
    SET_BITS(result, 9, _WINOUT.winobj_bg1_en);
    SET_BITS(result, 10, _WINOUT.winobj_bg2_en);
    SET_BITS(result, 11, _WINOUT.winobj_bg3_en);
    SET_BITS(result, 12, _WINOUT.winobj_obj_en);
    SET_BITS(result, 13, _WINOUT.winobj_color_effect);
    return result;
}

word_t IORegs::packMOSAIC(MOSAIC_st _MOSAIC){
    word_t result = 0;
    SET_BITS(result, 0, _MOSAIC.bg_hsize);
    SET_BITS(result, 4, _MOSAIC.bg_vsize);
    SET_BITS(result, 8, _MOSAIC.obj_hsize);
    SET_BITS(result, 12, _MOSAIC.obj_vsize);
    return result;
}

word_t IORegs::packBLDCNT(BLDCNT_st _BLDCNT){
    word_t result = 0;
    SET_BITS(result, 0, _BLDCNT.bg0_1st_target_pixel);
    SET_BITS(result, 1, _BLDCNT.bg1_1st_target_pixel);
    SET_BITS(result, 2, _BLDCNT.bg2_1st_target_pixel);
    SET_BITS(result, 3, _BLDCNT.bg3_1st_target_pixel);
    SET_BITS(result, 4, _BLDCNT.obj_1st_target_pixel);
    SET_BITS(result, 5, _BLDCNT.bd_1st_target_pixel);
    SET_BITS(result, 6, _BLDCNT.color_special_effect);
    SET_BITS(result, 8, _BLDCNT.bg0_2nd_target_pixel);
    SET_BITS(result, 9, _BLDCNT.bg1_2nd_target_pixel);
    SET_BITS(result, 10, _BLDCNT.bg2_2nd_target_pixel);
    SET_BITS(result, 11, _BLDCNT.bg3_2nd_target_pixel);
    SET_BITS(result, 12, _BLDCNT.obj_2nd_target_pixel);
    SET_BITS(result, 13, _BLDCNT.bd_2nd_target_pixel);
    return result;
}

word_t IORegs::packBLDALPHA(BLDALPHA_st _BLDALPHA){
    word_t result = 0;
    SET_BITS(result, 0, _BLDALPHA.EVA_coef);
    SET_BITS(result, 8, _BLDALPHA.EVB_coef);
    return result;
}

word_t IORegs::packBLDY(BLDY_st _BLDY){
    word_t result = 0;
    SET_BITS(result, 0, _BLDY.EVY_coef);
    return result;
}

word_t IORegs::packDMAAD(DMAAD_st _DMAAD){
    word_t result = 0;
    SET_BITS(result, 0, _DMAAD.address);
    return result;
}

word_t IORegs::packDMACNT_L(DMACNT_L_st _DMACNT_L){
    word_t result = 0;
    SET_BITS(result, 0, _DMACNT_L.count);
    return result;
}

word_t IORegs::packDMACNT_H(DMACNT_H_st _DMACNT_H){
    word_t result = 0;
    SET_BITS(result, 5, _DMACNT_H.dest_addr_cont);
    SET_BITS(result, 7, _DMACNT_H.source_addr_cont);
    SET_BITS(result, 9, _DMACNT_H.DMA_repeat);
    SET_BITS(result, 10, _DMACNT_H.DMA_type);
    SET_BITS(result, 11, _DMACNT_H.DRQ);
    SET_BITS(result, 12, _DMACNT_H.start_timing);
    SET_BITS(result, 14, _DMACNT_H.IRQ);
    SET_BITS(result, 15, _DMACNT_H.DMA_en);
    return result;
}

void IORegs::unpackDISPCNT(DISPCNT_st *_DISPCNT, word_t data) {
    _DISPCNT->BG_mode = GET_BITS(data, 0, 0x7);
    _DISPCNT->display_frame = GET_BITS(data, 4, 0x1);
    _DISPCNT->hblank_OAM_access = GET_BITS(data, 5, 0x1);
    _DISPCNT->two_d_obj_char = GET_BITS(data, 6, 0x1);
    _DISPCNT->BG0_display = GET_BITS(data, 8, 0x1);
    _DISPCNT->BG1_display = GET_BITS(data, 9, 0x1);
    _DISPCNT->BG2_display = GET_BITS(data, 10, 0x1);
    _DISPCNT->BG3_display = GET_BITS(data, 11, 0x1);
    _DISPCNT->OBJ_display = GET_BITS(data, 12, 0x1);
    _DISPCNT->WIN0_display = GET_BITS(data, 13, 0x1);
    _DISPCNT->WIN1_display = GET_BITS(data, 14, 0x1);
}

void IORegs::unpackDISPSTAT(DISPSTAT_st *_DISPSTAT, word_t data) {
    _DISPSTAT->vblank = GET_BITS(data, 0, 0x1);
    _DISPSTAT->hblank = GET_BITS(data, 1, 0x1);
    _DISPSTAT->vcounter = GET_BITS(data, 2, 0x1);
    _DISPSTAT->vblank_irq_en = GET_BITS(data, 3, 0x1);
    _DISPSTAT->hblank_irq_en = GET_BITS(data, 4, 0x1);
    _DISPSTAT->vcounter_irq_en = GET_BITS(data, 5, 0x1);
    _DISPSTAT->vcount_line = GET_BITS(data, 8, 0xff);
}

void IORegs::unpackBGCNT(BGCNT_st *_BGCNT, word_t data) {
    _BGCNT->priority = GET_BITS(data, 0, 0x3);
    _BGCNT->char_base_block = GET_BITS(data, 2, 0x3);
    _BGCNT->mosaic = GET_BITS(data, 6, 0x1);
    _BGCNT->palettes = GET_BITS(data, 7, 0x1);
    _BGCNT->screen_base_block = GET_BITS(data, 8, 0xf);
    _BGCNT->display_overflow = GET_BITS(data, 13, 0x1);
    _BGCNT->screen_size = GET_BITS(data, 14, 0x3);
}

void IORegs::unpackBGOFS(BGOFS_st *_BGOFS, word_t data) {
    _BGOFS->offset = GET_BITS(data, 0, 0x1ff);
}

void IORegs::unpackBGP(BGP_st *_BGP, word_t data) {
    _BGP->fractional = GET_BITS(data, 0, 0xff);
    _BGP->integer = GET_BITS(data, 8, 0x7f);
    _BGP->sign = GET_BITS(data, 15, 0x1);
}

void IORegs::unpackBGXY(BGXY_st *_BGXY, word_t data) {
    _BGXY->fractional = GET_BITS(data, 0, 0xff);
    _BGXY->integer = GET_BITS(data, 26, 0x7ffff);
    _BGXY->sign = GET_BITS(data, 27, 0x1);
}

void IORegs::unpackWIN(WIN_st *_WIN, word_t data) {
    _WIN->coord1 = GET_BITS(data, 0, 0xff);
    _WIN->coord2 = GET_BITS(data, 8, 0xff);
}

void IORegs::unpackWININ(WININ_st *_WININ, word_t data) {
    _WININ->win0_bg0_en = GET_BITS(data, 0, 0x1);
    _WININ->win0_bg1_en = GET_BITS(data, 1, 0x1);
    _WININ->win0_bg2_en = GET_BITS(data, 2, 0x1);
    _WININ->win0_bg3_en = GET_BITS(data, 3, 0x1);
    _WININ->win0_obj_en = GET_BITS(data, 4, 0x1);
    _WININ->win0_color_effect = GET_BITS(data, 5, 0x1);
    _WININ->win1_bg0_en = GET_BITS(data, 8, 0x1);
    _WININ->win1_bg1_en = GET_BITS(data, 9, 0x1);
    _WININ->win1_bg2_en = GET_BITS(data, 10, 0x1);
    _WININ->win1_bg3_en = GET_BITS(data, 11, 0x1);
    _WININ->win1_obj_en = GET_BITS(data, 12, 0x1);
    _WININ->win1_color_effect = GET_BITS(data, 13, 0x1);
}

void IORegs::unpackWINOUT(WINOUT_st *_WINOUT, word_t data) {
    _WINOUT->winout_bg0_en = GET_BITS(data, 0, 0x1);
    _WINOUT->winout_bg1_en = GET_BITS(data, 1, 0x1);
    _WINOUT->winout_bg2_en = GET_BITS(data, 2, 0x1);
    _WINOUT->winout_bg3_en = GET_BITS(data, 3, 0x1);
    _WINOUT->winout_obj_en = GET_BITS(data, 4, 0x1);
    _WINOUT->winout_color_effect = GET_BITS(data, 5, 0x1);
    _WINOUT->winobj_bg0_en = GET_BITS(data, 8, 0x1);
    _WINOUT->winobj_bg1_en = GET_BITS(data, 9, 0x1);
    _WINOUT->winobj_bg2_en = GET_BITS(data, 10, 0x1);
    _WINOUT->winobj_bg3_en = GET_BITS(data, 11, 0x1);
    _WINOUT->winobj_obj_en = GET_BITS(data, 12, 0x1);
    _WINOUT->winobj_color_effect = GET_BITS(data, 13, 0x1);
}

void IORegs::unpackMOSAIC(MOSAIC_st *_MOSAIC, word_t data) {
    _MOSAIC->bg_hsize = GET_BITS(data, 0, 0xf);
    _MOSAIC->bg_vsize = GET_BITS(data, 4, 0xf);
    _MOSAIC->obj_hsize = GET_BITS(data, 8, 0xf);
    _MOSAIC->obj_vsize = GET_BITS(data, 12, 0xf);
}

void IORegs::unpackBLDCNT(BLDCNT_st *_BLDCNT, word_t data) {
    _BLDCNT->bg0_1st_target_pixel = GET_BITS(data, 0, 0x1);
    _BLDCNT->bg1_1st_target_pixel = GET_BITS(data, 1, 0x1);
    _BLDCNT->bg2_1st_target_pixel = GET_BITS(data, 2, 0x1);
    _BLDCNT->bg3_1st_target_pixel = GET_BITS(data, 3, 0x1);
    _BLDCNT->obj_1st_target_pixel = GET_BITS(data, 4, 0x1);
    _BLDCNT->bd_1st_target_pixel = GET_BITS(data, 5, 0x1);
    _BLDCNT->color_special_effect = GET_BITS(data, 6, 0x1);
    _BLDCNT->bg0_2nd_target_pixel = GET_BITS(data, 8, 0x1);
    _BLDCNT->bg1_2nd_target_pixel = GET_BITS(data, 9, 0x1);
    _BLDCNT->bg2_2nd_target_pixel = GET_BITS(data, 10, 0x1);
    _BLDCNT->bg3_2nd_target_pixel = GET_BITS(data, 11, 0x1);
    _BLDCNT->obj_2nd_target_pixel = GET_BITS(data, 12, 0x1);
    _BLDCNT->bd_2nd_target_pixel = GET_BITS(data, 13, 0x1);
}

void IORegs::unpackBLDALPHA(BLDALPHA_st *_BLDALPHA, word_t data) {
    _BLDALPHA->EVA_coef = GET_BITS(data, 0, 0x1f);
    _BLDALPHA->EVB_coef = GET_BITS(data, 8, 0x1f);
}

void IORegs::unpackBLDY(BLDY_st *_BLDY, word_t data) {
    _BLDY->EVY_coef = GET_BITS(data, 0, 0x1f);
}

void IORegs::unpackDMAAD(DMAAD_st *_DMAAD, word_t data){
    _DMAAD->address = GET_BITS(data, 0, 0xfffffff);
}

void IORegs::unpackDMACNT_L(DMACNT_L_st *_DMACNT_L, word_t data) {
    _DMACNT_L->count = GET_BITS(data, 0, 0xffff);
}

void IORegs::unpackDMACNT_H(DMACNT_H_st *_DMACNT_H, word_t data) {
    _DMACNT_H->dest_addr_cont = GET_BITS(data, 5, 0x3);
    _DMACNT_H->source_addr_cont = GET_BITS(data, 7, 0x3);
    _DMACNT_H->DMA_repeat = GET_BITS(data, 9, 0x1);
    _DMACNT_H->DMA_type = GET_BITS(data, 10, 0x1);
    _DMACNT_H->DRQ = GET_BITS(data, 11, 0x1);
    _DMACNT_H->start_timing = GET_BITS(data, 12, 0x3);
    _DMACNT_H->IRQ = GET_BITS(data, 14, 0x1);
    _DMACNT_H->DMA_en = GET_BITS(data, 15, 0x1);
}