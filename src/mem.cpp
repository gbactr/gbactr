#include "mem.h"
#include "dma.h"
using namespace std;

Mem::Mem() {
    // Constructor
    bios = BIOS::getInstance();
    wram1 = new WRAM(WRAM_1_SIZE, WRAM_1_BASE_ADDRESS);
    wram2 = new WRAM(WRAM_2_SIZE, WRAM_2_BASE_ADDRESS);
    ioRegs = IORegs::getInstance();
    //paletteRam = PaletteRAM::getInstance();
    //vram = VRAM::getInstance();
    //oam = OAM::getInstance();
    gamepak = Gamepak::getInstance();
    //sram = SRAM::getInstance();
    dma = DMA::getInstance(this);
}

//Singleton
Mem*Mem::mem = nullptr;

Mem*Mem::getInstance() {
    if (mem == nullptr) {
        mem = new Mem();
    }
    return mem;
}

void Mem::request(MemReq *request) {
    addr_t address = request->address;
    if (IS_BETWEEN(BIOS_BASE_ADDRESS, BIOS_TOP_ADDRESS, address))       bios->request(request);
    if (IS_BETWEEN(WRAM_1_BASE_ADDRESS, WRAM_1_TOP_ADDRESS, address))   wram1->request(request);
    if (IS_BETWEEN(WRAM_2_BASE_ADDRESS, WRAM_2_TOP_ADDRESS, address))   wram2->request(request);
    if (IS_BETWEEN(GAMEPAK_BASE_ADDRESS, GAMEPAK_TOP_ADDRESS, address)) gamepak->request(request);
    if (IS_BETWEEN(IO_REGS_BASE_ADDRESS, IO_REGS_TOP_ADDRESS, address)) ioRegs->request(request);

    if (!dma->transfering) {
        dma->processTransfers(); // TODO: Only do this when writing to the enable addresses
    }
#ifdef FTR_PRINT_INSTRUCTION
    cout << "Memory Request: ";
    if (request->isRead()) {
        cout << "r ";
    }
    else if (request->isWrite()) {
        cout << "w ";
    }
    else {
        DBG_ASSERT();
    }

    cout << std::hex << request->address << " " << request->size() << " " << request->data;
    cout << endl;
#endif //PRINT_INSTRUCTION
}