#include "wram.h"
using namespace std;

WRAM::WRAM(int _size, addr_t _baseAddress) {
    // Constructor
    size = _size;
    baseAddress = _baseAddress;
    allocateMem();
}