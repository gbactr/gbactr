#include "decoderTHUMB.h"
#include <iomanip>
using namespace std;

DecoderTHUMB::DecoderTHUMB() {
    // Constructor
    regfile = Regfile::getInstance();
}

// Singleton
DecoderTHUMB *DecoderTHUMB::decoderTHUMB = nullptr;

DecoderTHUMB *DecoderTHUMB::getInstance() {
    if (decoderTHUMB == nullptr) {
        decoderTHUMB = new DecoderTHUMB();
    }
    return decoderTHUMB;
}

/*
    Decoding flow:
    decodeInstruction(): fetched instruction is provided.
    decodeType(): The type of instruction (Figure 4-1) is decoded and saved into the decoded result.
    decode[TYPE](): The instruction type is decoded and all the characteristics are saved into the decoded result.
    The decoded result is returned as instr_THUMB_st
*/

instr_THUMB_st DecoderTHUMB::decodeInstruction(half_t fetch_instr) {
    instr_THUMB_st decoded_instr;

    // Decode the instruction type
    decoded_instr.type = decodeType(fetch_instr);
    // Decode type-specific information
    switch (decoded_instr.type) {
        case MOVE_SHIFTED_REGISTER:
            decodeMoveShiftedRegister(fetch_instr, &decoded_instr);
            break;
        case ADD_SUBTRACT:
            decodeAddSubtract(fetch_instr, &decoded_instr);
            break;
        case MOVE_COMPARE_ADD_SUBTRACT_IMMEDIATE:
            decodeMoveCompareAndSubtractImmediate(fetch_instr, &decoded_instr);
            break;
        case ALU_OPERATIONS:
            decodeALUOperation(fetch_instr, &decoded_instr);
            break;
        case HI_REGISTER_OPERATIONS_BRANCH_EXCHANGE:
            decodeHiRegisterOperationsBranchExchange(fetch_instr, &decoded_instr);
            break;
        case PC_RELATIVE_LOAD:
            decodePCRelativeLoad(fetch_instr, &decoded_instr);
            break;
        case LOAD_STORE_WITH_REGISTER_OFFSET:
            decodeLoadStoreWithRegisterOffset(fetch_instr, &decoded_instr);
            break;
        case LOAD_STORE_SIGN_EXTENDED_BYTE_HALFWORD:
            decodeLoadStoreSignExtendedByteHalfword(fetch_instr, &decoded_instr);
            break;
        case LOAD_STORE_WITH_IMMEDIATE_OFFSET:
            decodeLoadStoreWithImmediateOffset(fetch_instr, &decoded_instr);
            break;
        case LOAD_STORE_HALFWORD:
            decodeLoadStoreHalfword(fetch_instr, &decoded_instr);
            break;
        case SP_RELATIVE_LOAD_STORE:
            decodeSPRelativeLoadStore(fetch_instr, &decoded_instr);
            break;
        case LOAD_ADDRESS:
            decodeLoadAddress(fetch_instr, &decoded_instr);
            break;
        case ADD_OFFSET_TO_STACK_POINTER:
            decodeAddOffsetToStackPointer(fetch_instr, &decoded_instr);
            break;
        case PUSH_POP_REGISTERS:
            decodePushPopRegisters(fetch_instr, &decoded_instr);
            break;
        case MULTIPLE_LOAD_STORE:
            decodeMultipleLoadStore(fetch_instr, &decoded_instr);
            break;
        case CONDITIONAL_BRANCH:
            decodeConditionalBranch(fetch_instr, &decoded_instr);
            break;
        case SOFTWARE_INTERRUPT_THUMB:
            decodeSoftwareInterrupt(fetch_instr, &decoded_instr);
            break;
        case UNCONDITIONAL_BRANCH:
            decodeUnconditionalBranch(fetch_instr, &decoded_instr);
            break;
        case LONG_BRANCH_WITH_LINK:
            decodeLongBranchWithLink(fetch_instr, &decoded_instr);
            break;
        default:
            HALT_ASSERT();
    }
    return decoded_instr;
}

instr_type_THUMB_e DecoderTHUMB::decodeType(half_t fetch_instr) {
    instr_type_THUMB_e type = MOVE_SHIFTED_REGISTER; // Dummy value

    if (checkBitmask(fetch_instr, OPCODE_LSB, ADD_SUBTRACT_MASK, ADD_SUBTRACT_OPCODE)) {
        type = ADD_SUBTRACT;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, MOVE_SHIFTED_REGISTER_MASK, MOVE_SHIFTED_REGISTER_OPCODE)) {
        type = MOVE_SHIFTED_REGISTER;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, MOVE_COMPARE_ADD_SUBTRACT_IMMEDIATE_MASK, MOVE_COMPARE_ADD_SUBTRACT_IMMEDIATE_OPCODE)) {
        type = MOVE_COMPARE_ADD_SUBTRACT_IMMEDIATE;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, ALU_OPERATIONS_MASK, ALU_OPERATIONS_OPCODE)) {
        type = ALU_OPERATIONS;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, HI_REGISTER_OPERATIONS_BRANCH_EXCHANGE_MASK, HI_REGISTER_OPERATIONS_BRANCH_EXCHANGE_OPCODE)) {
        type = HI_REGISTER_OPERATIONS_BRANCH_EXCHANGE;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, PC_RELATIVE_LOAD_MASK, PC_RELATIVE_LOAD_OPCODE)) {
        type = PC_RELATIVE_LOAD;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, LOAD_STORE_WITH_REGISTER_OFFSET_MASK, LOAD_STORE_WITH_REGISTER_OFFSET_OPCODE)) {
        type = LOAD_STORE_WITH_REGISTER_OFFSET;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, LOAD_STORE_SIGN_EXTENDED_BYTE_HALFWORD_MASK, LOAD_STORE_SIGN_EXTENDED_BYTE_HALFWORD_OPCODE)) {
        type = LOAD_STORE_SIGN_EXTENDED_BYTE_HALFWORD;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, LOAD_STORE_WITH_IMMEDIATE_OFFSET_MASK, LOAD_STORE_WITH_IMMEDIATE_OFFSET_OPCODE)) {
        type = LOAD_STORE_WITH_IMMEDIATE_OFFSET;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, LOAD_STORE_HALFWORD_MASK, LOAD_STORE_HALFWORD_OPCODE)) {
        type = LOAD_STORE_HALFWORD;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, SP_RELATIVE_LOAD_STORE_MASK, SP_RELATIVE_LOAD_STORE_OPCODE)) {
        type = SP_RELATIVE_LOAD_STORE;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, LOAD_ADDRESS_MASK, LOAD_ADDRESS_OPCODE)) {
        type = LOAD_ADDRESS;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, ADD_OFFSET_TO_STACK_POINTER_MASK, ADD_OFFSET_TO_STACK_POINTER_OPCODE)) {
        type = ADD_OFFSET_TO_STACK_POINTER;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, PUSH_POP_REGISTERS_MASK, PUSH_POP_REGISTERS_OPCODE)) {
        type = PUSH_POP_REGISTERS;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, MULTIPLE_LOAD_STORE_MASK, MULTIPLE_LOAD_STORE_OPCODE)) {
        type = MULTIPLE_LOAD_STORE;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, SOFTWARE_INTERRUPT_MASK, SOFTWARE_INTERRUPT_OPCODE)) {
        type = SOFTWARE_INTERRUPT_THUMB;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, CONDITIONAL_BRANCH_MASK, CONDITIONAL_BRANCH_OPCODE)) {
        type = CONDITIONAL_BRANCH;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, UNCONDITIONAL_BRANCH_MASK, UNCONDITIONAL_BRANCH_OPCODE)) {
        type = UNCONDITIONAL_BRANCH;
    }
    else if (checkBitmask(fetch_instr, OPCODE_LSB, LONG_BRANCH_WITH_LINK_MASK, LONG_BRANCH_WITH_LINK_OPCODE)) {
        type = LONG_BRANCH_WITH_LINK;
    }
    else {
        DBG_ASSERT();
    }
    return type;
}

void DecoderTHUMB::decodeMoveShiftedRegister(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rs = getRange(fetch_instr, RS_LSB, RS_MSB);
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t offset5 = getRange(fetch_instr, OFFSET5_LSB, OFFSET5_MSB);
    byte_t op = getRange(fetch_instr, THUMB_OP1_LSB, THUMB_OP1_MSB);
    bool C;
    bool prevC = regfile->readCPSR_C();

    word_t op1 = regfile->readGPR(Rs);
    switch (op) {
        case 0: // LSL
            if (offset5 == 0) {
                C = prevC;
            } else {
                C = GET_BIT(op1, 31 - offset5 + 1);
            }
            op1 <<= offset5;
            decoded_instr->name = THUMB_LSL;
            break;
        case 1: // LSR
            if (offset5 == 0) offset5 = 32;
            C = GET_BIT(op1, offset5 - 1);
            op1 >>= offset5;
            decoded_instr->name = THUMB_LSR;
            break;
        case 2: // ASR
            if (offset5 == 0) {
                C = GET_BIT(op1, 31);
            } else {
                C = GET_BIT(op1, offset5 - 1);
            }
            op1 = (sword_t) op1 >> offset5;
            decoded_instr->name = THUMB_ASR;
            break;
        default:
            HALT_ASSERT();
    }

    decoded_instr->r1 = Rs;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->C = C;
    decoded_instr->setCondCode = true;
}

void DecoderTHUMB::decodeAddSubtract(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rs = getRange(fetch_instr, RS_LSB, RS_MSB);
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t offset3 = getRange(fetch_instr, OFFSET3_LSB, OFFSET3_MSB);
    byte_t Rn = offset3;
    bool op = GET_BIT(fetch_instr, THUMB_OP2_BIT);
    bool I = GET_BIT(fetch_instr, I_BIT);
    word_t op1 = regfile->readGPR(Rs);
    word_t op2;

    if (op) {
        decoded_instr->name = THUMB_SUB;
    }
    else {
        decoded_instr->name = THUMB_ADD;
    }

    if (I) {
        op2 = offset3;
    }
    else {
        op2 = regfile->readGPR(Rn);
    }

    decoded_instr->r1 = Rs;
    decoded_instr->r2 = Rn;
    decoded_instr->rd = Rd;
    decoded_instr->isImm = I;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->setCondCode = true;
}

void DecoderTHUMB::decodeMoveCompareAndSubtractImmediate(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD2_LSB, RD2_MSB);
    byte_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);
    byte_t op = getRange(fetch_instr, THUMB_OP1_LSB, THUMB_OP1_MSB);

    word_t op1 = regfile->readGPR(Rd);

    switch (op) {
        case 0: decoded_instr->name = THUMB_MOV; break;
        case 1: decoded_instr->name = THUMB_CMP; break;
        case 2: decoded_instr->name = THUMB_ADD; break;
        case 3: decoded_instr->name = THUMB_SUB; break;
    }

    decoded_instr->rd = Rd;
    decoded_instr->r2 = Rd;
    if (decoded_instr->name == THUMB_MOV) {
        decoded_instr->op1 = offset8;
    } else {
        decoded_instr->op1 = op1;
    }
    decoded_instr->op2 = offset8;
    decoded_instr->C = regfile->readCPSR_C();
    decoded_instr->setCondCode = true;
}

void DecoderTHUMB::decodeALUOperation(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t Rs = getRange(fetch_instr, RS_LSB, RS_MSB);
    byte_t op = getRange(fetch_instr, THUMB_OP3_LSB, THUMB_OP3_MSB);

    word_t op1 = regfile->readGPR(Rd);
    word_t op2 = regfile->readGPR(Rs);

    switch (op) {
        case 0: decoded_instr->name = THUMB_AND; break;
        case 1: decoded_instr->name = THUMB_EOR; break;
        case 2: decoded_instr->name = THUMB_LSL; break;
        case 3: decoded_instr->name = THUMB_LSR; break;
        case 4: decoded_instr->name = THUMB_ASR; break;
        case 5: decoded_instr->name = THUMB_ADC; break;
        case 6: decoded_instr->name = THUMB_SBC; break;
        case 7: decoded_instr->name = THUMB_ROR; break;
        case 8: decoded_instr->name = THUMB_TST; break;
        case 9: decoded_instr->name = THUMB_NEG; break;
        case 10: decoded_instr->name = THUMB_CMP; break;
        case 11: decoded_instr->name = THUMB_CMN; break;
        case 12: decoded_instr->name = THUMB_ORR; break;
        case 13: decoded_instr->name = THUMB_MUL; break;
        case 14: decoded_instr->name = THUMB_BIC; break;
        case 15: decoded_instr->name = THUMB_MVN; break;
        default: HALT_ASSERT();
    }

    bool prevC = regfile->readCPSR_C();
    bool C;

    if (decoded_instr->name == THUMB_LSL) {
        if (op2 == 0) {
            C = prevC;
        } else {
            C = GET_BIT(op1, 31 - op2 + 1);
        }
        op1 <<= op2;
    }
    if (decoded_instr->name == THUMB_LSR) {
        if (op2 == 0) op2 = 32;
        C = GET_BIT(op1, op2 - 1);
        op1 >>= op2;
    }
    if (decoded_instr->name == THUMB_ASR) {
        if (op2 == 0) {
            C = GET_BIT(op1, 31);
        } else {
            C = GET_BIT(op1, op2 - 1);
        }
        op1 = (sword_t) op1 >> op2;
    }
    if (decoded_instr->name == THUMB_ROR) {
        // Special case, RRX
        if (op2 == 0) {
            op2 = 1;
            dword_t longShift = (dword_t) prevC << 32;
            longShift |= op1;
            C = GET_BIT(op1, 0);
            op1 = longShift >> op2;
        }
        else {
            op2 %= 32;
            if (op2 == 0) op2 = 32; // Different than RRX 0 encoding
            dword_t longShift = (dword_t) op1 << 32;
            op1 = op1 >> op2;
            op1 |= longShift >> op2;
            C = GET_BIT(op1, op2 - 1);
        }
    }
    else C = prevC;

    decoded_instr->r1 = Rd;
    decoded_instr->r2 = Rs;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->C = C;
    decoded_instr->setCondCode = true;
}

void DecoderTHUMB::decodeHiRegisterOperationsBranchExchange(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t Rs = getRange(fetch_instr, RS_LSB, RS_MSB);
    byte_t op = getRange(fetch_instr, THUMB_OP4_LSB, THUMB_OP4_MSB);
    bool H1 = GET_BIT(fetch_instr, H1_BIT);
    bool H2 = GET_BIT(fetch_instr, H2_BIT);
    DBG_ASSERT_E(!H1 && !H2 && (op != 3)); // Undefined behavior
    DBG_ASSERT_E(H1 && (op == 3)); // Undefined behavior

    if (H1) Rd += 8;
    if (H2) Rs += 8;
    word_t op1 = regfile->readGPR(Rd);
    word_t op2 = regfile->readGPR(Rs);

    if (Rs == PC) {
        op2 += 2; // + 4, but prefetch is already done
    }

    switch (op) {
        case 0: decoded_instr->name = THUMB_ADD; break;
        case 1: decoded_instr->name = THUMB_CMP; break;
        case 2: decoded_instr->name = THUMB_MOV; break;
        case 3: decoded_instr->name = THUMB_BX; break;
        default: HALT_ASSERT();
    }

    decoded_instr->r1 = Rd;
    decoded_instr->r2 = Rs;
    decoded_instr->rd = Rd;
    if (decoded_instr->name == THUMB_MOV) {
        decoded_instr->op1 = op2;
    } else {
        decoded_instr->op1 = op1;
    }
    decoded_instr->op2 = op2;
    if (decoded_instr->name == THUMB_CMP) {
        decoded_instr->setCondCode = true;
    }
    else {
        decoded_instr->setCondCode = false;
    }
}

void DecoderTHUMB::decodePCRelativeLoad(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD2_LSB, RD2_MSB);
    byte_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);

    word_t op3 = offset8 << 2;
    word_t pc = regfile->readGPR(PC) + 2; // + 4, but prefetch is already done
    pc &= ~3; // Clear last two bits

    decoded_instr->name = THUMB_LDR;
    decoded_instr->rd = Rd;
    decoded_instr->op2 = pc; // base
    decoded_instr->op3 = op3; // offset
    decoded_instr->isByte = false;
}

void DecoderTHUMB::decodeLoadStoreWithRegisterOffset(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t Rb = getRange(fetch_instr, RB1_LSB, RB1_MSB);
    byte_t Ro = getRange(fetch_instr, RO_LSB, RO_MSB);
    bool B = GET_BIT(fetch_instr, B_BIT);
    bool L = GET_BIT(fetch_instr, L_BIT);

    word_t op1 = regfile->readGPR(Rd);
    word_t op2 = regfile->readGPR(Rb);
    word_t op3 = regfile->readGPR(Ro);

    if (L) {
        decoded_instr->name = THUMB_LDR;
    }
    else {
        decoded_instr->name = THUMB_STR;
    }

    decoded_instr->r1 = Rd;
    decoded_instr->r2 = Rb;
    decoded_instr->r3 = Ro;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->op3 = op3;
    decoded_instr->isByte = B;
}

void DecoderTHUMB::decodeLoadStoreSignExtendedByteHalfword(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t Rb = getRange(fetch_instr, RB1_LSB, RB1_MSB);
    byte_t Ro = getRange(fetch_instr, RO_LSB, RO_MSB);
    bool S = GET_BIT(fetch_instr, S1_BIT);
    bool H = GET_BIT(fetch_instr, H_BIT);

    word_t op1 = regfile->readGPR(Rd);
    word_t op2 = regfile->readGPR(Rb);
    word_t op3 = regfile->readGPR(Ro);

    if (!H && !S) {
        decoded_instr->name = THUMB_STRH;
    }
    else {
        decoded_instr->name = THUMB_LDRH;
    }

    decoded_instr->r1 = Rd;
    decoded_instr->r2 = Rb;
    decoded_instr->r3 = Ro;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->op3 = op3;
    decoded_instr->isHalfword = H;
    decoded_instr->isSigned= S;
}

void DecoderTHUMB::decodeLoadStoreWithImmediateOffset(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t Rb = getRange(fetch_instr, RB1_LSB, RB1_MSB);
    byte_t offset5 = getRange(fetch_instr, OFFSET5_LSB, OFFSET5_MSB);
    bool B = GET_BIT(fetch_instr, B_BIT);
    bool L = GET_BIT(fetch_instr, L_BIT);
    word_t offset;

    if (B) {
        offset = offset5;
    }
    else {
        offset = offset5 << 2;
    }

    word_t op1 = regfile->readGPR(Rd);
    word_t op2 = regfile->readGPR(Rb);

    if (L) {
        decoded_instr->name = THUMB_LDR;
    }
    else {
        decoded_instr->name = THUMB_STR;
    }

    decoded_instr->r1 = Rd;
    decoded_instr->r2 = Rb;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->op3 = offset;
    decoded_instr->isByte = B;
}

void DecoderTHUMB::decodeLoadStoreHalfword(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD1_LSB, RD1_MSB);
    byte_t Rb = getRange(fetch_instr, RB1_LSB, RB1_MSB);
    byte_t offset5 = getRange(fetch_instr, OFFSET5_LSB, OFFSET5_MSB);
    bool L = GET_BIT(fetch_instr, L_BIT);

    word_t op1 = regfile->readGPR(Rd);
    word_t op2 = regfile->readGPR(Rb);
    word_t op3 = offset5 << 1;

    if (L) {
        decoded_instr->name = THUMB_LDRH;
    }
    else {
        decoded_instr->name = THUMB_STRH;
    }

    decoded_instr->r1 = Rd;
    decoded_instr->r2 = Rb;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->op3 = op3;
    decoded_instr->isHalfword = true;
    decoded_instr->isSigned= false;
}

void DecoderTHUMB::decodeSPRelativeLoadStore(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD2_LSB, RD2_MSB);
    byte_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);
    bool L = GET_BIT(fetch_instr, L_BIT);

    word_t op1 = regfile->readGPR(Rd);
    word_t op2 = regfile->readGPR(SP);
    word_t op3 = offset8 << 2;

    if (L) {
        decoded_instr->name = THUMB_LDR;
    }
    else {
        decoded_instr->name = THUMB_STR;
    }

    decoded_instr->r1 = Rd;
    decoded_instr->r2 = SP;
    decoded_instr->rd = Rd;
    decoded_instr->op1 = op1;
    decoded_instr->op2 = op2;
    decoded_instr->op3 = op3;
    decoded_instr->isByte = false;
}

void DecoderTHUMB::decodeLoadAddress(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t Rd = getRange(fetch_instr, RD2_LSB, RD2_MSB);
    byte_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);
    bool _SP = GET_BIT(fetch_instr, SP_BIT);

    word_t op2 = offset8 << 2;

    if (_SP) {
        decoded_instr->r1 = SP;
        decoded_instr->op1 = regfile->readGPR(SP);
    }
    else {
        decoded_instr->r1 = PC;
        decoded_instr->op1 = regfile->readGPR(PC);
    }

    decoded_instr->name = THUMB_ADD;
    decoded_instr->rd = Rd;
    decoded_instr->op2 = op2;
}

void DecoderTHUMB::decodeAddOffsetToStackPointer(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t offset7 = getRange(fetch_instr, OFFSET7_LSB, OFFSET7_MSB);
    bool S = GET_BIT(fetch_instr, S2_BIT);

    word_t op2 = offset7 << 2;
    if (S) op2 = -op2;

    decoded_instr->name = THUMB_ADD;
    decoded_instr->r1 = SP;
    decoded_instr->rd = SP;
    decoded_instr->op1 = regfile->readGPR(SP);
    decoded_instr->op2 = op2;
}

void DecoderTHUMB::decodePushPopRegisters(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    word_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);
    bool R = GET_BIT(fetch_instr, R_BIT);
    bool L = GET_BIT(fetch_instr, L_BIT);

    if (L) {
        offset8 |= (R << PC); // If R, Add PC to the list
        decoded_instr->name = THUMB_POP;
    }
    else {
        offset8 |= (R << LR); // If R, Add LR to the list
        decoded_instr->name = THUMB_PUSH;
    }

    decoded_instr->regList = offset8;
}

void DecoderTHUMB::decodeMultipleLoadStore(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);
    byte_t Rb = getRange(fetch_instr, RB2_LSB, RB2_MSB);
    bool L = GET_BIT(fetch_instr, L_BIT);

    word_t op1 = regfile->readGPR(Rb);

    if (L) {
        decoded_instr->name = THUMB_LDMIA;
    }
    else {
        decoded_instr->name = THUMB_STMIA;
    }

    decoded_instr->r1 = Rb;
    decoded_instr->op1 = op1;
    decoded_instr->regList = offset8;
}

void DecoderTHUMB::decodeConditionalBranch(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    sbyte_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);
    byte_t cond = getRange(fetch_instr, THUMB_CONDITION_LSB, THUMB_CONDITION_MSB);

    HALT_ASSERT_E(cond == 0xe || cond == 0xf); // Undefined behavior
    sword_t op1 = SIGN_EXT((offset8 << 1), 8, 31) + regfile->readGPR(PC) + 2; // Normally 4 but prefetch

    decoded_instr->name = THUMB_Bxx;
    decoded_instr->op1 = op1;
    decoded_instr->branchCond = cond;
}

void DecoderTHUMB::decodeSoftwareInterrupt(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    byte_t offset8 = getRange(fetch_instr, OFFSET8_LSB, OFFSET8_MSB);

    decoded_instr->name = THUMB_SWI;
    decoded_instr->op1 = offset8;
}

void DecoderTHUMB::decodeUnconditionalBranch(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    word_t offset11 = getRange(fetch_instr, OFFSET11_LSB, OFFSET11_MSB);

    decoded_instr->name = THUMB_B;
    decoded_instr->op1 = SIGN_EXT(((sword_t) offset11 << 1), 11, 31) + regfile->readGPR(PC) + 2; // Normally 4 but prefetch
}

void DecoderTHUMB::decodeLongBranchWithLink(half_t fetch_instr, instr_THUMB_st *decoded_instr) {
    word_t offset11 = getRange(fetch_instr, OFFSET11_LSB, OFFSET11_MSB);
    bool H = GET_BIT(fetch_instr, H_BIT);

    word_t op1;
    if (H) {
        op1 = offset11 << 1;
        op1 += regfile->readGPR(LR);
        op1 = SIGN_EXT(op1, 23, 31);
        decoded_instr->name = THUMB_BLH;
    }
    else {
        op1 = offset11 << 12;
        op1 = SIGN_EXT(op1, 12, 31);
        op1 += regfile->readGPR(PC) + 2;
        decoded_instr->name = THUMB_BL;
    }

    decoded_instr->op1 = op1;
}

bool DecoderTHUMB::checkBitmask(half_t instr, byte_t field_lsb, byte_t mask, byte_t value) {
    byte_t field = (byte_t) (instr >> field_lsb);
    byte_t bitmask = field & mask;
    if (bitmask == value) return true;
    else return false;
}

word_t DecoderTHUMB::getRange(word_t word, byte_t lsb, byte_t msb) {
    DBG_ASSERT_E(lsb > msb);
    word_t result = word << 31 - msb;
    result >>= (31 - msb) + lsb; // word_t is unsigned
    return result;
}
