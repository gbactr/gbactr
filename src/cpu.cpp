#include "cpu.h"
using namespace std;

CPU::CPU() {
    // Constructor
    fetchUnit = FetchUnit::getInstance();
    decoder = Decoder::getInstance();
    decoderTHUMB = DecoderTHUMB::getInstance();
    executionUnit = ExecutionUnit::getInstance();
    executionUnitTHUMB = ExecutionUnitTHUMB::getInstance();
#ifdef FTR_TRACE_LOG
    traceLog = TraceLog::getInstance();
#endif // FTR_TRACE_LOG
#ifdef FTR_TRANS_LOG
    transLog = TransLog::getInstance();
#endif // FTR_TANS_LOG
    regfile = Regfile::getInstance();
}

//Singleton
CPU *CPU::cpu = nullptr;

CPU *CPU::getInstance() {
    if (cpu == nullptr) {
        cpu = new CPU();
    }
    return cpu;
}

void CPU::executeInstruction() {
    if (regfile->getCPUMode() == ARM) {
        executeInstructionARM();
    }
    else if (regfile->getCPUMode() == THUMB) {
        executeInstructionTHUMB();
    }
    else {
        HALT_ASSERT();
    }
}

void CPU::executeInstructionARM() {
    // INSTRUCTION FETCH
#ifdef FTR_TRACE_LOG
    traceLog->tracePC(regfile->readGPR(PC));
#endif // FTR_TRACE_LOG
    word_t instr = fetchUnit->fetchInstruction();
#ifdef FTR_TRACE_LOG
    traceLog->traceFetchInstr(instr);
#endif // FTR_TRACE_LOG

    // INSTRUCTION DECODE
    instr_st decoded_instr = decoder->decodeInstruction(instr);
#ifdef FTR_TRACE_LOG
    traceLog->traceInstrAsm(decoded_instr);
    traceLog->traceRegisters();
#endif // FTR_TRACE_LOG

    // INSTRUCTION EXECUTE
    executionUnit->executeInstruction(decoded_instr);

#ifdef FTR_TRANS_LOG
    transLog->transaction(decoded_instr);
#endif // TRANS_LOG
}

void CPU::executeInstructionTHUMB() {
    // INSTRUCTION FETCH
#ifdef FTR_TRACE_LOG
    traceLog->tracePC(regfile->readGPR(PC));
#endif // FTR_TRACE_LOG
    half_t instr = fetchUnit->fetchInstructionTHUMB();
#ifdef FTR_TRACE_LOG
    traceLog->traceFetchInstrTHUMB(instr);
#endif // FTR_TRACE_LOG

    // INSTRUCTION DECODE
    instr_THUMB_st decoded_instr = decoderTHUMB->decodeInstruction(instr);
#ifdef FTR_TRACE_LOG
    traceLog->traceInstrAsmTHUMB(decoded_instr);
    traceLog->traceRegisters();
#endif // FTR_TRACE_LOG

    executionUnitTHUMB->executeInstruction(decoded_instr);
}