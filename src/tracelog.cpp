#include "tracelog.h"
using namespace std;

TraceLog::TraceLog() {
    // Constructor
    regfile = Regfile::getInstance();

    traceFile.open(GBACTR_TRACE, ios::out | ios::binary);
}

//Singleton
TraceLog *TraceLog::traceLog = nullptr;

TraceLog *TraceLog::getInstance() {
    if (traceLog == nullptr) {
        traceLog = new TraceLog();
    }
    return traceLog;
}

void TraceLog::tracePC(addr_t pc) {
    traceFile << HEX_COUT(8) << pc << " ";
}

void TraceLog::traceFetchInstr(word_t instr) {
    traceFile << HEX_COUT(8) << instr << " ";
}

void TraceLog::traceFetchInstrTHUMB(half_t instr) {
    traceFile << HEX_COUT(4) << instr << " ";
}

void TraceLog::traceInstrAsm(instr_st instr) {
    string name = getInstrName(instr);
    traceFile << name << endl;
    // TODO: register encoding
}

void TraceLog::traceInstrAsmTHUMB(instr_THUMB_st instr) {
    string name = getInstrNameTHUMB(instr);
    traceFile << name << endl;
    // TODO: register encoding
}

void TraceLog::traceRegisters() {
    for (int i = 0; i<NUM_GPR; ++i) {
        traceFile << "r" << DEC_COUT(2) << i << "=" << HEX_COUT(8) << regfile->readGPR(i) << " ";
    }
    word_t psr = regfile->PSRToWord(regfile->getCPSR()); // TODO: Single function
    traceFile << "r16=" << HEX_COUT(8) << psr << " ";
    traceFile << (GET_BIT(psr, 31) ? "N" : "n");
    traceFile << (GET_BIT(psr, 30) ? "Z" : "z");
    traceFile << (GET_BIT(psr, 29) ? "C" : "c");
    traceFile << (GET_BIT(psr, 28) ? "V" : "v");
    traceFile << (GET_BIT(psr, 7) ? "I" : "i");
    traceFile << (GET_BIT(psr, 6) ? "F" : "f");
    traceFile << (GET_BIT(psr, 5) ? "T" : "t");
    traceFile << endl;
}

// TODO: Move this somewhere else so translog can also access it
// It is duplicated code!
string TraceLog::getInstrName(instr_st instr) {
    string s;
    switch(instr.name) {
        case (ADC): s = "adc"; break;
        case (ADD): s = "add"; break;
        case (AND): s = "and"; break;
        case (B): s = "b"; break;
        case (BIC): s = "bic"; break;
        case (BL): s = "bl"; break;
        case (BX): s = "bx"; break;
        case (CDP): s = "cdp"; break;
        case (CMN): s = "cmn"; break;
        case (CMP): s = "cmps"; break;
        case (EOR): s = "eor"; break;
        case (LDC): s = "ldc"; break;
        case (LDM): s = "ldm"; break;
        case (LDR): s = "ldr"; break;
        case (MCR): s = "mcr"; break;
        case (MLA): s = "mla"; break;
        case (MLAL): s = "mlal"; break;
        case (MOV): s = "mov"; break;
        case (MRC): s = "mrc"; break;
        case (MRS): s = "mrs"; break;
        case (MSR): s = "msr"; break;
        case (MUL): s = "mul"; break;
        case (MULL): s = "mull"; break;
        case (MVN): s = "mvn"; break;
        case (ORR): s = "orr"; break;
        case (RSB): s = "rsb"; break;
        case (RSC): s = "rsc"; break;
        case (SBC): s = "sbc"; break;
        case (STC): s = "stc"; break;
        case (STM): s = "stm"; break;
        case (STR): s = "str"; break;
        case (SUB): s = "sub"; break;
        case (SWI): s = "swi"; break;
        case (SWP): s = "swp"; break;
        case (TEQ): s = "teqs"; break;
        case (TST): s = "tst"; break;
        case (UND): s = "und"; break;
    }
    return s;
}

// TODO: Move this somewhere else so translog can also access it
// It is duplicated code!
string TraceLog::getInstrNameTHUMB(instr_THUMB_st instr) {
    string s;
    switch(instr.name) {
        case THUMB_ADC: s = "adc"; break;
        case THUMB_ADD: s = "add"; break;
        case THUMB_AND: s = "and"; break;
        case THUMB_ASR: s = "asr"; break;
        case THUMB_B: s = "b"; break;
        case THUMB_Bxx: 
            switch (instr.branchCond) {
                case EQ: s = "beq"; break;
                case NE: s = "bne"; break;
                case CS: s = "bcs"; break;
                case CC: s = "bcc"; break;
                case MI: s = "bmi"; break;
                case PL: s = "bpl"; break;
                case VS: s = "bvs"; break;
                case VC: s = "bvc"; break;
                case HI: s = "bhi"; break;
                case LS: s = "bls"; break;
                case GE: s = "bge"; break;
                case LT: s = "blt"; break;
                case GT: s = "bgt"; break;
                case LE: s = "ble"; break;       
                default: DBG_ASSERT();
            }
            break;
        case THUMB_BIC: s = "bic"; break;
        case THUMB_BL: s = "bl"; break;
        case THUMB_BLH: s = "blh"; break;
        case THUMB_BX: s = "bx"; break;
        case THUMB_CMN: s = "cmn"; break;
        case THUMB_CMP: s = "cmps"; break;
        case THUMB_EOR: s = "eor"; break;
        case THUMB_LDMIA: s = "ldmia"; break;
        case THUMB_LDR: s = "ldr"; break;
        case THUMB_LDRB: s = "ldrb"; break;
        case THUMB_LDRH: s = "ldrh"; break;
        case THUMB_LSL: s = "lsl"; break;
        case THUMB_LDSB: s = "ldsb"; break;
        case THUMB_LDSH: s = "ldsh"; break;
        case THUMB_LSR: s = "lsr"; break;
        case THUMB_MOV: s = "mov"; break;
        case THUMB_MUL: s = "mul"; break;
        case THUMB_MVN: s = "mvn"; break;
        case THUMB_NEG: s = "neg"; break;
        case THUMB_ORR: s = "orr"; break;
        case THUMB_POP: s = "pop"; break;
        case THUMB_PUSH: s = "push"; break;
        case THUMB_ROR: s = "ror"; break;
        case THUMB_SBC: s = "sbc"; break;
        case THUMB_STMIA: s = "stmia"; break;
        case THUMB_STR: s = "str"; break;
        case THUMB_STRB: s = "strb"; break;
        case THUMB_STRH: s = "strh"; break;
        case THUMB_SWI: s = "swi"; break;
        case THUMB_SUB: s = "sub"; break;
        case THUMB_TST: s = "tst"; break;
    }
    return s;
}