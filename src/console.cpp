#include <iostream>
#include <windows.h>
#include <tchar.h>
#include "cpu.h"
#include "comparer.h"
using namespace std;

CPU *cpu;
Comparer comparer;
TraceLog *tracelog;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
void vBlank();

HWND hwnd;
//extern "C" {
    int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
        WNDCLASSEX wc;
        //HWND hwnd;
        MSG Msg = {0};
        bool bDone = false;

        wc.cbSize = sizeof(WNDCLASSEX);
        wc.style = 0;
        wc.lpfnWndProc = WndProc;
        wc.cbClsExtra = 0;
        wc.cbWndExtra = 0;
        wc.hInstance = hInstance;
        wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
        wc.hCursor = LoadCursor(NULL, IDC_ARROW);
        wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND);
        wc.lpszMenuName = NULL;
        wc.lpszClassName = TEXT("GBA_CTR");
        wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

        if (!RegisterClassEx(&wc)) {
            MessageBox(NULL, TEXT("Window Registration Failed!"), TEXT("Error!"), MB_ICONEXCLAMATION | MB_OK);
            return 0;
        }

        hwnd = CreateWindowEx(
            WS_EX_CLIENTEDGE,
            TEXT("GBA_CTR"),
            TEXT("GBA CTR Emulator"),
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, CW_USEDEFAULT, 256, 224,
            NULL, NULL, hInstance, NULL);

        if (hwnd == NULL) {
            MessageBox(NULL, TEXT("Window Creation Failed!"), TEXT("Error!"), MB_ICONEXCLAMATION | MB_OK);
            return 0;
        }

        ShowWindow(hwnd, SW_SHOW);
        UpdateWindow(hwnd);
        DisableProcessWindowsGhosting();
        while (!bDone) {
            if (PeekMessage(&Msg, hwnd, 0, 0, PM_REMOVE)) {
                TranslateMessage(&Msg);
                DispatchMessage(&Msg);
                if (Msg.message == WM_QUIT) bDone = true;
            }
        }
        DestroyWindow(hwnd);
        UnregisterClass(TEXT("GBA CTR"), hInstance);
        return 0;
    }
//}

int tick = 0;
COLORREF *arr;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
    //TEXT can be STATIC, BUTTON or TEXT
    //if (tick == 20) {
        //CreateWindow(TEXT("STATIC"), TEXT(tick1), 
            //WS_VISIBLE | WS_CHILD, 
            //10, 10, 100, 25,
            //hwnd, (HMENU) NULL, NULL, NULL);
    //}
    //tick++;
    switch(msg) {
        case WM_CREATE:
            cpu = CPU::getInstance();
            tracelog = TraceLog::getInstance();
            arr = (COLORREF *) malloc(256*224 * sizeof(COLORREF));
            SetTimer(hwnd, 1, 16, (TIMERPROC) &vBlank);
            break;
        case WM_TIMER: {
            vBlank();
            break;
        }
        case WM_CLOSE:
            DestroyWindow(hwnd);
            free(arr);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            free(arr);
            break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

void vBlank() {
    cpu->executeInstruction();
    comparer.compare();
    //short xState = GetAsyncKeyState(0x58);
    //if ((xState >> 15) & 1) cout << "x key press" << endl;
    HDC hdc = GetDC(hwnd);
    HBITMAP map = CreateBitmap(256, 224, 1, 8*4, (void *) arr);
    HDC src = CreateCompatibleDC(hdc);
    SelectObject(src, map);
    BitBlt(hdc, 0, 0, 256, 224, src, 0, 0, SRCCOPY);
    DeleteObject(map);
    DeleteDC(src);
    ReleaseDC(NULL, hdc);
    //free(arr);
}