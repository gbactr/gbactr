#include "comparer.h"
using namespace std;

Comparer::Comparer() {
    // Constructor
}

void Comparer::compare() {
    if (!traceFile.is_open()) {
        traceFile.open(GBACTR_TRACE, ios::in);
    }
    if (!goldenTraceFile.is_open()) {
        goldenTraceFile.open(GOLDEN_TRACE, ios::in);
    }
    string trace, goldenTrace;

    // [PC]
    traceFile >> trace;
    goldenTraceFile >> goldenTrace;
    checkTraceDifference(trace, goldenTrace);

    // Instruction encoding
    traceFile >> trace;
    goldenTraceFile >> goldenTrace;
    checkTraceDifference(trace, goldenTrace);

    // Instruction name
    traceFile >> trace;
    goldenTraceFile >> goldenTrace;
    //checkTraceDifference(trace, goldenTrace);

    // R0
    traceFile >> trace;
    do { // Avoid register operands for now
        goldenTraceFile >> goldenTrace;
    } while (goldenTrace.substr(0,4) != "r00=");
    checkTraceDifference(trace, goldenTrace);

    for (int i = 1; i < 16; ++i) {
        traceFile >> trace;
        goldenTraceFile >> goldenTrace;
        checkTraceDifference(trace, goldenTrace);
    }
    traceFile >> trace; // R16, which we are not checking

    // condition bits, which we are not checking
    traceFile >> trace;
 //   checkTraceDifference(trace, goldenTrace);
    traceLine++;
}

void Comparer::checkTraceDifference(string trace, string goldenTrace) {
    if (trace != goldenTrace) {
        cout << "TRACE DIFFERENCE ON LINE " << traceLine << ":" << endl <<
                "GBACTR: " << trace << endl <<
                "GOLDEN: " << goldenTrace << endl;
        HALT_ASSERT();
    }
}