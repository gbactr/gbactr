#include "execution_unit.h"
#include <iostream>
#include <iomanip>
using namespace std;

ExecutionUnit::ExecutionUnit() {
    // Constructor
    regfile = Regfile::getInstance();
    mem = Mem::getInstance();
}

// Singleton
ExecutionUnit *ExecutionUnit::executionUnit = nullptr;

ExecutionUnit *ExecutionUnit::getInstance() {
    if (executionUnit == nullptr) {
        executionUnit = new ExecutionUnit();
    }
    return executionUnit;
}

void ExecutionUnit::executeInstruction(instr_st instr) {
    // Determine if the condition code is fulfilled
    if(isConditionFulfilled(instr.condition)) {
        switch(instr.name) {
            case ADC:  executeADC(instr);  break;
            case ADD:  executeADD(instr);  break;
            case AND:  executeAND(instr);  break;
            case B:    executeB(instr);    break;
            case BIC:  executeBIC(instr);  break;
            case BL:   executeBL(instr);   break;
            case BX:   executeBX(instr);   break;
            case CDP:  executeCDP(instr);  break;
            case CMN:  executeCMN(instr);  break;
            case CMP:  executeCMP(instr);  break;
            case EOR:  executeEOR(instr);  break;
            case LDC:  executeLDC(instr);  break;
            case LDM:  executeLDM(instr);  break;
            case LDR:  executeLDR(instr);  break;
            case LDRH: executeLDRH(instr); break;
            case MCR:  executeMCR(instr);  break;
            case MLA:  executeMLA(instr);  break;
            case MLAL: executeMLAL(instr); break;
            case MOV:  executeMOV(instr);  break;
            case MRC:  executeMRC(instr);  break;
            case MRS:  executeMRS(instr);  break;
            case MSR:  executeMSR(instr);  break;
            case MUL:  executeMUL(instr);  break;
            case MULL: executeMULL(instr); break;
            case MVN:  executeMVN(instr);  break;
            case ORR:  executeORR(instr);  break;
            case RSB:  executeRSB(instr);  break;
            case RSC:  executeRSC(instr);  break;
            case SBC:  executeSBC(instr);  break;
            case STC:  executeSTC(instr);  break;
            case STM:  executeSTM(instr);  break;
            case STR:  executeSTR(instr);  break;
            case STRH: executeSTRH(instr); break;
            case SUB:  executeSUB(instr);  break;
            //case SWI:  executeSWI(instr);  break;
            case SWP:  executeSWP(instr);  break;
            case TEQ:  executeTEQ(instr);  break;
            case TST:  executeTST(instr);  break;
            case UND:  executeUND(instr);  break;
            default: DBG_ASSERT();
        }
    }

}

bool ExecutionUnit::isConditionFulfilled(condition_e cond_code) {
    bool result;
    PSR_t cpsr = regfile->getCPSR();

    switch (cond_code) {
        case EQ: result =  cpsr.Z;                       break;
        case NE: result = !cpsr.Z;                       break;
        case CS: result =  cpsr.C;                       break;
        case CC: result = !cpsr.C;                       break;
        case MI: result =  cpsr.N;                       break;
        case PL: result = !cpsr.N;                       break;
        case VS: result =  cpsr.V;                       break;
        case VC: result = !cpsr.V;                       break;
        case HI: result =  cpsr.C && !cpsr.Z;            break;
        case LS: result = !cpsr.C ||  cpsr.Z;            break;
        case GE: result =  cpsr.N ==  cpsr.V;            break;
        case LT: result =  cpsr.N !=  cpsr.V;            break;
        case GT: result = !cpsr.Z && (cpsr.N == cpsr.V); break;
        case LE: result =  cpsr.Z || (cpsr.N != cpsr.V); break;       
        case AL: result =  true;                         break;
        default: DBG_ASSERT();
    }

    return result;
}

/*
**********************
* CPSR set functions *
**********************
*/

void ExecutionUnit::setLogicalInstrCPSR(instr_st instr, word_t result) {
    // If rd is R15 and S is set, set CPSR to SPSR of current mode
    if (instr.rd == PC && instr.setCondition) {
        PSR_t spsr = regfile->getSPSR();
        regfile->writeCPSR(spsr);
    } 
    else {
        // V flag remains unchanged
        // C flag is set to the carry out from the barrel shifter
        regfile->writeCPSR_C(instr.C);
        // Z flag is set if the result is all zeroes
        regfile->writeCPSR_Z(result == 0);
        // N flag is set to the logical value of bit 31 of the result
        regfile->writeCPSR_N(GET_BIT(result, 31));
    }
}

void ExecutionUnit::setArithmeticalInstrCPSR(instr_st instr, word_t op1, word_t op2, word_t result, bool carry) {
    // V flag records if an overflow happened
    if (GET_BIT(op1, 31) == GET_BIT(op2, 31)) {
        regfile->writeCPSR_V(GET_BIT(op1, 31) != GET_BIT(result, 31));
    }
    // C flag will be set to the carry out of bit 31 of the ALU
    regfile->writeCPSR_C(carry);
    // Z flag is set if the result is all zeroes
    regfile->writeCPSR_Z(result == 0);
    // N flag will be set to the value of bit 31 of the result
    regfile->writeCPSR_N(GET_BIT(result, 31));
}

void ExecutionUnit::setMultiplyInstrCPSR(instr_st instr, word_t result) {
    // N flag is set to bit 31 of the result
    regfile->writeCPSR_N(GET_BIT(result, 31));
    // Z flag is set if the result is zero
    regfile->writeCPSR_Z(result == 0);
}

void ExecutionUnit::setMultiplyLongInstrCPSR(instr_st instr, dword_t result) {
    // N flag is set to bit 63 of the result
    regfile->writeCPSR_N(GET_BIT(result, 63));
    // Z flag is set if the result is zero
    regfile->writeCPSR_Z(result == 0);
}

/*
***********************************
* Instruction execution functions *
***********************************
*/

void ExecutionUnit::executeADC(instr_st instr) {
    dword_t result = instr.op1 + instr.op2 + regfile->readCPSR_C();
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}

void ExecutionUnit::executeADD(instr_st instr) {
    dword_t result = instr.op1 + instr.op2;
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}

void ExecutionUnit::executeAND(instr_st instr) {
    word_t result = instr.op1 & instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeB(instr_st instr) {
    sword_t offset = (sword_t)SIGN_EXT(instr.op1, 23, 31);
    offset <<= 2;

    regfile->writeGPR(PC, (sword_t) (regfile->readGPR(PC) + 4 + offset)); // Usually + 8, but prefetch already done
}

void ExecutionUnit::executeBIC(instr_st instr) {
    word_t result = instr.op1 & (~instr.op2);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeBL(instr_st instr) {
    sword_t offset = (sword_t)SIGN_EXT(instr.op1, 23, 31);
    offset <<= 2;

    regfile->writeGPR(LR, regfile->readGPR(PC)); // Prefetch already done
    regfile->writeGPR(PC, (sword_t) (regfile->readGPR(PC) + 4 + offset)); // Usually + 8, but prefetch already done
}

void ExecutionUnit::executeBX(instr_st instr) {
    word_t address = instr.op1 & (-2); // Clear bit 0
    cpu_mode_e cpu_mode = GET_BIT(instr.op1, 0) ? THUMB : ARM;

    regfile->writeGPR(PC, address);
    regfile->setCPUMode(cpu_mode);
}

void ExecutionUnit::executeCDP(instr_st instr) {
    // No coprocessor on GBA
    DBG_ASSERT();
}

void ExecutionUnit::executeCMN(instr_st instr) {
    dword_t result = instr.op1 + instr.op2;
    bool carry = GET_BIT(result, 32);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}

void ExecutionUnit::executeCMP(instr_st instr) {
    dword_t result = instr.op1 - instr.op2;
    bool carry = !GET_BIT(result, 32);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, -instr.op2, result, carry);
    }
}

void ExecutionUnit::executeEOR(instr_st instr) {
    word_t result = instr.op1 ^ instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeLDC(instr_st instr) {
    // No coprocessor on GBA
    DBG_ASSERT();
}

void ExecutionUnit::executeLDM(instr_st instr) {
    word_t regList = instr.regList;
    DBG_ASSERT_E(regList == 0);
    word_t result;
    addr_t address = instr.op1;
    word_t offset = instr.isUp ? 4 : -4;
    mode_e current_mode = regfile->getCurrentMode();

    if (instr.isPSR && !GET_BIT(regList, 15)) {
        DBG_ASSERT_E(instr.isWriteBack); // Writeback and PSR transfer can't be active at the same time if !R15
        regfile->setCurrentMode(USER);
    }

    for (int i = instr.isUp ? 0 : NUM_GPR ; i != (instr.isUp ? NUM_GPR : 0); i += instr.isUp ? 1 : -1) {
        if (GET_BIT(regList, i)) {
            if (instr.isPreIndexing) address += offset;
            MemReq req(READ_WORD, address);
            mem->request(&req);
            if (!instr.isPreIndexing) address += offset;

            regfile->writeGPR(i, req.data);
        }
    }

    if (instr.isPSR) {
        if (!GET_BIT(regList, 15)) {
            regfile->setCurrentMode(current_mode);
        }
        PSR_t psr = regfile->getSPSR();
        regfile->writeCPSR(psr);
    }
    if (instr.isWriteBack) {
        regfile->writeGPR(instr.r1, address);
    }
}

void ExecutionUnit::executeLDR(instr_st instr) {
    addr_t offset = instr.op2;
    word_t result;
    addr_t address = instr.op1;

    if (!instr.isUp) offset = -offset;
    if (instr.isPreIndexing) address += offset;

    MemReq req(instr.isByte ? READ_BYTE : READ_WORD, address);
    mem->request(&req);
    regfile->writeGPR(instr.rd, req.data);

    // 4.9.1 - In case of post-indexed addressing, the write back bit is redundant and is always
    // set to zero, since the old base value can be retained by setting the offset to zero.
    // Therefore post-indexed data transfers always write back the modified base
    if (instr.isWriteBack || (!instr.isPreIndexing)) {
        if (!instr.isPreIndexing) address += offset;
        regfile->writeGPR(instr.r1, address);
    }
}
void ExecutionUnit::executeLDRH(instr_st instr) {
    addr_t offset = instr.op2;
    word_t result;
    addr_t address = instr.op1;

    if (!instr.isUp) offset = -offset;
    if (instr.isPreIndexing) address += offset;

    MemReq req(instr.isHalfword ? READ_HALF : READ_BYTE, address);
    mem->request(&req);
    if (instr.isSigned) {
        if (instr.isHalfword) {
            req.data |= (-1 << 16);
        }
        else {
            req.data |= (-1 << 8);
        }
    }
    regfile->writeGPR(instr.rd, req.data);

    // 4.9.1 - In case of post-indexed addressing, the write back bit is redundant and is always
    // set to zero, since the old base value can be retained by setting the offset to zero.
    // Therefore post-indexed data transfers always write back the modified base
    if (instr.isWriteBack || (!instr.isPreIndexing)) {
        if (!instr.isPreIndexing) address += offset;
        regfile->writeGPR(instr.r1, address);
    }
}

void ExecutionUnit::executeMCR(instr_st instr) {
    // No coprocessor on GBA
    DBG_ASSERT();
}

void ExecutionUnit::executeMLA(instr_st instr) {
    word_t result = (instr.op2 * instr.op3) + instr.op1;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setMultiplyInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeMLAL(instr_st instr) {
    // Convert source operands to signed dword operands
    sword_t sop2 = (sword_t) instr.op2;
    sword_t sop3 = (sword_t) instr.op3;
    sdword_t sdop2 = (sdword_t) sop2;
    sdword_t sdop3 = (sdword_t) sop3;
    dword_t dop2 = (dword_t) sdop2;
    dword_t dop3 = (dword_t) sdop3;

    // Accum is encoded in rdHi:rdLo, we convert it to signed dword
    word_t op1Lo = regfile->readGPR(instr.rd);
    word_t op1Hi = regfile->readGPR(instr.rdHi);
    dword_t dop1 = ((dword_t) op1Hi << 32) || op1Lo;
    sdword_t sdop1 = (sdword_t) dop1;

    if (instr.isUns) {
        dword_t result = (dop2 * dop3) + dop1;
        regfile->writeGPR(result >> 32, instr.rdHi);
        regfile->writeGPR((word_t) result, instr.rd);
        setMultiplyLongInstrCPSR(instr, result);
    }
    else {
        sdword_t sresult = (sdop2 * sdop3) + sdop1;
        regfile->writeGPR(sresult >> 32, instr.rdHi);
        regfile->writeGPR((word_t) sresult, instr.rd);
        setMultiplyLongInstrCPSR(instr, sresult);
    }
}

void ExecutionUnit::executeMOV(instr_st instr) {
    word_t result = instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeMRC(instr_st instr) {
    // No coprocessor on GBA
    DBG_ASSERT();
}

void ExecutionUnit::executeMRS(instr_st instr) {
    PSR_t psr;
    if (instr.isSPSR) {
        psr = regfile->getSPSR();
    }
    else {
        psr = regfile->getCPSR();
    }
    
    word_t wpsr = regfile->PSRToWord(psr);
    regfile->writeGPR(instr.rd, wpsr);
}

void ExecutionUnit::executeMSR(instr_st instr) {
    word_t wpsr = instr.op2;

    if (instr.isSPSR) {
        regfile->writeSPSR(wpsr);
    }
    else {
        regfile->writeCPSR(wpsr);
    }
}

void ExecutionUnit::executeMUL(instr_st instr) {
    word_t result = instr.op2 * instr.op3;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setMultiplyInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeMULL(instr_st instr) {
    // Convert source operands to signed dword operands
    sword_t sop2 = (sword_t) instr.op2;
    sword_t sop3 = (sword_t) instr.op3;
    sdword_t sdop2 = (sdword_t) sop2;
    sdword_t sdop3 = (sdword_t) sop3;
    dword_t dop2 = (dword_t) sdop2;
    dword_t dop3 = (dword_t) sdop3;

    if (instr.isUns) {
        dword_t result = dop2 * dop3;
        regfile->writeGPR(result >> 32, instr.rdHi);
        regfile->writeGPR((word_t) result, instr.rd);
        setMultiplyLongInstrCPSR(instr, result);
    }
    else {
        sdword_t sresult = sdop2 * sdop3;
        regfile->writeGPR(sresult >> 32, instr.rdHi);
        regfile->writeGPR((word_t) sresult, instr.rd);
        setMultiplyLongInstrCPSR(instr, sresult);
    }
}

void ExecutionUnit::executeMVN(instr_st instr) {
    word_t result = ~instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeORR(instr_st instr) {
    word_t result = instr.op1 || instr.op2;
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}
void ExecutionUnit::executeRSB(instr_st instr) {
    dword_t result = instr.op2 - instr.op1;
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}
void ExecutionUnit::executeRSC(instr_st instr) {
    dword_t result = instr.op1 - instr.op2 + regfile->readCPSR_C() - 1;
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}
void ExecutionUnit::executeSBC(instr_st instr) {
    dword_t result = instr.op2 - instr.op1 + regfile->readCPSR_C() - 1;
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}

void ExecutionUnit::executeSTC(instr_st instr) {
    // No coprocessor on GBA
    DBG_ASSERT();
}

void ExecutionUnit::executeSTM(instr_st instr) {
    word_t regList = instr.regList;
    DBG_ASSERT_E(regList == 0);
    word_t result;
    addr_t address = instr.op1;
    word_t offset = instr.isUp ? 4 : -4;
    mode_e current_mode = regfile->getCurrentMode();

    if (instr.isPSR) {
        DBG_ASSERT_E(instr.isWriteBack); // Writeback and PSR transfer can't be active at the same time
        regfile->setCurrentMode(USER);
    }
    for (int i = instr.isUp ? 0 : NUM_GPR ; i != (instr.isUp ? NUM_GPR : 0); i += instr.isUp ? 1 : -1) {
        if (GET_BIT(regList, i)) {
            if (instr.isPreIndexing) address += offset;
            word_t value = regfile->readGPR(i);
            if (i == PC) value += 8; // Normally + 12, but prefetch is already done
            MemReq req(WRITE_WORD, address, value);
            mem->request(&req);
            if (!instr.isPreIndexing) address += offset;
        }
    }

    if (instr.isPSR) {
        regfile->setCurrentMode(current_mode);
    }
    if (instr.isWriteBack) {
        regfile->writeGPR(instr.r1, address);
    }
}

void ExecutionUnit::executeSTR(instr_st instr) {
    addr_t offset = instr.op2;
    word_t result;
    addr_t address = instr.op1;

    if (!instr.isUp) offset = -offset;
    if (instr.isPreIndexing) address += offset;

    result = regfile->readGPR(instr.rd);
    if (instr.rd == PC) result += 8; // Normally + 12, but prefetch is already done
    MemReq req(instr.isByte ? WRITE_BYTE : WRITE_WORD, address, result);
    mem->request(&req);

    // 4.9.1 - In case of post-indexed addressing, the write back bit is redundant and is always
    // set to zero, since the old base value can be retained by setting the offset to zero.
    // Therefore post-indexed data transfers always write back the modified base
    if (instr.isWriteBack || (!instr.isPreIndexing)) {
        if (!instr.isPreIndexing) address += offset;
        regfile->writeGPR(instr.r1, address);
    }
}

void ExecutionUnit::executeSTRH(instr_st instr) {
    addr_t offset = instr.op2;
    word_t result;
    addr_t address = instr.op1;

    if (!instr.isUp) offset = -offset;
    if (instr.isPreIndexing) address += offset;

    result = regfile->readGPR(instr.rd);
    if (instr.rd == PC) result += 8; // Normally + 12, but prefetch is already done
    MemReq req(instr.isHalfword ? WRITE_HALF : WRITE_BYTE, address, result);
    mem->request(&req);

    // 4.9.1 - In case of post-indexed addressing, the write back bit is redundant and is always
    // set to zero, since the old base value can be retained by setting the offset to zero.
    // Therefore post-indexed data transfers always write back the modified base
    if (instr.isWriteBack || (!instr.isPreIndexing)) {
        if (!instr.isPreIndexing) address += offset;
        regfile->writeGPR(instr.r1, address);
    }
}

void ExecutionUnit::executeSUB(instr_st instr) {
    dword_t result = instr.op1 - instr.op2;
    bool carry = GET_BIT(result, 32);
    regfile->writeGPR(instr.rd, result);

    if (instr.setCondition) {
        setArithmeticalInstrCPSR(instr, instr.op1, instr.op2, result, carry);
    }
}
//void ExecutionUnit::executeSWI(instr_st instr) {
void ExecutionUnit::executeSWP(instr_st instr) {
    word_t result;
    addr_t address = instr.op1;

    MemReq req1(instr.isByte ? READ_BYTE : READ_WORD, address);
    mem->request(&req1);
    MemReq req2(instr.isByte ? WRITE_BYTE : WRITE_WORD, address, instr.op2);
    regfile->writeGPR(instr.rd, req1.data);
}

void ExecutionUnit::executeTEQ(instr_st instr) {
    word_t result = instr.op1 ^ instr.op2;

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeTST(instr_st instr) {
    word_t result = instr.op1 & instr.op2;

    if (instr.setCondition) {
        setLogicalInstrCPSR(instr, result);
    }
}

void ExecutionUnit::executeUND(instr_st instr) {
    DBG_ASSERT();
}